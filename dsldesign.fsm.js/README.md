# Ecore + Node.js + Ecore.js example

To run, install ``nodejs``, for instance on Debian-based Linux distributions:
```sh
$ sudo apt install nodejs 
```

Install `ecore.js`
```sh
$ npm install ecore
```
The above install is local to the project, not to the system, so you need to
repeat it every time you clone, etc.

Then execute
```sh
$ node constraints.js
```

``Ecore.js`` works also in the browser, which might be useful for websites
that use DSLs.
