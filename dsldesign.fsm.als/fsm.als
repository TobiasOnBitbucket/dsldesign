sig Name { }
sig Label { }

abstract sig NamedElement {
 	name: one Name
}

one sig Model extends NamedElement {
  machines : some FiniteStateMachine
}

sig FiniteStateMachine extends NamedElement {
	states : set State,
	initial : one State,
}

sig State extends NamedElement {
	leavingTransitions: set Transition,
    machine: one FiniteStateMachine
}

sig Transition {
   target: one State,
   input: one Label,
   output: lone Label,
   source: one State,
}

fact { Model.machines = FiniteStateMachine } 
fact { FiniteStateMachine.states = State } 
fact { State.leavingTransitions = Transition } 
fact { machine = ~states }
fact { source = ~leavingTransitions }

// Constraints C1 and C2: all machines (states) have distinct names is
// not used in the Alloy model. Due to weak support for modeling Strings, 
// we chose not to model names. We use instance identities instead.

fact C1 { #FiniteStateMachine.name = #FiniteStateMachine }
fact C2 { all m: FiniteStateMachine | #m.states.name = #m.states }

// Constraint C3:  For each state machine m, the state designated 
// as the initial state of m is also a member of the collection of 
// states contained in M .

fact C3 { initial in states }

// Constraint C4: Transitions cannot cross machine boundaries 
// (target and source are in the same state machine).

fact C4 { source.machine = target.machine }

// The following condition requires a transition between machines.
// You can activate it, to see that the tool will find an inconsistency 
// with the constraint, or comment out C4, to generate such instance.
// fact test_C4 { one t: Transition | t.source.machine != t.target.machine }

// Constraint C5: Each state must be reachable from the initial 
// state in each state machine.

fact C5 { all m: FiniteStateMachine |
               m.states in m.initial.*(leavingTransitions.target) }

//fact test_C5 { some m: FiniteStateMachine |
//                m.states != m.initial.*(leavingTransitions.target) }

//fact test_C5' { some m: FiniteStateMachine | some s: State | 
//  s in m.states and s in m.initial.*(leavingTransitions.target) }

pred show {}
run show for 1 Model, 1 FiniteStateMachine, 1 State, 1 Transition, 3 Label, 8 Name
