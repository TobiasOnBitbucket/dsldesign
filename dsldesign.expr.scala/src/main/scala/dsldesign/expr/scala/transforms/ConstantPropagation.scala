// (c) dsldesign, berger, wasowski

package dsldesign.expr.scala.transforms

import dsldesign.expr.scala.adt._
import dsldesign.scala.emf._
import org.bitbucket.inkytonik.kiama.rewriting.Cloner.{bottomup, everywherebu, innermost, rule, topdown}
import org.bitbucket.inkytonik.kiama.rewriting.Rewriter

class ConstantPropagation (val p: Configuration) extends
  CopyingParameterizedTrafo[Expression, Configuration, Expression] {

  val constantPropagationRule = everywherebu{
    rule[Expression]{
      case id@Identifier (_) if (p.identifierValues.keySet contains id) =>
        p.identifierValues.get (id).get
    }
  }

  override def run (self: Expression): Expression =
    Rewriter.rewrite (constantPropagationRule) (self)

}
