// (c) dsldesign, berger, wasowski

package dsldesign.expr.scala.transforms

import dsldesign.expr.scala.adt._
import dsldesign.scala.emf._
import org.bitbucket.inkytonik.kiama.rewriting.Cloner.{innermost, rule}
import org.bitbucket.inkytonik.kiama.rewriting.Rewriter

object SimplifyExpression extends CopyingTrafo[Expression, Expression] {

  val simplifyRule = innermost {
    rule[Expression] {
      case NOT (NOT (a))                 => a
      case NOT (True)                    => False
      case NOT (False)                   => True
      case OR (a, b) if a == b           => a
      case OR (True, a)                  => True
      case OR (False, a)                 => a
      case OR (a, True)                  => True
      case OR (a, False)                 => a
      case AND (True, a)                 => a
      case AND (False, a)                => False
      case AND (a, True)                 => a
      case AND (a, False)                => False
      case AND (a, b) if a == b          => a
      case AND (AND (a, b), c) if b == c => AND (a, b)
      case AND (a, AND (b, c)) if a == b => AND (b, c)

      case OR (AND (a, b), AND (c, d)) if a == c => AND (a, OR (b, d))
      case OR (AND (a, b), AND (c, d)) if b == d => AND (b, OR (a, c))
      case OR (AND (a, b), AND (c, d)) if b == c => AND (b, OR (a, d))
      case OR (AND (a, b), AND (c, d)) if a == d => AND (a, OR (b, c))

      case AND (OR (a, b), OR (c, d)) if a == c => OR (a, AND (b, d))
      case AND (OR (a, b), OR (c, d)) if b == d => OR (b, AND (a, c))
      case AND (OR (a, b), OR (c, d)) if b == c => OR (b, AND (a, d))
      case AND (OR (a, b), OR (c, d)) if a == d => OR (a, AND (b, c))

      case OR (a, AND(b, c)) if (a==b || a==c) => a
      case OR (AND(a, b), c) if (a==c || b==c) => c

      case OR (AND (a, b), OR (c, d)) if (a==c || a==d || b==c || b==d) => OR (c, d)
      case OR (OR (c, d), AND (a, b)) if (a==c || a==d || b==c || b==d) => OR (c, d)

      case OR (NOT (a), b) if a==b => True
      case OR (a, NOT (b)) if a==b => True
    }
  }

  override def run (self: Expression): Expression = Rewriter.rewrite (simplifyRule) (self)

}
