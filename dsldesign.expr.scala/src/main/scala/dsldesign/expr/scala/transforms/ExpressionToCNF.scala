// (c) dsldesign, berger, wasowski

package dsldesign.expr.scala.transforms

import dsldesign.expr.scala.adt._
import dsldesign.scala.emf._
import org.bitbucket.inkytonik.kiama.rewriting.Cloner.{everywheretd, reduce, rule}
import org.bitbucket.inkytonik.kiama.rewriting.{Rewriter, Strategy}

object ExpressionToCNF extends CopyingTrafo[Expression, Expression] {

  val demorgansRule = reduce {
    rule[Expression] {
      case NOT (AND (x, y)) => OR (NOT (x), NOT (y))
      case NOT (OR (x, y)) => AND (NOT (x), NOT (y))
    }
  }

  val doubleNotRule = reduce {
    rule[Expression] {
      case NOT (NOT (x)) => x
    }
  }

  val notValueRule = everywheretd {
    rule[Expression] {
      case NOT (True) => False
      case NOT (False) => True
    }
  }

  // use in fixpoint function
  val distributivityRule = everywheretd {
    rule[Expression] {
      case OR (x, AND (y, z)) => AND (OR (x, y), OR (x, z))
      case OR (AND (x, y), z) => AND (OR (x, z), OR (y, z))
    }
  }

  def fixpoint (s: => Strategy) (e: Expression): Expression = {
    def iter (last: Expression): Expression = {
      val result = Rewriter.rewrite (s) (last)
      if (result == last) return last else iter (result)
    }
    iter (e)
  }

  override def run (self: Expression): Expression = {
    val preCNF = Rewriter.rewrite (
      demorgansRule <*
      doubleNotRule <*
      notValueRule) (self)
    println (preCNF)
    fixpoint (distributivityRule) (preCNF)

  }

}
