// (c) dsldesign, berger, wasowski
package dsldesign.expr.scala.transforms

import dsldesign.expr.scala.adt.ExpressionConversions.string2Identifier


object ExpressionToCNFMain extends App {

  val self = !(!("a" & "b") & "b")
  println (self)

  val converted = ExpressionToCNF.run (self)

  println (converted)

}
