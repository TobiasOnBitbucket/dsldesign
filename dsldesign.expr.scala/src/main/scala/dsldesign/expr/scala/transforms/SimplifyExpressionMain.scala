// (c) dsldesign, berger, wasowski
package dsldesign.expr.scala.transforms

import dsldesign.expr.scala.adt.ExpressionConversions.string2Identifier


object SimplifyExpressionMain extends App {

  val self = "a" & "b" & "b"
  println (self)
  val simplified = SimplifyExpression.run (self)
  println (simplified)

}
