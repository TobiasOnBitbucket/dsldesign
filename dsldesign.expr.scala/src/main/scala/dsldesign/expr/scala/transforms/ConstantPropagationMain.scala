// (c) dsldesign, berger, wasowski
package dsldesign.expr.scala.transforms

import dsldesign.expr.scala.adt.{Configuration, Identifier, True}
import dsldesign.expr.scala.adt.ExpressionConversions.string2Identifier

object ConstantPropagationMain extends App {

  val self = "a" & "b" & "c"
  println (self)
  val configuration = Configuration ("test", Map (Identifier("b") -> True))
  println (configuration)

  val partiallyConfigured = new ConstantPropagation (configuration).run (self)
  println (partiallyConfigured)

}
