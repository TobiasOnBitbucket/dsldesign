// (c) dsldesign, berger, wasowski
package dsldesign.expr.scala.adt

sealed abstract class Expression {
  def & (other: Expression): Expression = other match {
    case True => this
    case _ => AND (this, other)
  }
  def | (other: Expression): Expression = other match {
    case False => this
    case _ => OR (this, other)
  }
  def unary_! (): Expression = NOT (this)
}

abstract class BinaryExpression (val left: Expression, val right: Expression) extends Expression
abstract class UnaryExpression (val expr: Expression) extends Expression

case class NOT (e: Expression) extends UnaryExpression (e) {
  override def toString = "!" + e
}
case class AND (l: Expression, r: Expression) extends BinaryExpression (l, r) {
  override def toString = "( " + l + " & " + r + " )"
}
case class OR (l: Expression, r: Expression) extends BinaryExpression (l, r) {
  override def toString = "( " + l + " | " + r+ " )"
}

case class Identifier (name: String ) extends Expression {
  override def toString = name
}

object ExpressionConversions {
  implicit def string2Identifier (s: String) = Identifier (s)
}

case object True extends Expression {
  override def & (other: Expression) = other
  override def unary_! = False
  override def toString = "TRUE"
}

case object False extends Expression {
  override def | (other: Expression) = other
  override def unary_! = True
  override def toString = "FALSE"
}

case class Configuration (val name: String, val identifierValues: Map[Identifier,Expression])
