// (c) dsldesign, wasowski, tberger
package dsldesign.fsm.scala


import scala.jdk.CollectionConverters._

import dsldesign.fsm._
import dsldesign.scala.emf._
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.scalacheck.Gen

/**
 * This package implements two kinds of generators for FSM models. 'genModel'
 * generates models that are meant to be statically correct. 'genFreeModel'
 * generates instances that are possibly not well-formed, even if they are valid
 * Ecore instances.  We will use both generators in testing the tool chain.
 *
 * It also contains a simple adapatation for Ecore generated factory methods,
 * that are a bit natural to use in value-oriented programming (function
 * programming).
 *
 * We are using the scalacheck's generator framework (Gen, see:
 * https://github.com/typelevel/scalacheck/blob/master/src/main/scala/org/scalacheck/Gen.scala)
 * This way the generators are easy to integrate with tests, however they can
 * also be used outside the testing framework.
 */

package object generators {

  /**
   * Convenience wrappers for the Ecore factories (so that we can be more
   * concise in the generators --- it would be nice to generate such wrappers
   * automaticaly).  This way we are also masking (just a notch) the imperative
   * nature of the Ecore APIs.
   */

  /** Short name for the Ecore factory singleton object */
  private val factory = FsmFactory.eINSTANCE

  /** Create a model object */
  def Model (name: String, machines: Seq[FiniteStateMachine] = List ()): Model =
  {
    val model = factory.createModel
    model.setName (name)
    model.getMachines.addAll (machines.asJava)
    model
  }


  /** Create a finite state machine object */
  def FiniteStateMachine (name: String, states: Seq[State], initial: State)
    : FiniteStateMachine = {
      val machine = factory.createFiniteStateMachine
      machine.setName (name)
      machine.getStates.addAll (states.asJava)
      machine.setInitial (initial)
      machine
    }


  /** Create a state object, and place it in a machine, if provided. */
  def State (name: String, machine: FiniteStateMachine = null): State = {
    val state = factory.createState
    state.setName (name)
    state.setMachine (machine)
    state
  }

  /** Create a new transition object. */
  def Transition (input: String,
                  output: String = null,
                  source: State = null,
                  target: State = null): Transition = {

    val transition = factory.createTransition
    transition.setInput (input)
    transition.setOutput (output)
    transition.setSource (source)
    transition.setTarget (target)
    transition
  }


  /**
   * The generators for FSM models. Let's first build a generator of relatively
   * correct models. Relatively? We want them to be correct, but we will check
   * that when testing constraints.
   *
   * The idea is to exploit the partonomy in generation: transitons are
   * generated for each machine separately, this way they don't cross machine
   * boundaries.  States are generated for each machine separately, and assigned
   * from as initial from the machine-specific set.  In general, generating type
   * correct instances may be difficult (in such case it might be better to ask
   * a solver or a tool like Alloy Analyzer for help).
   **/

  val genFreeName: Gen[String] = for {

    predefined <- Gen.oneOf ("Name1" , "Name2", "Name3", "Name4")
    fresh <- Gen.asciiPrintableStr suchThat { !_.isEmpty }
    name <- Gen.frequency ( 2 -> predefined, 1 -> fresh )

  } yield name.substring (0, name.size min 60)



  val genName: Gen[String] = for {

    L <- Gen.choose (1, 30)
    first <- Gen.alphaChar
    chars <- Gen.listOfN (L, Gen.alphaNumChar)

  } yield (first::chars).mkString



  val genModel: Gen[Model] = for {

    name <- genName
    M <- Gen.choose (0, 5)
    machines <- Gen.listOfN (M, genMachine)

  } yield Model (name, machines)



  val genMachine: Gen[FiniteStateMachine] = for {

    name <- genName
    S <- Gen.choose (1, 10)
    states <- Gen.listOfN (S, genState)
    initial <- Gen.oneOf (states)

    T <- Gen.choose (0, 20)
    transitions <- Gen.listOfN  (T, genTransition (states, genName))

  } yield FiniteStateMachine (name, states, initial)



  val genState: Gen[State] =
    for { name <- genName }
    yield State (name)



  def genTransition (states: Seq[State], gen: Gen[String]): Gen[Transition] = for {

    input <- gen
    output <- gen
    source <- Gen.oneOf (states)
    target <- Gen.oneOf (states)

  } yield Transition (input, output, source, target)


  /**
   * For models that can possibly violate static constraints, we generate a flat
   * set of states and a flat set of machines.  Then we generate transitions
   * between these states, and arbitrarily assign the individual states to
   * machines, also to initial.  Most of these models should fail our
   * constraints.
   *
   * We still want them to satisfy the meta-model constraints, so that we do not
   * get random crashes from EMF.
   */
  lazy val genFreeModel: Gen[Model] = for {

      name <- genFreeName

      M <- Gen.choose (0, 30)
      machines <- Gen.listOfN (M, genFreeMachine)

      S <- Gen.choose (M, 2*M)
      states <- Gen.listOfN (S, genFreeState (machines))

      T <- Gen.choose (0, 10*M)
      // Connected to model via side effects, courtesy of Ecore
      _ <- Gen.listOfN  (T, genTransition (states, genFreeName))

      // Only run for side effects, courtesy Ecore
      _ <- Gen.sequence[List[Unit],Unit] {
              for ( ma <- machines )
              yield
                Gen.oneOf (states)
                  .map { ma.setInitial _ }
            }
      _ = assert ( machines.forall { _.getStates.asScala.size >=1 } )
      _ = assert ( machines.forall { _.getInitial != null } )
      _ = assert ( states.forall { _.getMachine != null } )

    } yield Model (name, machines)



  def genFreeState (machines: List[FiniteStateMachine]): Gen[State] = for {

      name <- genFreeName

      // A free model could have states not connected to machines, but this
      // crashes Ecore infrastructure as it violates meta-model constraints,
      // so this is not a useful test. We put each state in some machine.
      owner <-
        machines.find { _.getStates.isEmpty } match {

          case Some (m) =>
            Gen.const (m) // first fill machines with no states to eliminate such

          case None =>
            Gen.oneOf (machines)
        }

    } yield State (name, owner)



  lazy val genFreeMachine: Gen[FiniteStateMachine] =
    for { name <- genFreeName }
    yield FiniteStateMachine (name, List (), null)

}
