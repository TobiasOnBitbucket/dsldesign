// (c) dsldesign, wasowski, tberger
// An in-place transformation implemented directly in Scala (no special
// frameworks).  Translates a finite state machine to a Petri net.  There is
// presently no runner for this example.

package dsldesign.fsm.scala.transforms

import dsldesign.scala.emf._
import dsldesign.{fsm, petrinet => PN}

import scala.jdk.CollectionConverters._

object FsmToPetriNet extends CopyingTrafo[fsm.Model, PN.Model] {

  val pFactory = PN.PetrinetFactory.eINSTANCE

  // we add some queries over States
  implicit class StateExtension( self: fsm.State ){
    def getInitialTokenCount = if ( self.getMachine.getInitial == self ) 1 else 0
    def isEndState = self.getLeavingTransitions.isEmpty
  }

  def convertState (self: fsm.State): PN.Place = pFactory.createPlace before { p =>
    p setName self.getName
    p setTokenNo self.getInitialTokenCount
  }

  def convertTransition (places: List[PN.Place]) (self: fsm.Transition): PN.Transition =
    pFactory.createTransition before { pnt =>
      pnt setInput self.getInput
      pnt.getFromPlace addAll (places.filter (_.getName == self.getSource.getName).asJava)
      pnt.getToPlace   addAll (places.filter (_.getName == self.getTarget.getName).asJava)
    }

  def convertEndState2RemTrans( places: List[PN.Place] )( self: fsm.State ): PN.Transition =
    pFactory.createTransition before { pnt =>
      pnt setInput ""
      pnt.getFromPlace add( places.find(_.getName == self.getName ).get )
  }

  def convertStateMachine (self: fsm.FiniteStateMachine): PN.PetriNet =
    pFactory.createPetriNet before { pn =>

      pn setName self.getName
      val places = self.getStates.asScala.toList map convertState
      pn.getPlaces.addAll (places.asJava)
      pn.getTransitions.addAll (
        self.getStates.asScala
        .flatMap (_.getLeavingTransitions.asScala.toList)
        .map (convertTransition (places) _)
        .asJava)

      // for each end state generate a transition that throws a token away
      pn.getTransitions.addAll (
        self.getStates.asScala
        .filter( _.isEndState )
        .map( convertEndState2RemTrans( places ) )
        .asJava
      )
    }

  override def run (self: fsm.Model): PN.Model =
    pFactory.createModel before {
      _.getPetrinets addAll (self.getMachines.asScala.map(convertStateMachine _).asJava) }

}

//def fsmEndStates (m: fsm.FiniteStateMachine): List[fsm.State] =
//m.getStates.asScala.toList filter (_.getLeavingTransitions.isEmpty)

// prev. implementation, changed to align with the qvto trafo
// for each end state generate a transition that throws a token away
//      for (s <- fsmEndStates (self)) {
//        val Some (p: PN.Place) = places find (_.getName == s.getName)
//        pFactory.createTransition before { t =>
//          t.getFromPlace.add (p)
//          t setInput ""
//          pn.getTransitions.add (t)
//          p.getOutgoingTransitions.add (t)
//        }
//      }

//// doesn't help
//implicit def scalalist2javaCollection( l:List[_ <: EObject] ):java.util.Collection[_ <: EObject] = l.asJavaCollection
//
//// helps
//implicit def scalaPlacelist2javaCollection( l:List[PN.Place] ) = l.asJavaCollection
//implicit def javaEList2scalaList( l:EList[fsm.State] ) = l.asScala.toList
