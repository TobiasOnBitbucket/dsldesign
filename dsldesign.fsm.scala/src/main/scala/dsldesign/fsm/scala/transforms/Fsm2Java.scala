// (c) dsldesign, wasowski, tberger
//
// Translate a a finite statem machine to a simple Java program.
// We use a minimal subset of Java, and little structure.  Almost the same
// code should be generated for C, to be used on a micro-controller (for example).
//
// The lack of control-string inversion like in template languages gets in the way
// here and there.
package dsldesign.fsm.scala.transforms

import java.io.File
import scala.jdk.CollectionConverters._

import dsldesign.fsm._
import dsldesign.scala.emf._
import org.eclipse.emf.ecore.resource.Resource

object Fsm2Java
{
  private def mkId (s: String): String = s
    .filter { c => c.isLetter || c.isDigit }
    .capitalize

  private def mkID (s: String): String = s
    .replace (' ', '_')
    .filter { c => c.isLetter || c.isDigit || c == '_' }
    .toUpperCase

  def fmtInputs (s: State): String =
    (s.getLeavingTransitions.asScala.map { "<" + _.getInput + ">" })
      .mkString ("|    \"","","\"")

  def fmtState (state: State): String =
    s"""
        |          case ${mkID(state.getName)}:
        |            switch (input) {
                       ${(state.getLeavingTransitions.asScala map {
                            fmtTransition (_) }).mkString.dropRight(1)}|            }
        |            break;
        """

  def fmtTransition (t: Transition): String =
    s"""|              case "${t.getInput}":
        |                System.out.println ("machine says: ${t.getOutput}");
        |                current = ${mkID (t.getTarget.getName)};
        |                break;
        """

  def compileToJava(it: FiniteStateMachine): String = {

    val states = it.getStates.asScala

    List (s"""|import java.util.Scanner;
              |
              |class FSM${mkId(it.getName)} {
              |
              """,
              (states.zipWithIndex.map { case (s,i) =>
          s"""|  static final int ${mkID{s.getName}} = ${i};
          """ }) .mkString,
          s"""|
              |  static int current;

              |  static final String[] stateNames = {
              |    ${states map {"\"" + _.getName + "\""} mkString (", ") }
              |  };
              |
              |  static final String[] availableInputs = {
                   ${states map { fmtInputs (_) } mkString (",\n") }
              |  };
              |
              |  public static void main (String[] args) {
              |
              |    @SuppressWarnings(value = { "resource" })
              |    Scanner scanner = new Scanner (System.in);
              |    current = ${mkID (it.getInitial.getName)};
              |
              |    while (true) {
              |
              |      System.out.print ("[" + stateNames[current] + "] ");
              |      System.out.print ("What is the next event? available: "
              |                           + availableInputs[current]);
              |      System.out.print ("?");
              |      String input = scanner.nextLine();
              |
              |      switch (current) {
                       ${(states map { fmtState (_) }).mkString}
              |      }
              |    }
              |  }
              |}
              |""").mkString.stripMargin
  }

}
