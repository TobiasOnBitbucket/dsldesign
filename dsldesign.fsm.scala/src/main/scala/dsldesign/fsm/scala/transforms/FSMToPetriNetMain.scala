// (c) dsldesign, wasowski, tberger
// Example constraints implemented for the FSM models in Scala
// This is the main runner for the constraints example
// Run using 'sbt run'
package dsldesign.fsm.scala.transforms

import dsldesign.fsm.{FsmPackage, Model}
import dsldesign.scala.emf
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl


object FSMToPetriNetMain extends App {

  // register a resource factory for XMI files
  Resource.Factory.Registry.INSTANCE.
    getExtensionToFactoryMap.put("xmi", new XMIResourceFactoryImpl)

  // register the package magic (impure)
  FsmPackage.eINSTANCE.eClass

  // load the fsm model
  val model:Model = emf.loadFromXMI( "dsldesign.fsm/test-files/CoffeeMachine.xmi" )

  // we run the transformation
  val petrinetModel = FsmToPetriNet.run( model )

  // save the petrinet model
  emf.saveAsXMI ( "dsldesign.petrinet/test-files/CoffeeMachine-transformed-by-Scala.xmi") ( petrinetModel )

}
