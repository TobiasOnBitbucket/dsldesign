/*
   Copyright 2020-2021 Andrzej Wasowski and Thorsten Berger

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package dsldesign.fsm.scala.internal

import scala.jdk.CollectionConverters._
import dsldesign.fsm
import dsldesign.fsm.FsmFactory

package object deep {

  /** A transformation from our ADT representation to Ecore FSM instance */
  private[deep] def modelRep2Model (m: ModelRep) : fsm.Model = {

    val s2tMap = m.tran.map { t => t.source -> t }.toMap

    def stateRep2State (s: String) : fsm.State = {
      val S = FsmFactory.eINSTANCE.createState
      S.setName (s)
      S
    }

    val SS = m.states.map (s => s -> stateRep2State (s)).toMap

    def tranRep2Transition (t: TranRep) : fsm.Transition = {
      val T = FsmFactory.eINSTANCE.createTransition
      t.input.foreach  { T setInput  (_) }
      t.output.foreach { T.setOutput (_) }
      t.target.foreach { s => T.setTarget (SS (s)) }
      T
    }

    m.tran.foreach { (t: TranRep) =>
      SS(t.source).getLeavingTransitions.add (tranRep2Transition (t)) }

    val M = FsmFactory.eINSTANCE.createModel
    M.setName (m.name)
    val FSM = FsmFactory.eINSTANCE.createFiniteStateMachine
    M.getMachines.add (FSM)
    FSM.getStates.addAll (SS.asJava.values)
    m.initial.foreach { s => FSM.setInitial (SS(s)) }
    M
  }

}
