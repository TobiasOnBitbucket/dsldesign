/*
   Copyright 2020-2021 Andrzej Wasowski and Thorsten Berger

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

/** An example model, CoffeeState, in the fsm language, the variant using
  * shallow embedding in Scala (an internal dSL).
  */
package dsldesign.fsm.scala.internal.shallow

object ShallowCoffeeMachine {

  lazy val initial: State = (state
    input "coin"  output "what drink do you want?" target selection
    input "idle"                                   target initial
    input "break" output  "machine is broken"      target deadlock
  )

  lazy val selection: State = (state
    input "tea"     output "serving tea"                target makingTea
    input "coffee"  output "serving coffee"             target makingCoffee
    input "timeout" output "coin returned; insert coin" target initial
    input "break"   output "machine is broken!"         target deadlock
  )

  lazy val makingCoffee: State = (state
    input "done"    output "coffee served. Enjoy!"      target initial
    input "break"   output "machine is broken!"         target deadlock
  )

  lazy val makingTea: State = (state
    input "done"    output "tea served. Enjoy!"         target initial
    input "break"   output "machine is broken!"         target deadlock
  )

  lazy val deadlock: State = (state)

}
