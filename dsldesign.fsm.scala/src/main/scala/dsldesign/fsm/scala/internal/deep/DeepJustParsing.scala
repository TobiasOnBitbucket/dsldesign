/*
   Copyright 2020-2021 Andrzej Wasowski and Thorsten Berger

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package dsldesign.fsm.scala.internal.deep

/** The FSM language implemented as an internal DSL in Scala.  Only one machine
  * per model, for simplicity. This file only shows "parsing" so defining the
  * syntax, while Deep.scala also includes AST construction.
  *
  * The keywords are captured either by singleton objects or methods.  An object
  * can introduce the keywords that are allowed directly to follow it by
  * implementing the suitably named methods. Each method returns an object of a
  * new class that defines what keywords are allowed to follow.
  */
object DeepJustParsing {

  /** Introduce the opening keywords "state machine". */
  object state {
    def machine (name: String) = INITIAL_OR_STATE_OR_END
  }

  /** In the context of a machine, allow opening a state definition (using
   *  methods "state" or "initial") and a closing a machine using "end".  */
  object INITIAL_OR_STATE_OR_END  {

    /** Open a new non-initial state.  Only "input" may follow, we
     *  start a new state, or "end" the entire machine.
      */
    def state (src: String) = INPUT_OR_NEXT_STATE

    /** Open a new initial state. */
    def initial (src: String) = INPUT_OR_NEXT_STATE

    /** Close the current machine object. */
    val end = ()
  }

  /** Detect a new transition (input) or end of the state definition */
  object INPUT_OR_NEXT_STATE  {

    /** Parse a new transition definition */
    def input (input: String) = OUTPUT_OR_TARGET

    /** End of the state definition, start a new state. */
    def state (name: String) = INITIAL_OR_STATE_OR_END.state (name)

    /** End of the state definition, start a new initial state. */
    def initial (name: String) = INITIAL_OR_STATE_OR_END.initial (name)

    /** End the state machine definition. Return to the machine level. */
    val end = ()
  }

  /** Detect the target state phrase */
  class TARGET {
    /** Detect "and go" and then await for "to". */
    def target (name: String) = INPUT_OR_NEXT_STATE
  }

  /** Detect an optional output action or a target state (inherited). */
  object OUTPUT_OR_TARGET extends TARGET  {
    /** Record the output of a transition and move to the target state.  */
    def output (output: String) = new TARGET
  }
}
