/*
   Copyright 2020-2021 Andrzej Wasowski and Thorsten Berger

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


/** The FSM language implemented as an internal DSL in Scala.  Only one machine
  * per model, for simplicity.
  */
package dsldesign.fsm.scala.internal.deep

import dsldesign.scala.emf._
import dsldesign.fsm
import dsldesign._

/** Very simple AST algebraic data types (ADT) for internal representation of
  * the parsed model in Scala. In the last phase of execution we will transform
  * this to EMF types, so that we gain compatibility with other possible parts
  * of the tool chain that may be implemented following the external DSL route.
  */
sealed case class ModelRep (
  name: String,
  states: List[String] = Nil,
  tran: List[TranRep] = Nil,
  initial: Option[String] = None
)

sealed case class TranRep (
  source: String,
  input: Option[String] = None,
  output: Option[String] = None,
  target: Option[String] = None
)

/** Below we capture the syntax of the internal DSLs. The keywords are captured
  * either by singleton objects or methods.  An object can introduce the
  * keywords that are allowed directly to follow it by implementing the
  * suitably named methods. Each method returns an object of a new class that
  * defines what keywords are allowed to follow.
  */

/** Introduce the opening keywords "state machine".  Note that the other use
  * of "state", for defining a new state is not implemented using this
  * singleton, but using a method on "EXPECT_INITIAL_OR_STATE_OR_END.
  */
case object state {
  def machine (name: String) =
    INITIAL_OR_STATE_OR_END (ModelRep (name))
}

/** Inside a machine context we allow opening a state definition (the "state"
  * method and the "initial" method for initial states) and a closing of a
  * machine with an "end" keyword.
  */
case class INITIAL_OR_STATE_OR_END (machine: ModelRep) {

  /** Open a new state object, with an "initial" keyword, so representing the
    * initial state.
    */
  def initial (src: String) = {
    val model1 = machine.copy (initial = Some (src), states = src ::machine.states)
    INPUT_OR_NEXT_STATE (model1, src)
  }


  /** Open a new state object, with a "state" keyword, after this only "input"
    * is a valid input, or we are seeing a new state with "state" or "initial"
    * (or "end" to close the model).
    */
  def state (src: String) = {
    val model1 = machine.copy (states = src ::machine.states)
    INPUT_OR_NEXT_STATE (model1, src)
  }


  /** Close the current machine object, with "end" and translate the current
    * partial model representation to an Ecore instance.
    */
  def end: fsm.Model =
    modelRep2Model (machine)
}


/** Detect a new transition (input) or end of the state definition */
case class INPUT_OR_NEXT_STATE (machine: ModelRep, src: String) {

  /** Parse a new transition definition */
  def input (input: String) = {
    val tran = TranRep (source = src, input = Some (input))
    OUTPUT_OR_TARGET (machine, src, tran)
  }

  /** End of the state definition, start a new state. We delegate to
    * the machine level and the function already implemented there.
    */
  def state (name: String) =
    INITIAL_OR_STATE_OR_END (machine).state (name)

  /** End of the state definition, start a new initial state. We delegete
    * the machine level and the function already implemented there.
    */
  def initial (name: String) =
    INITIAL_OR_STATE_OR_END (machine).initial (name)

  /** End the state machine definition. We delegate to the machine level. */
  def end: fsm.Model =
    INITIAL_OR_STATE_OR_END (machine).end
}


/** Detect the target state phrase */
class TARGET (machine: ModelRep, src: String, tran: TranRep) {

  /** Detect "and go" and move to an objecct that awaits for "to".
    * We could use inheritance here to merge it with EXPECT_OUTPUT_OR_AND.
    */
  def target (name: String) = {
    val tran1 = tran.copy (target = Some (name))
    val machine1 = machine.copy (tran = tran1 ::machine.tran)
    INPUT_OR_NEXT_STATE (machine1, src)
  }
}

/** Detect an optional output actiona or a target state */
case class OUTPUT_OR_TARGET (machine: ModelRep, src: String, tran: TranRep)
  extends TARGET (machine, src, tran) {

  /** Record the output of a transition and move to the target state.  */
  def output (output: String) = {
    val tran1 = tran.copy (output = Some(output))
    new TARGET (machine, src, tran1)
  }
}
