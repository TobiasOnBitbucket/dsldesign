/*
   Copyright 2020-2021 Andrzej Wasowski and Thorsten Berger

   Licensed under the Apache License, Version 2.0 (the "License"); you may not
   use this file except in compliance with the License.  You may obtain a copy
   of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
   WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
   License for the specific language governing permissions and limitations under
   the License.
*/

/** The FSM language implemented as a shallow DSL embedded in Scala.  Only one
  * machine per model.
  */
package dsldesign.fsm.scala.internal.shallow

/** The top-level values and types are implemented in the package object in
  * Scala 2 (see package.scala).
  */

trait State {

  /** A State object embeds a function 'step', its essential element that
    * represents the meaning of the state -- the transition from an input to an
    * output and a target state. We implement this function (abstract here)
    * differently for each actual state, see below under 'target'.
    */
  def step: Step

  /** Our DSL has "partial operators". Input by itself is unable to create a
    * state transition. It needs an output (optional) and a target (mandatory).
    * Thus we "cheat" slightly, and create a local "deep" representation, a
    * suspended transition. Once the target is available, we immediately provide
    * the semantics for triaging the transition.
    */
  def input (event: String): Suspended =
    Suspended (source=this, event=event, output=None)

}

case class Suspended (source: State, event: String, output: Option[String]) {

  def output (o: String): Suspended =
    Suspended (source, event, Some (o))

  /** Target is the operator that is "ready" to create a new State, as all the
    * information is now available. It compiles a transition to a State, so a
    * function from input to an output and a target state.
    */
  def target (t: => State): State = new State {
    def step: Step = input =>
      if (input == event) (output,t)
      else source.step (input)
  }

}
