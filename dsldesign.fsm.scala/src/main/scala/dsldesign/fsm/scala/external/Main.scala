/*
   Copyright 2020-2021 Andrzej Wasowski and Thorsten Berger

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

/** Run FSM interpreter with a CLI frontend; load and parse outside Eclipse. */
package dsldesign.fsm.scala.external

import dsldesign.fsm.FsmPackage
import dsldesign.fsm.FiniteStateMachine
import dsldesign.fsm.Model
import dsldesign.fsm.scala._
import dsldesign.scala.emf._
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl

object Main extends App {

  // Change input file name of your state machine model here;
  // path is relative to project root
  val instanceFileName = "../dsldesign.fsm/test-files/CoffeeMachine.xmi"

  // register a resource factory for XMI files
  Resource.Factory.Registry.INSTANCE.
  getExtensionToFactoryMap.put("xmi", new XMIResourceFactoryImpl)

  // Register our meta-model package for abstract syntax
  FsmPackage.eINSTANCE.eClass

  // Load the file
  val uri = URI.createURI (instanceFileName)
  var resource = new ResourceSetImpl().getResource (uri, true);

  // The call to get(0) below gives you the first model root.
  // If you open a directory instead of a file,
  // you can iterate over all models in it,
  // by changing 0 to a suitable index
  val model = resource.getContents.get (0).asInstanceOf[Model]

  run (model)
}

