/*
   Copyright 2020-2021 Andrzej Wasowski and Thorsten Berger

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package dsldesign.fsm

import _root_.scala.jdk.CollectionConverters._
import _root_.scala.io
import dsldesign.fsm

import dsldesign.scala.emf._

package object scala {

  /** An interpreter for FSM models with a single machine */
  def run (m: fsm.Model): Unit = {
    require (m != null && m.getMachines.size == 1)
    repl (m.getMachines.get(0).getInitial)
  }

  /** A repl for FSM that reads an input from the user, evaluates it to get a
    * new state, prints an output (if present), and loops again on the new state.
    * REPL = Read-Evaluate-Print-Loop
    */
  def repl (s: fsm.State): Unit =  {

    val inputs = s.getLeavingTransitions.asScala.map { _.getInput } mkString ", "
    print (s"\nMachine is in state: ${s.getName}. Input [$inputs]? ")
    val input = io.StdIn.readLine

    eval (s) (input) match {

      case Some (s1 -> Some (output)) =>
        repl (s1) before { println (s"Machine outputs: $output") }

      case Some (s1 -> None) =>
        repl (s1)

      case None =>
        repl (s) before { println ("Invalid input!") }
    }
  }


  /** An evaluator for a given state. Returns None if there is no transition
    * labelled 'input', or (Some of) a pair of the successor state and an output
    * message.  The output message is optional, because we admit silent.
    * transitions.
    *
    * @param s presently active state
    * @param input the input message (action/event) to be send to the machine
    * @return result of finding and firing a suitable transition.
    */
  def eval (s: fsm.State) (input: String): Option[(fsm.State, Option[String])] =
    s.getLeavingTransitions.asScala
     .find { tran => tran.getInput == input }
     .map { tran => (tran.getTarget, Option (tran.getOutput))}
}
