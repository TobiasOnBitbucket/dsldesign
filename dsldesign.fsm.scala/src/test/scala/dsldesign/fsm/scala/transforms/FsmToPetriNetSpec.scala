// (c) dsldesign, wasowski, tberger
package dsldesign.fsm.scala.transforms

import org.eclipse.emf.common.util.Diagnostic
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.util.Diagnostician
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import org.eclipse.emf.ecore.EObject

import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

import dsldesign.scala.emf

import dsldesign.fsm
import dsldesign.{ petrinet => PN }

class FsmToPetriNetSpec extends AnyFreeSpec with Matchers {

  // Initialize EMF

  Resource.Factory.Registry.INSTANCE.
    getExtensionToFactoryMap.put("xmi", new XMIResourceFactoryImpl)
  fsm.FsmPackage.eINSTANCE.eClass
	PN.PetrinetPackage.eINSTANCE.eClass

  // Set up fixtures. Note: imperative tests destroy fixtures

  val ids = List ("test-00", "test-01", "test-02", "test-03", "test-04", "test-05", "test-06", "test-07")
  val N = ids.size-1
  val fnames = ids map (t => s"../dsldesign.fsm/test-files/$t.xmi")
  val roots :List[fsm.Model] = fnames map {emf loadFromXMI[fsm.Model] _}

  sealed case class Fixture (root :fsm.Model, id :String)

  def fixture (n: Int) = Fixture (
      root = EcoreUtil.copy(roots(n)).asInstanceOf[fsm.Model], // yuck
      id = ids(n) )

  // helper methods

  def validates (root: EObject): Boolean =
    emf.validate (root, false)
      .map[Boolean] { m => m.foreach[Unit] { s => info (s"\t$s") }; false }
      .getOrElse (true)

  // tests

  "FsmToPetriNet" - {

    s"give results that validate in the new metamodel" in {

      val in = 0 to N map (fixture _) filter { m => validates (m.root) }
      val out = in map { FsmToPetriNet run  _.root }
      out foreach { o => validates (o) shouldBe true }
    }

    s"save results of each transformations into new pn xmi format" in {

      val in = 0 to N map (fixture _) filter { m => validates (m.root) }
      val out = in.map { f => f.id -> (FsmToPetriNet run  f.root) }
      out foreach { m => (emf.saveAsXMI (s"test-out/${m._1}.pn.xmi") (m._2)) }

      // let's see whether they load without errors

      val reloaded = out map { m => emf.loadFromXMI[PN.Model](s"test-out/${m._1}.pn.xmi") }
      reloaded foreach { m => validates (m) shouldBe true }

    }
  }

}
