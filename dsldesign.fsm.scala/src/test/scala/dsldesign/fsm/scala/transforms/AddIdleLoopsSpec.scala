// (c) dsldesign, wasowski, tberger

package dsldesign.fsm.scala.transforms

import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

import dsldesign.fsm.scala.Constraints
import dsldesign.fsm.{FsmPackage, Model}
import dsldesign.scala.emf
import dsldesign.scala.emf.asScalaTreeIteratorConverter
import org.eclipse.emf.ecore.util.EcoreUtil

class AddIdleLoopsSpec extends AnyFreeSpec with Matchers {

  // Initialize the meta-model
  FsmPackage.eINSTANCE.eClass

  // Load test fixtures
  val ids = List ("test-00", "test-01", "test-02", "test-03", "test-04",
                  "test-05", "test-06", "test-07")
  val roots: List[Model] =
    ids.map { t => emf.loadFromXMI[Model] (s"../dsldesign.fsm/test-files/$t.xmi") }

  sealed case class Fixture (root :Model, id :String)

  def fixture (n: Int) = Fixture (
      root = EcoreUtil.copy (roots (n)).asInstanceOf[Model], // yuck
      id = ids (n) )

  // helper methods

  def hasLoops (root: Model) :Boolean =
    EcoreUtil.getAllContents (root,false).forall ( Constraints.idle.check _ )

  // The tests in this module has been written ignoring the OCL constraints
  // embedded in fsm.ecore, so we deactivate OCL validation here
  def validates (root: Model) :Boolean =
    emf.validate (root, ocl=false)
      .map[Boolean] { m => m.foreach[Unit] { s => info (s"\t$s") }; false }
      .getOrElse (true)

  // tests

  "AddIdleLoops" - {

    s"add a loop on a single state ${ids(0)}" in {

      val f = fixture (0)

      assert (validates(f.root))
      assert (!hasLoops(f.root))

      AddIdleLoops.run (f.root)

      assert (validates(f.root))
      assert (hasLoops(f.root))
    }

    "don't add an idle transition if one exists" in {

      val f = fixture (2)
      val size = EcoreUtil.getAllContents (f.root,false).size

      assert (validates(f.root))
      assert (hasLoops(f.root))

      AddIdleLoops.run (f.root)

      assert (validates(f.root))
      assert (hasLoops(f.root))
      EcoreUtil.getAllContents (f.root,false).size shouldBe size
    }

    "add idle on more than one state" in {

      val f = fixture (3)
      assert (validates(f.root) && !hasLoops(f.root))
      assert (f.root.getMachines.get(0).getStates.get(0).getLeavingTransitions.isEmpty)
      assert (f.root.getMachines.get(1).getStates.get(0).getLeavingTransitions.isEmpty)
      assert (f.root.getMachines.get(1).getStates.get(1).getLeavingTransitions.isEmpty)

      AddIdleLoops.run (f.root)

      assert (validates(f.root) && hasLoops(f.root))
      assert (f.root.getMachines.get(0).getStates.get(0).getLeavingTransitions.get(0).getInput == "idle")
      assert (f.root.getMachines.get(1).getStates.get(0).getLeavingTransitions.get(0).getInput == "idle")
      assert (f.root.getMachines.get(1).getStates.get(1).getLeavingTransitions.get(0).getInput == "idle")
    }

    "not crash on an empty model" in {

      val f = fixture (7)
      assert (validates(f.root))
      AddIdleLoops.run (f.root)
      EcoreUtil.getAllContents(f.root, false).size shouldBe 0
    }

    "run on a maximal model (mm-coverage)" in {

      val f = fixture (5)
      assert (validates(f.root))
      assert (!hasLoops(f.root))
      AddIdleLoops.run (f.root)
      assert (validates(f.root))
      assert (hasLoops(f.root))
    }
  }
}
