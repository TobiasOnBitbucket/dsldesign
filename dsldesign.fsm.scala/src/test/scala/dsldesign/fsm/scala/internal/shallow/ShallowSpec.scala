/*
   Copyright 2020-2021 Andrzej Wasowski and Thorsten Berger

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package dsldesign.fsm.scala.internal.shallow

/** Example scenario tests for shallowly embedded DSL for Finite State Machines */
class ShallowSpec
  extends org.scalatest.freespec.AnyFreeSpec
  with org.scalatest.matchers.should.Matchers
  with org.scalatestplus.scalacheck.ScalaCheckPropertyChecks {

  val deadlock = state

  "t0 composed t1 on random input not handled by t1 is the same as t" in {

    forAll { inputStr: String =>
      forAll { inputStr1: String =>
        whenever (inputStr != inputStr1) {

          lazy val t0: State = state input (inputStr) output (inputStr) target (t0)
          val t1 = t0 input (inputStr1) output (inputStr1) target (deadlock)

          t0.step (inputStr) should be { t1.step (inputStr) }

          val t2 = state input (inputStr1) output (inputStr1) target (deadlock)
          t2.step (inputStr1) should be { t1.step (inputStr1) }
        }
      }
    }
  }

}
