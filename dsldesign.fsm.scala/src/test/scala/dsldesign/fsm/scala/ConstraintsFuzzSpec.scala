// (c) dsldesign, wasowski, tberger
package dsldesign.fsm.scala

import scala.jdk.CollectionConverters._

import org.eclipse.emf.ecore.util.EcoreUtil
import org.scalacheck.Gen
import org.scalacheck.Prop
import org.scalacheck.Shrink



class ConstraintsFuzzSpec
  extends org.scalatest.freespec.AnyFreeSpec
  with org.scalatest.matchers.should.Matchers
  with org.scalatest.BeforeAndAfter
  with org.scalatest.prop.Configuration
  with org.scalatestplus.scalacheck.Checkers
  with org.scalatestplus.scalacheck.ScalaCheckPropertyChecks {

  import dsldesign.fsm._
  import dsldesign.fsm.scala.generators._
  import dsldesign.fsm.scala.Constraints._
  import dsldesign.scala.emf._


  // We do not want any shrinking when working with Ecore
  // The framework is stateful and it is too easy to mess it up with leftover objects
  // (this would be fine when working with ADTs though)
  // (for the same reason these tests are also mesed up by concurrency)
  implicit def noShrink[T]: Shrink[T] = Shrink.shrinkAny

  before { dsldesign.fsm.FsmPackage.eINSTANCE.eClass }

  "Fuzz the generators" - {

    // Be more brutal (more iterations)
    implicit val generatorDrivenConfig =
      PropertyCheckConfiguration (minSuccessful = 250)


    "Instances created by genFreeModel validate" in check {

      Prop.forAll (genFreeModel) { m: Model =>
        validate (m, false)
          .isEmpty
          .before { EcoreUtil.delete (m) }
      }
    }



    "Instances created by genModel validate" in check {

      Prop.forAll (genModel) {m: Model =>
          validate (m, ocl = false)
            .isEmpty
            .before { EcoreUtil.delete (m) }
      }
    }
  }



  "Fuzz the constraints" - {

    def fuzz (c: Constraint, gen: Gen[Model]) =
      Prop.forAll (gen) { m: Model =>
        c.checkAll (m)
        EcoreUtil.delete (m)
        true  // only test for exceptions, value of constraint irrelevant
      }


    "C1 on valid models"  in check { fuzz (C1, genModel) }
    "C2 on valid models"  in check { fuzz (C2, genModel) }
    "C2a on valid models" in check { fuzz (C2a, genModel) }
    "C2b on valid models" in check { fuzz (C2b, genModel) }
    "C3 on valid models"  in check { fuzz (C3, genModel) }
    "C4 on valid models"  in check { fuzz (C4, genModel) }
    "C5 on valid models"  in check { fuzz (C5, genModel) }
    "C6 on valid models"  in check { fuzz (C6, genModel) }


    "C1 on free models"  in check { fuzz (C1, genFreeModel) }
    "C2 on free models"  in check { fuzz (C2, genFreeModel) }
    "C2a on free models" in check { fuzz (C2a, genFreeModel) }
    "C2b on free models" in check { fuzz (C2b, genFreeModel) }
    "C3 on free models"  in check { fuzz (C3, genFreeModel) }
    "C4 on free models"  in check { fuzz (C4, genFreeModel) }
    "C5 on free models"  in check { fuzz (C5, genFreeModel) }
    "C6 on free models"  in check { fuzz (C6, genFreeModel) }
  }



  "Consistency (Constraints should be satisfiable)" - {

    implicit val generatorDrivenConfig =
      PropertyCheckConfiguration (
        minSuccessful = 1,
        maxDiscardedFactor = 1000 )

    def consistent (c: Constraint): Prop =
      Prop.exists (genModel) { m: Model =>
        c.checkAll (m) before { EcoreUtil.delete (m) } }

    "C1 is satisfisfiable"  in check { consistent (C1) }
    "C2 is satisfisfiable"  in check { consistent (C2) }
    "C2a is satisfisfiable" in check { consistent (C2a) }
    "C2b is satisfisfiable" in check { consistent (C2b) }
    "C3 is satisfisfiable"  in check { consistent (C3) }
    "C4 is satisfisfiable"  in check { consistent (C4) }
    "C5 is satisfisfiable"  in check { consistent (C5) }
    "C6 is satisfisfiable"  in check { consistent (C6) }
  }


  "Falsfiability (constraints must be possible to violate)" - {

    implicit val generatorDrivenConfig =
      PropertyCheckConfiguration (
        minSuccessful = 1,
        maxDiscardedFactor = 1000 )

    def nontrivial (c: Constraint): Prop =
      Prop.exists (genFreeModel) { m: Model =>
        (!c.checkAll (m)) before { EcoreUtil.delete (m) } }

    "C1 can be violated"  in check { nontrivial (C1) }
    "C2 can be violated"  in check { nontrivial (C2) }
    "C2a can be violated" in check { nontrivial (C2a) }
    "C2b can be violated" in check { nontrivial (C2b) }
    "C3 can be violated"  in check { nontrivial (C3) }
    "C4 can be violated"  in check { nontrivial (C4) }
    "C5 can be violated"  in check { nontrivial (C5) }
    "C6 can be violated"  in check { nontrivial (C6) }
  }

}
