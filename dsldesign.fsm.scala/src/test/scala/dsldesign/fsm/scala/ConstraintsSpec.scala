// (c) dsldesign, wasowski, tberger
// These tests cannot depend on xtext parser, as we land a circular dependency
// then.  This is probably possible to sort-out but better avoided.
package dsldesign.fsm.scala

import dsldesign.scala.emf

class ConstraintsSpec
  extends org.scalatest.freespec.AnyFreeSpec
  with org.scalatest.matchers.should.Matchers
  with org.scalatest.BeforeAndAfter {

  import Constraints._

  before { dsldesign.fsm.FsmPackage.eINSTANCE.eClass }

  def loadFSM (name: String) =
    emf.loadFromXMI[dsldesign.fsm.Model] (s"../dsldesign.fsm/test-files/${name}.xmi")


  "C1" - {

    "positive tests" - {

      "single machine (test-00)" in {
        C1.checkAll (loadFSM ("test-00")) shouldBe true }

      "two machines (test-01.xmi)" in {
        C1.checkAll (loadFSM ("test-01")) shouldBe true }

      "two larger machines (test-02)" in {
        C1.checkAll (loadFSM ("test-02")) shouldBe true }

      "empty model (test-07)" in {
        C1.checkAll (loadFSM ("test-07")) shouldBe true }

    }

    "negative tests" - {

      "two machines (test-08)" in {
        C1.checkAll (loadFSM ("test-08")) shouldBe false }

      "five machines (test-09)" in {
        C1.checkAll (loadFSM ("test-09")) shouldBe false }
    }
  }



  for ( Cn <- List (C2 -> "C2", C2a -> "C2a", C2b -> "C2b") ) {

    Cn._2 - {

      val C = Cn._1

      "positive tests" - {

        "single machine (test-00)" in {
          C.checkAll (loadFSM ("test-00")) shouldBe true }

        "two machines (test-01)" in {
          C.checkAll (loadFSM ("test-01")) shouldBe true }

        "two larger machines (test-02)" in {
          C.checkAll (loadFSM ("test-02")) shouldBe true }

        "empty model (test-07)" in {
          C.checkAll (loadFSM ("test-07")) shouldBe true }

        "names across scopes (test-09)" in {
          C.checkAll (loadFSM ("test-09")) shouldBe true }

        "no states (test-11)" in {
          C.checkAll (loadFSM ("test-11")) shouldBe true }

        "CoffeeMachine.xmi" in {
          C.checkAll (loadFSM ("CoffeeMachine")) shouldBe true }
      }

      "negative tests" - {

        "two no-name states (test-04)" in {
          C.checkAll (loadFSM ("test-04")) shouldBe false }

        "two machines (test-08)" in {
          C.checkAll (loadFSM ("test-08")) shouldBe false }

        "one machine (test-14)" in {
          C.checkAll (loadFSM ("test-14")) shouldBe false }
      }
    }
  }



  "C3" - {

    "positive tests" - {

      "single machine (test-00)" in {
        C3.checkAll (loadFSM ("test-00")) shouldBe true }

      "two machines (test-01.xmi)" in {
        C3.checkAll (loadFSM ("test-01")) shouldBe true }

      "two larger machines (test-03)" in {
        C3.checkAll (loadFSM ("test-03")) shouldBe true }
    }

    "negative tests" - {

      "Example from Fig. 5.3 (test-10)" in {
        C3.checkAll (loadFSM ("test-10")) shouldBe false }

      "test-11" in {
        C3.checkAll (loadFSM ("test-11")) shouldBe false }
    }
  }



  "C4" - {

    "positive tests" - {

      "single machine (test-00)" in {
        C4.checkAll (loadFSM ("test-00")) shouldBe true }

      "two machines (test-01.xmi)" in {
        C4.checkAll (loadFSM ("test-01")) shouldBe true }

      "two larger machines with transitions (test-05)" in {
        C4.checkAll (loadFSM ("test-05")) shouldBe true }

    }

    "negative tests" - {

      "Cross machine transition (test-12)" in {
        C4.checkAll (loadFSM ("test-12")) shouldBe false }
    }
  }



  "C5" - {

    "positive tests" - {

      "single machine (test-00)" in {
        C5.checkAll (loadFSM ("test-00")) shouldBe true }

      "two machines (test-01.xmi)" in {
        C5.checkAll (loadFSM ("test-01")) shouldBe true }

      "two larger machines with transitions (test-05)" in {
        C5.checkAll (loadFSM ("test-05")) shouldBe true }

      "CoffeeMachine.xmi" in {
        C5.checkAll (loadFSM ("CoffeeMachine")) shouldBe true }
    }

    "negative tests" - {

      "test-03" in {
        C5.checkAll (loadFSM ("test-03")) shouldBe false }

      "Cross machine transition (test-06)" in {
        C5.checkAll (loadFSM ("test-06")) shouldBe false }

      "Cross machine transition (test-12)" in {
        C5.checkAll (loadFSM ("test-12")) shouldBe false }

      "Valid but with dead state (test-13)" in {
        C5.checkAll (loadFSM ("test-13")) shouldBe false }

      "test-14" in {
        C5.checkAll (loadFSM ("test-14")) shouldBe false }
    }
  }



  "C6" - {

    "positive tests" - {

      "single machine (test-00)" in {
        C6.checkAll (loadFSM ("test-00")) shouldBe true }

      "two machines (test-01.xmi)" in {
        C6.checkAll (loadFSM ("test-01")) shouldBe true }

      "two larger machines with transitions (test-05)" in {
        C6.checkAll (loadFSM ("test-05")) shouldBe true }

      "CoffeeMachine.xmi" in {
        C6.checkAll (loadFSM ("CoffeeMachine")) shouldBe true }
    }

    "negative tests" - {

      "Valid but non-deterministic (test-13)" in {
        C6.checkAll (loadFSM ("test-13")) shouldBe false }
    }
  }
}
