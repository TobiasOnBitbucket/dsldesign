// (c) mdsebook, wasowski, tberger

package mdsebook.pascal.scala

import scala.collection.JavaConverters._ // for natural access to EList
import dsldesign.scala.emf._
import mdsebook.pascal._

object Constraints {

  val invariants: List[Constraint] = List (

    inv[Triangle] { _ => true }

  )

}
