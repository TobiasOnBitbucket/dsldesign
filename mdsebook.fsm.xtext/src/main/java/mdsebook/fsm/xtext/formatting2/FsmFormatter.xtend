/*
 * generated by Xtext 2.19.0
 */
package mdsebook.fsm.xtext.formatting2

import com.google.inject.Inject
import dsldesign.fsm.FiniteStateMachine
import dsldesign.fsm.Model
import mdsebook.fsm.xtext.services.FsmGrammarAccess
import org.eclipse.xtext.formatting2.AbstractFormatter2
import org.eclipse.xtext.formatting2.IFormattableDocument

class FsmFormatter extends AbstractFormatter2 {
	
	@Inject extension FsmGrammarAccess

	def dispatch void format(Model model, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		for (finiteStateMachine : model.machines) {
			finiteStateMachine.format
		}
	}

	def dispatch void format(FiniteStateMachine finiteStateMachine, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		for (state : finiteStateMachine.states) {
			state.format
		}
	}
	
	// TODO: implement for State
}
