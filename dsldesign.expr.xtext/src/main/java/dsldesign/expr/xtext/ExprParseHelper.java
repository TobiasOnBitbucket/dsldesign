package dsldesign.expr.xtext;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.Resource.Diagnostic;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.xtext.testing.util.ParseHelper;
import org.eclipse.xtext.resource.IResourceFactory;
import org.eclipse.xtext.resource.XtextResourceSet;

import com.google.inject.Inject;
import com.google.inject.Provider;

public class ExprParseHelper<T extends EObject> extends ParseHelper<T> {

    @Inject
    private IResourceFactory resourceFactory;

    @Inject
    private Provider<XtextResourceSet> resourceSetProvider;

    public List<Diagnostic> parseToError(InputStream in, URI uriToUse, Map<?, ?> options, ResourceSet resourceSet) {
        Resource resource = resourceFactory.createResource(uriToUse);
        resourceSet.getResources().add(resource);
        try {
            resource.load(in, options);
            if (resource.getErrors().size() > 0) {
                return resource.getErrors();
            }
            throw new IllegalStateException();
        } catch (IOException e) {
            throw new WrappedException(e);
        }
    }

    public List<Diagnostic> parseToError(CharSequence text) throws Exception {
        return parseToError(text, resourceSetProvider.get());
    }

    public List<Diagnostic> parseToError(CharSequence text, ResourceSet resourceSetToUse) throws Exception {
        return parseToError(getAsStream(text), computeUnusedUri(resourceSetToUse), null, resourceSetToUse);
    }

    public List<Diagnostic> parseToError(CharSequence text, URI uriToUse, ResourceSet resourceSetToUse) throws Exception {
        return parseToError(getAsStream(text), uriToUse, null, resourceSetToUse);
    }

}