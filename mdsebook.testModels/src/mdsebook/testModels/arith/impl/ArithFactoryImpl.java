/**
 */
package mdsebook.testModels.arith.impl;

import mdsebook.testModels.arith.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ArithFactoryImpl extends EFactoryImpl implements ArithFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ArithFactory init() {
		try {
			ArithFactory theArithFactory = (ArithFactory)EPackage.Registry.INSTANCE.getEFactory(ArithPackage.eNS_URI);
			if (theArithFactory != null) {
				return theArithFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ArithFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArithFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ArithPackage.PLUS_EXP: return createPlusExp();
			case ArithPackage.SUB_EXP: return createSubExp();
			case ArithPackage.CONST_INT: return createConstInt();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PlusExp createPlusExp() {
		PlusExpImpl plusExp = new PlusExpImpl();
		return plusExp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubExp createSubExp() {
		SubExpImpl subExp = new SubExpImpl();
		return subExp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstInt createConstInt() {
		ConstIntImpl constInt = new ConstIntImpl();
		return constInt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArithPackage getArithPackage() {
		return (ArithPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ArithPackage getPackage() {
		return ArithPackage.eINSTANCE;
	}

} //ArithFactoryImpl
