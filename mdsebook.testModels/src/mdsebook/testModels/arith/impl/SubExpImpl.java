/**
 */
package mdsebook.testModels.arith.impl;

import mdsebook.testModels.arith.ArithPackage;
import mdsebook.testModels.arith.SubExp;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sub Exp</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SubExpImpl extends BinExpImpl implements SubExp {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubExpImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ArithPackage.Literals.SUB_EXP;
	}

} //SubExpImpl
