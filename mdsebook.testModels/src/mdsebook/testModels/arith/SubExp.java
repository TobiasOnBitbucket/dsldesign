/**
 */
package mdsebook.testModels.arith;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sub Exp</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mdsebook.testModels.arith.ArithPackage#getSubExp()
 * @model
 * @generated
 */
public interface SubExp extends BinExp {
} // SubExp
