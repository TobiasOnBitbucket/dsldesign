/*
   Copyright 2020-2021 Andrzej Wasowski and Thorsten Berger

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package dsldesign.scala

import scala.language.implicitConversions
import scala.jdk.CollectionConverters._
import scala.reflect.ClassTag
import org.eclipse.emf.common.util.{Diagnostic, EList, EMap, URI}
import org.eclipse.emf.ecore.impl.EStringToStringMapEntryImpl
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.util.{Diagnostician, EcoreUtil}
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import org.eclipse.emf.ecore.{EClass, EDataType, EObject, EOperation, EValidator}
import org.eclipse.ocl.pivot.utilities.PivotConstants.OCL_DELEGATE_URI_PIVOT

import java.io.File

package object emf {

  // Initialize OCL (this is needed for loading files that contain Pivot OCL annotations
  org.eclipse.ocl.xtext.essentialocl.EssentialOCLStandaloneSetup.doSetup

  // EMF iterators to Scala iterators

  implicit def asScalaTreeIteratorConverter[A]
    (x: org.eclipse.emf.common.util.TreeIterator[A])
      : Iterator[A] = x.asScala

  implicit def asScalaTuple( x: EStringToStringMapEntryImpl ): (String, String) = ( x.getKey, x.getValue )

  implicit def asScalaMap[A,B]( x: EMap[A,B] ): Map[A,B] = x.map().asScala.toMap

  // A before operator that allows to avoid introducing local name bindings (as => in Xtend and before in scala)

  implicit class EMFConvenienceOps[T] (o: T) {

    def before (f: T => Unit): T =
      { f(o); o }

    def before (closure: => Unit): T =
     { closure; o }
  }

  // An implies operator for more elegant constraints in Scala

  implicit class ImpliesExtension (a: Boolean) {

    def implies(b : => Boolean) = !a || b

  }


  // Add binary and ternary quantifiers

  implicit class QuantifierExtensions[+A]
    (c: scala.collection.Iterable[A]) {

    def forall (p: (A, A) => Boolean): Boolean =
      c forall { a1 =>
        c forall { a2 => p (a1, a2) } }

    def forall (p: (A, A, A) => Boolean): Boolean =
      c forall ( (a1: A, a2: A) =>
        c forall { a3 => p (a1, a2, a3) } )

    def forAllDifferent (p: (A, A) => Boolean): Boolean =
      c forall { (a1: A, a2: A) =>
        a1 != a2 implies p (a1, a2) }

    def forAllDifferent (p: (A, A, A) => Boolean): Boolean =
      c forall { (a1: A, a2: A, a3: A) =>
        (a1 != a2  && a2 != a3 && a1 != a3) implies p (a1, a2, a3) }

    def exist (p: (A, A) => Boolean): Boolean =
      c exists { a1 => c exists { a2 => p (a1, a2) } }

    def exist (p: (A, A, A) => Boolean): Boolean =
      c exist ( (a1: A, a2: A) => c exists { a3 => p (a1, a2, a3) } )

    def existDifferent (p: (A, A) => Boolean): Boolean =
      c exist { (a1: A, a2: A) => a1 != a2 && p (a1, a2) }

    def existDifferent (p: (A, A, A) => Boolean): Boolean =
      c exist { (a1: A, a2: A, a3: A) =>
        (a1 != a2  && a2 != a3 && a1 != a3) && p (a1, a2, a3) }
  }

  // a restricted implicit conversion to directly inject the above into Java collections

  implicit class QuantifierExtensionsJava[+A, +CC[_], +C]
    (jc: org.eclipse.emf.common.util.EList[A])
      extends QuantifierExtensions[A] (jc.asScala)

  /** Support for constraints */

  sealed trait Constraint {

    def check (o: EObject) :Boolean

    /** Check the constraint on all the objects of the iterator */
    def checkAll (content: Iterator[EObject]) :Boolean =
      content forall { this.check _ }

    /**
     * Check the constraint on all objects in a model that owes the given
     * object 'o'.
     */
    def checkAll (o: EObject): Boolean =
      check (o) &&
        checkAll (EcoreUtil.getRootContainer (o).eAllContents)


    // These methods can help to filter constraints by types to avoid executing
    // them for performance reasons, or to avoid message clutter

    def appliesTo (o :EObject) :Boolean
    def appliesToAny (os :List[EObject]) :Boolean

  }

  final case class Inv[T <: EObject] private (
    p: T => Boolean, ct: ClassTag[T]) extends Constraint
  {

    // Use this to build more complex constraints from smaller pieces

    def &&[S <: EObject] (that: Inv[S])
      (implicit ct :ClassTag[T with S]) :Inv[T with S] =
      Inv[T with S] (x => p(x) && that.p(x), ct)

    // Since invariants are "invariant" (no pun intendeted), to follow the
    // ClassTag[T] implementation, we add this method to help up casting a bit:
    // The method implements the contravariant casting, so specializing the
    // context type is allowed.

    def inCtx[S <: T] (implicit cts: ClassTag[S]): Inv[S] =
      Inv[S] (p,cts)

    // Use this method to check the constraint on any ECore instance object.
    // If the object is not in the context, the constraint will pass (hold)
    // vacously.

    def check (o: EObject): Boolean =
      !appliesTo (o) || p(o.asInstanceOf[T])

    def appliesTo (o: EObject) :Boolean =
      instanceOf[T] (o) (ct)

    def appliesToAny (os: List[EObject]): Boolean =
      os.exists (appliesTo _)

  }

  private object Inv {

    def instanceOf[T] (o: Any) (ct :ClassTag[T]) :Boolean =
      ct.runtimeClass.isInstance(o)

    // A convenience constructor (also protecting the actual class
    // representation for future evolution)

    def inv[T <: EObject] (p: T => Boolean) (implicit ct : ClassTag[T]) :Inv[T] =
      Inv[T] (p,ct)

  }

  /**
   * Support for Meta-model validation (not for Scala constraints, but for
   * constraints embedded in the Ecore model.
   *
   * It can validate OCL constraints and the meta-model constraints.
   */

  private class DummyDelegate extends EValidator.ValidationDelegate {

    override def validate (
      eClass: EClass,
      eObject: EObject,
      context: java.util.Map[java.lang.Object, java.lang.Object],
      invariant: EOperation,
      expression: java.lang.String): Boolean = true

    override def validate(
      eClass: EClass,
      eObject: EObject,
      context: java.util.Map[java.lang.Object, java.lang.Object],
      constraint: java.lang.String,
      expression: java.lang.String): Boolean = true

    override def validate(
      eDataType: EDataType,
      value: java.lang.Object,
      context: java.util.Map[java.lang.Object, java.lang.Object],
      constraint: java.lang.String,
      expression: java.lang.String): Boolean = true
  }

  private val dummyDelegate = new DummyDelegate

  // This function is not thread safe (it changes the validator globably)
  def validate (root: EObject, ocl: Boolean = true): Option[Seq[String]] =
    EValidator.ValidationDelegate.Registry.INSTANCE.synchronized {

      val oclDelegate =
        EValidator.ValidationDelegate.Registry.INSTANCE.get (OCL_DELEGATE_URI_PIVOT)

      // Rumours that this will behave differently in Eclipse and outside:
      // https://www.eclipse.org/forums/index.php/t/129584/
      if (!ocl)
        EValidator.ValidationDelegate.Registry.INSTANCE
          .put (OCL_DELEGATE_URI_PIVOT, dummyDelegate)

      val diagnostic = Diagnostician.INSTANCE.validate (root)

      if (!ocl)
        EValidator.ValidationDelegate.Registry.INSTANCE
          .put (OCL_DELEGATE_URI_PIVOT, oclDelegate)

      def fmt (severity: Int): String = {
        val severities =
          List ("CANCEL").filter { _ => (Diagnostic.CANCEL & severity) != 0 }::
          List ("ERROR").filter { _ => (Diagnostic.ERROR & severity) != 0 }::
          List ("INFO").filter { _ => (Diagnostic.INFO & severity) != 0 }::
          List ("OK").filter { _ => (Diagnostic.OK & severity) != 0 }::
          List ("WARNING").filter { _ => (Diagnostic.WARNING & severity) != 0 }::
          Nil
        severities.flatten.mkString ("|")
      }

      Option (diagnostic)
        .filter { _.getSeverity != Diagnostic.OK }
        .map { _.getChildren
                .asScala
                .map { d =>
                  s"${fmt (d.getSeverity)}: ${d.getMessage} [${d.getException}] <${d.getData}>" }
                .toSeq
        }
    }

  // Support for transformations

  trait Trafo

  trait InPlaceTrafo[T] extends Trafo {

    def run (m: T) :Unit

  }

  trait InPlaceParameterizedTrafo[T,Parameter] extends Trafo {

    val p:Parameter

    def run (m: T) :Unit

  }

  trait CopyingTrafo[In,Out] extends Trafo {

    def run (m: In) :Out

  }

  trait CopyingParameterizedTrafo[In,Parameter,Out] extends Trafo {

    val p:Parameter

    def run (m: In) :Out

  }

  // Delegations for easier importing

  def inv[T <: EObject] (check: T => Boolean) (implicit ct: ClassTag[T]) :Inv[T] =
    Inv.inv[T] (check) (ct)

  def instanceOf[T] (o: Any) (implicit ct: ClassTag[T]) :Boolean =
    Inv.instanceOf[T] (o) (ct)


  // I/O

  def loadFromXMI[T] (file: String): T =
    loadAllFromXMI[T] (file) (0)



  def loadAllFromXMI[T] (file: String): Seq[T] =
    (new ResourceSetImpl)
      .getResource (URI.createURI (file), true)
      .getContents
      .asScala
      .toSeq
      .asInstanceOf[Seq[T]]

  def saveAsXMI (file: String) (root: EObject): Unit =
    saveAsXMI( new File( file ) ) ( root )

  def saveAsXMI (file: File) (root: EObject): Unit = {
    // need absolute file path to avoid relative paths in class references, see https://www.eclipse.org/forums/index.php/t/171122/
    val res = (new ResourceSetImpl).createResource (URI createFileURI file.getAbsolutePath)
    res.getContents add root
    res save java.util.Collections.EMPTY_MAP

  }


}
