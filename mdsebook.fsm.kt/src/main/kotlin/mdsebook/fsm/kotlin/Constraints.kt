// Copyright 2020 Andrzej Wasowski and Thorsten Berger
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dsldesign.fsm.kotlin

import dsldesign.fsm.FiniteStateMachine
import dsldesign.fsm.Model

import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl

internal object Main {

    init {
      Resource.Factory.Registry.INSTANCE
            .extensionToFactoryMap["xmi"] = XMIResourceFactoryImpl ()
      dsldesign.fsm.FsmPackage.eINSTANCE.eClass()
    }
    val resourceSet: ResourceSet = ResourceSetImpl ()

    val C2: (FiniteStateMachine) -> Boolean = { 
      it.states.all { s1 ->
      it.states.all { s2 -> s1==s2 || s1.name!=s2.name } }
    }

    // Load a test case
    fun loadInstance(fname: String): Model {
        println ("Loading '$fname'...")
        return resourceSet
          .getResource(URI.createURI (fname), true)
          .contents[0] as Model
    }

    // Check constraint C2 on all machines in the file designated by fname
    fun checkAll (fname: String) {
        val results = 
          loadInstance ("../dsldesign.fsm/test-files/${fname}.xmi")
            .machines
            .map { Pair(it, C2 (it)) }

        if (results.all { it.second })
            println ("C2 is satisfied by all machines in ${fname}.xmi")
        else results
          .filter { !it.second }
          .forEach { println ("C2 VIOLATED by machine '${it.first.name}' in ${fname}.xmi") }
    }

    @JvmStatic
    fun main(args: Array<String>) {

        // File names we want to test (same as test cases for Scala)
        val testModels = 
          listOf("test-00", "test-01", "test-02", "test-07", "test-09",
                 "test-11", "test-04", "test-14", "test-08")

        testModels.forEach { checkAll (it) }
    }
}
