// (c) dsl.design, wasowski, tberger

package dsldesign.featuremodels1.scala

import scala.collection.JavaConverters._ // for natural access to EList
import dsldesign.scala.emf._
import dsldesign.featuremodels1._

object Constraints {

  val invariants: List[Constraint] = List (

    inv[Model1] { m => true }

  )

}
