// (c) dsldesign, wasowski, tberger
// An in-place transformation implemented directly in Scala (no special
// frameworks).  Translates a finite state machine to a Petri net.  There is
// presently no runner for this example.

package dsldesign.featuremodels1.scala.transforms

import dsldesign.featuremodels1.{Feature1, Group1, Model1, OrGroup1}
import dsldesign.featuremodels2.{Feature2, Group2, Model2}
import dsldesign.scala.emf._
import dsldesign.{featuremodels2 => fm2}

import scala.jdk.CollectionConverters._

object FeatureModel1ToFeatureModel2 extends CopyingTrafo[Model1, Model2] {

  val fm2Factory = fm2.Featuremodels2Factory.eINSTANCE

  def convertGroup(self: Group1): Group2 = {
    if (self.isInstanceOf[OrGroup1])
      fm2Factory.createOrGroup2()
    else
      fm2Factory.createXorGroup2()
  } before {
    _.getMembers addAll self.getMembers.asScala.map(convertFeature).asJava
  }

  def convertFeature(self: Feature1): Feature2 =
    fm2Factory.createFeature2() before { f =>
      f setName self.getName
      f.getSolitarySubfeatures addAll (
        self.getSubfeatures.asScala.filter(f =>
          self.getGroups.asScala.forall( ! _.getMembers.asScala.exists(m => m == f) ) ).
          map(convertFeature).asJava )
      f.getGroups addAll( self.getGroups.asScala.map( convertGroup ).asJava )
    }

  def convertModel(self: Model1): Model2 =
    fm2Factory.createModel2 before { m =>
      m setName self.getName
      m.setRoot(convertFeature(self.getRoot))
    }

  override def run(self: Model1): Model2 =
    fm2Factory.createModel2 before {
      _.setRoot(convertFeature(self.getRoot))
    }

}