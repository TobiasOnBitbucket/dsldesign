// (c) dsldesign, wasowski, tberger
// Example constraints implemented for the FSM models in Scala
// This is the main runner for the constraints example
// Run using 'sbt run'
package dsldesign.featuremodels1.scala.transforms

import dsldesign.featuremodels1.{FeatureModels1Package, Model1}
import dsldesign.featuremodels2.Featuremodels2Package
import dsldesign.scala.emf
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl


object FeatureModel1ToFeatureModel2Main extends App {

  // register a resource factory for XMI files
  Resource.Factory.Registry.INSTANCE.
    getExtensionToFactoryMap.put("xmi", new XMIResourceFactoryImpl)

  // register the package magic (impure)
  FeatureModels1Package.eINSTANCE.eClass
  Featuremodels2Package.eINSTANCE.eClass

  // load the fsm model
  val featuremodel1:Model1 = emf.loadFromXMI( "dsldesign.featuremodels/test-files/test-00.xmi" )

  // we run the transformation
  val featuremodel2 = FeatureModel1ToFeatureModel2.run( featuremodel1 )

  // save the petrinet model
  emf.saveAsXMI ( "dsldesign.featuremodels/test-files/test-00-transformed-by-Scala.xmi") ( featuremodel2 )

}
