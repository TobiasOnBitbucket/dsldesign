﻿(*
   Copyright 2020 Andrzej Wasowski and Thorsten Berger
  
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
  
       http://www.apache.org/licenses/LICENSE-2.0
  
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*)

open System
open DslDesign.Fsm

// The meta-model registration with NMF happens in the imported C# dll.  This
// is why this file (seemingly) does not register a meta-mode with NMF.
let C2: IFiniteStateMachine -> bool = fun m -> query {
    for s1 in m.States do
    for s2 in m.States do
      where (s1 <> s2)
      all (s1.Name <> s2.Name) 
}


// If you find the relational style quirky, or if you dislike how the
// use of .All mismatches the syntactic style of LINQ above, we bring
// the following non-LINQ version for you:
let  C2a: IFiniteStateMachine -> bool = fun m ->
  m.States |> Seq.toList |> List.forall (fun s1 ->
  m.States |> Seq.toList |> List.forall (fun s2 -> 
    s1 = s2 || s1.Name <> s2.Name) )

 
let repository = new NMF.Models.Repository.ModelRepository ()


// Load a test case
let loadInstance (fname: string): IModel = 
  printfn "Loading '%s'..." fname
  (repository.Resolve fname).RootElements.Item (0) :?> IModel



// Check constraint C2 on all machines in the file designated by fname
let CheckAll (fname: string): unit = 
  let model = loadInstance ("../dsldesign.fsm/test-files/" + fname + ".xmi") 
  let results = 
    model.Machines |> Seq.toList |> List.map (fun m -> (m, C2 m))

  if results |> List.forall snd then
    printfn "C2 is satisfied by all machines in %s.xmi" fname
  else 
    results
      |> List.filter (fun r -> not (snd r))
      |> List.map (fun r -> (fst r).Name) 
      |> List.iter (fun name -> 
         printfn "C2 VIOLATED by machine '%s' in %s.xmi" name fname)



let testModels = [ "test-00"; "test-01"; "test-02"; "test-07"; "test-09";
                   "test-11"; "test-04"; "test-14"; "test-08" ]

printfn "F# example in dsldesign.fsm.fs/"
List.iter CheckAll testModels
