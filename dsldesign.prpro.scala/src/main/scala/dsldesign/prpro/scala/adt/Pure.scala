// (c) dsldesign, wasowski, tberger
package dsldesign.prpro.scala.adt

object Pure { // NB. Almost pure

  import Types._

  trait Typeable {

    private var ty: Ty = null 

    def getTy: Ty = this.ty

    private[adt] def setTy (ty: Ty): Ty = { 
      this.ty = ty
      ty 
    }

  }

  sealed abstract trait Expression extends Typeable
  sealed abstract trait Declaration extends NamedElement
  sealed abstract trait Distribution extends Expression {
    val observed: Option[Expression]
  }

  case class Let (
    name: String,
    value: Expression
  ) extends Declaration

  case class Data (
    name: String, 
    ty: Ty) 
  extends Declaration

  case class Uniform (
    lo: Expression,
    hi: Expression, 
    observed: Option[Expression] = None
  ) extends Distribution

  case class Normal (
    mu: Expression,
    sigma: Expression,
    observed: Option[Expression] = None
  ) extends Distribution

  case class BExpr (
    left: Expression, 
    operator: Operator,
    right: Expression
  ) extends Expression

  case class VarRef (name: String) extends NamedElement with Expression
  case class CstI (value: Int) extends Expression
  case class CstF (value: Double) extends Expression

  sealed abstract trait Operator
  case object Plus extends Operator
  case object Minus extends Operator
  case object Mult extends Operator
  case object Div extends Operator

  type Model = List[Declaration]
}
