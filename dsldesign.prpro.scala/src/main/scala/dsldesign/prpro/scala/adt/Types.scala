// (c) dsldesign, wasowski, tberger
package dsldesign.prpro.scala.adt

object Types {

  import Errors._

  sealed abstract trait Ty  {

    def isVector = false
    def isSimple: Boolean = false

    def isSubTypeOf (t: Ty): Boolean =
      (this, t) match {

        // (SSimple)
        case (t1: SimpleTy, t2: SimpleTy) =>
          t1.superTys.contains (t2)

        // (SVect)
        case (VectorTy (l1, t1), VectorTy (l2, t2)) =>
          l1 >= l2 && (t1 isSubTypeOf t2)

        // (SDist-1)
        case (DistribTy (t1), DistribTy (t2)) =>
          t1 isSubTypeOf t2

        // (SDist-2)
        case (DistribTy (t1), t2: SimpleTy) =>
          t1 isSubTypeOf t2

        case _ => false
      }

    def isSuperTypeOf (t: Ty): Boolean =
      t isSubTypeOf this
  }

  sealed abstract trait SimpleTy extends Ty {

    override def isSimple: Boolean = true

    // Represent the reduced subtyping relation
    // (overload for concrete types)

    protected def superTy: Set[SimpleTy] = Set()

    // Reflexive transitive closure of the above

    private def _superTys (ts: Set[SimpleTy]): Set[SimpleTy] = {

      val result: Set[SimpleTy] = ts ++ (ts.flatMap (_.superTy))
      if (result == ts)
        result
      else
        _superTys (result)
    }

    lazy val superTys = _superTys (Set (this))

  }

  case object IntTy extends SimpleTy {
    protected override def superTy = Set (FloatTy)
  }

  case object NonNegIntTy extends SimpleTy {
    protected override def superTy = Set (IntTy, NonNegFloatTy)
  }

  case object NatTy extends SimpleTy {
    protected override def superTy = Set (NonNegIntTy, PosFloatTy)
  }

  case object FloatTy extends SimpleTy

  case object NonNegFloatTy extends SimpleTy {
    protected override def superTy = Set (FloatTy)
  }

  case object PosFloatTy extends SimpleTy {
    protected override def superTy = Set (NonNegFloatTy)
  }

  case object ProbTy extends SimpleTy {
    protected override def superTy = Set (NonNegFloatTy)
  }

  case object PosProbTy extends SimpleTy {
    protected override def superTy = Set (PosFloatTy, ProbTy)
  }

  sealed abstract trait CompositeTy extends Ty

  sealed case class VectorTy (len: Int, elemTy: SimpleTy) extends CompositeTy {
    override def isVector = true
  }

  sealed case class DistribTy (outcomeTy: SimpleTy) extends CompositeTy

  private[adt] val topologicallySortedSimpleTys =
    List (NatTy, PosProbTy, NonNegIntTy, PosFloatTy, ProbTy, IntTy,
      NonNegFloatTy, FloatTy)

  def lub (t1: SimpleTy, t2: SimpleTy): SimpleTy =
    topologicallySortedSimpleTys
      .find { t => (t isSuperTypeOf t1) && (t isSuperTypeOf t2) }
      .getOrElse (null) // find always succeeds (an offensive null)

  def lub (t1: Ty, t2: Ty): Result[Ty] =
    (t1, t2) match {
      case (ty1: SimpleTy, ty2: SimpleTy) =>
        Right (lub (ty1, ty2))

      case (VectorTy (len1, ty1), VectorTy (len2, ty2)) =>
        Right (VectorTy (len1 min len2, lub (ty1, ty2)))

      case (DistribTy (ty1), DistribTy (ty2)) =>
        Right (DistribTy (lub (ty1, ty2)))

      case (DistribTy (ty1), ty2: SimpleTy) =>
        Right (lub (ty1, ty2))

      case (ty1: SimpleTy, DistribTy (ty2)) =>
        Right (lub (ty1, ty2))

      case _ => Left (s"An attempt to unify incompatible types: $t1, $t2")

    }

}
