// (c) dsldesign, wasowski, tberger
package dsldesign.prpro.scala.adt

object TypeChecker {

  import Pure._
  import Types._
  import Errors._
  import Either.cond

  type TypingEnvironment = Map[String, Ty]

  val emptyTypingEnvironment: TypingEnvironment = Map()

  implicit class ResultExt[+T] (res: Result[T]) {
    def failIf (p: T => Boolean, msg: T => String): Result[T] =
      res.flatMap { t => if (p(t)) Left (msg (t)) else res }

    def ensure (p: T => Boolean, msg: T => String): Result[T] =
      res.flatMap { t => if (p(t)) res else Left (msg (t)) }
  }

  def tyCheck (tenv: TypingEnvironment, model: Model): Result[TypingEnvironment] =
    model.foldLeft[Result[TypingEnvironment]] (Right (tenv)) { tyCheck _ }

  def tyCheck (res: Result[TypingEnvironment], decl: Declaration)
    : Result[TypingEnvironment] =
      res.flatMap { tyCheck (_,decl) }

  def tyCheck (tenv: TypingEnvironment, decl: Declaration)
    : Result[TypingEnvironment] =
    decl match {

      case Data (name, ty) =>
        tenv.get (name) match {
          case Some (_) =>
            Left (s"Identifier '$name' has already been defined!")
          case None =>
            Right (tenv + (name -> ty))
        }

      case Let (name, value) =>
        tyCheck (tenv, value)
          .ensure (
            t1 => tenv.get (name).isEmpty,
            t1 => s"'$name' has already been defined!" )
          .map { t1 => tenv + (name -> t1) }
    }

  def tyCheck (tenv: TypingEnvironment, expr: Expression): Result[Ty] =
    expr match {

      case BExpr (left, operator, right) =>
        for {
          t1 <- tyCheck (tenv, left)
          t2 <- tyCheck (tenv, right)
          t  <- lub (t1,t2)    // incorrect for minus and other operators
        } yield expr.setTy (t) // we have an exercise to fix this

      case CstI (n) if n > 0 =>
        Right (expr.setTy (NatTy))

      case CstI (0) =>
        Right (expr.setTy (NonNegIntTy))

      case CstI (_) =>
        Right (expr.setTy (IntTy))

      case CstF (0.0) =>
        Right (expr.setTy (ProbTy))

      case CstF (x) if x > 0.0 && x <= 1.0 =>
        Right (expr.setTy (PosProbTy))

      case CstF (x) if x > 1.0 =>
        Right (expr.setTy (PosFloatTy))

      case CstF (_) =>
        Right (expr.setTy (FloatTy))

      case VarRef (name) =>
        tenv.get (name)
          .toRight (s"Undeclared variable '${name}'")
          .map (expr.setTy)

      case Normal (mu, sigma, oobserved) =>
        for {
          _ <- tyCheck (tenv, mu).ensure (
            t => t.isSubTypeOf (FloatTy),
            t => s"Need a sub-type FloatTy for 'mu' but got '$t'" )

          _ <- tyCheck (tenv, sigma).ensure (
            t => t.isSubTypeOf (NonNegFloatTy),
            t => s"Need a sub-type of NonNegFloatTy for 'sigma' but got '$t'" )

          _ <- oobserved
               .map { observed =>
                 tyCheck (tenv, observed)
                 .ensure (
                   tob => tob.isSubTypeOf (VectorTy (1, FloatTy)),
                   tob => s"Need a vector of Floats for observed data but got '$tob'"
                 )
               }.getOrElse (Right (VectorTy (1, FloatTy)))

        } yield expr.setTy (DistribTy (FloatTy))

      case Uniform (lo, hi, oobserved) =>
        for {
          t0 <- tyCheck (tenv, lo)
          t1 <- tyCheck (tenv, hi)
          t <- lub (t0, t1) match {
             case Right (t: SimpleTy) =>
               Right (t)
             case Right (DistribTy (t)) =>
               Right (t)
             case Right (t) =>
               Left (s"Endpoint types must be sub-types of simple type (got '$t')")
             case Left (msg) =>
               Left (msg)
          }

          tob <- oobserved.map { observed =>
            tyCheck (tenv, observed).ensure (
              tob => tob.isSubTypeOf (VectorTy (1, t)),
              tob => s"Need a vector of '$t' for observed data but got '$tob'"
            )
          }.getOrElse (Right (VectorTy (1, t)))

        } yield expr.setTy (DistribTy (t))

    }

}
