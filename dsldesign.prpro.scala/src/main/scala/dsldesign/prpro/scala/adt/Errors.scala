// (c) dsldesign, wasowski, tberger
package dsldesign.prpro.scala.adt

object Errors {

  type ErrMessage = String
  type Result[+T] = Either[ErrMessage,T] 

}
