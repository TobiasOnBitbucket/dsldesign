// (c) dsldesign, wasowski, tberger
package dsldesign.prpro.scala.adt

import org.scalacheck.Gen
import org.scalacheck.Arbitrary
import org.scalacheck.Arbitrary.arbitrary

import Pure._
import Types._

object Generators {

  // Generators for the type language

  implicit lazy val arbTy: Arbitrary[Ty] =
    Arbitrary (Gen.oneOf (arbitrary[SimpleTy], arbitrary[CompositeTy]))

  implicit lazy val arbSimpleTy: Arbitrary[SimpleTy] =
    Arbitrary (Gen.oneOf (Types.topologicallySortedSimpleTys))

  implicit lazy val arbCompositeTy: Arbitrary[CompositeTy] =
    Arbitrary {
      Gen.lzy {
        Gen.oneOf (arbitrary[VectorTy], arbitrary[DistribTy])
      }
    }

  implicit lazy val arbVectorTy: Arbitrary[VectorTy] =
    Arbitrary {
        arbitrary[(Int,SimpleTy)]
          .map { case (len, elemTy) =>
              VectorTy (Math.abs (len % 1000) + 1, elemTy) }
    }

  implicit lazy val arbDistribTy: Arbitrary[DistribTy] =
    Arbitrary { arbitrary[SimpleTy].map { ty => DistribTy (ty) } }

  // Generators of models (of abstract syntax)

  implicit lazy val arbExpression: Arbitrary[Expression] =
    Arbitrary {
      Gen.lzy {
        Gen.frequency (
          1 -> arbitrary[Distribution],
          1 -> arbitrary[BExpr],
          2 -> arbitrary[VarRef],
          2 -> arbitrary[CstI],
          2 -> arbitrary[CstF]
        )
      }
    }

  implicit lazy val arbDeclaration: Arbitrary[Declaration] =
    Arbitrary (Gen.oneOf (arbitrary[Data], arbitrary[Let]))

  implicit lazy val arbDistribution: Arbitrary[Distribution] =
    Arbitrary {
      Gen.oneOf (
          arbitrary[Uniform],
          arbitrary[Normal]
      )
    }

  implicit lazy val arbLet: Arbitrary[Let] =
    Arbitrary {
      arbitrary[(String,Expression)]
        .map { case (name,value) => Let (name,value) }
    }

  implicit lazy val arbData: Arbitrary[Data] =
    Arbitrary {
      arbitrary[(String, Ty)]
        .map { case (name, ty) => Data (name, ty)}
    }

  implicit lazy val arbUniform: Arbitrary[Uniform] =
    Arbitrary {
      arbitrary[(Expression, Expression)]
        .map { case (lo,hi) => Uniform (lo, hi) }
    }

  implicit lazy val arbNormal: Arbitrary[Normal] =
    Arbitrary {
      arbitrary[(Expression, Expression)]
        .map { case (mu, sigma) => Normal (mu, sigma) }
    }

  implicit lazy val arbBExpr: Arbitrary[BExpr] =
    Arbitrary {
      arbitrary[(Expression,Operator,Expression)]
        .map { case (left, operator, right) =>
          BExpr (left, operator, right) }
    }

  implicit lazy val arbVarRef: Arbitrary[VarRef] =
    Arbitrary {
      arbitrary[String]
        .map { name => VarRef (name) }
    }

  implicit lazy val arbCstI: Arbitrary[CstI] =
    Arbitrary {
      arbitrary[Int]
        .map { value => CstI (value) }
    }

  implicit lazy val arbCstF: Arbitrary[CstF] =
    Arbitrary {
      arbitrary[Double]
        .map { value => CstF (value) }
    }

  implicit lazy val arbOperator: Arbitrary[Operator]  =
    Arbitrary { Gen.oneOf (Plus, Mult, Div, Minus) }

  implicit lazy val arbModel: Arbitrary[Model] =
    Arbitrary {
      Gen.chooseNum (0, 500)
        .flatMap { len => Gen.listOfN (len, arbitrary[Declaration]) }
    }

}
