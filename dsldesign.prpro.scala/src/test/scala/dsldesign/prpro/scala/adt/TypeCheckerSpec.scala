// (c) dsldesign, wasowski, tberger
package dsldesign.prpro.scala.adt

import Generators._
import Pure._
import TypeChecker._
import Types._

class TypeCheckerSpec
  extends org.scalatest.freespec.AnyFreeSpec
  with org.scalatest.matchers.must.Matchers
  with org.scalatest.prop.Configuration
  with org.scalatestplus.scalacheck.ScalaCheckPropertyChecks
{

  val tenv0 = emptyTypingEnvironment

  "Test cases " -  {

    "00 Example 1 shall typeCheck" in {

        val m: Model = List (

          Data ("x", PosFloatTy),
          Data ("y", PosFloatTy),

          Let ("β0", Uniform (CstI (-200), CstI(200))),

          Let ("β1", Uniform (CstI (-200), CstI(200))),

          Let ("σ",
            Uniform (CstF (0), CstI  (100)) ),

          Let ("y0",
            Normal(
              mu = BExpr (
                BExpr (VarRef ("β1"), Mult, VarRef ("x")),
                Plus,
                VarRef ("β0")),
             sigma = VarRef ("σ") )
          )
        )

        tyCheck (tenv0, m) must be (Symbol ("Right"))
    }

    "01 Regression for Uniform (dist, dist)" in {
      val e = Uniform (
        lo = Normal (CstF (0.0), CstF (0.1)),
        hi = Normal (CstF (42.0), CstF (0.1)))
      tyCheck (tenv0, e) must be (Right (DistribTy (FloatTy)))
    }

    "02 Regression name conclicts (Let; Data)" in {
        val m: Model = List (
          Let ("x", Uniform (CstI (-200), CstI(200))),
          Data ("x", PosFloatTy))
        tyCheck (tenv0, m) must be (Symbol ("Left"))
    }

    "03 Regression name conclicts (Data; Data)" in {
        val m: Model = List (
          Data ("x", PosFloatTy),
          Data ("x", PosFloatTy))
        tyCheck (tenv0, m) must be (Symbol ("Left"))
    }

    "04 Regression name conclicts (Let; Let); Data)" in {
        val m: Model = List (
          Let ("x", Uniform (CstI (-200), CstI(200))),
          Let ("x", Uniform (CstI (-100), CstI(100))))
        tyCheck (tenv0, m) must be (Symbol ("Left"))
    }

  }

  "Typechecker 'fuzzing'" - {

    "A 'fuzz' test for CstI" in {
      forAll ("cst") {
         (cst: CstI) =>
           noException must be thrownBy tyCheck (tenv0, cst)
      }
    }

    "A 'fuzz' test for VarRef" in {
      forAll ("v") {
         (v: VarRef) =>
           noException must be thrownBy tyCheck (tenv0, v)
      }
    }

    "A 'fuzz' test for Uniform" in {
      forAll ("d") {
         (d: Uniform) =>
           noException must be thrownBy tyCheck (tenv0, d)
      }
    }

    "A 'fuzz' test for Normal" in {
      forAll ("d") {
         (d: Normal) =>
           noException must be thrownBy tyCheck (tenv0, d)
      }
    }

    "A 'fuzz' test for BExpr" in {
      forAll ("expr") {
         (expr: BExpr) =>
           noException must be thrownBy tyCheck (tenv0, expr)
      }
    }

    "A 'fuzz' test for Expression" in {
      forAll ("expr") {
         (expr: Expression) =>
           noException must be thrownBy tyCheck (tenv0, expr)
      }
    }

    "A 'fuzz' test for Model" in {
      forAll ("model") {
         (model: Model) =>
           noException must be thrownBy tyCheck (tenv0, model)
      }
    }

  }

}
