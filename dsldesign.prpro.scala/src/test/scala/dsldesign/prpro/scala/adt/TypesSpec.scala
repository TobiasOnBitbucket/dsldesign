// (c) dsldesign, wasowski, tberger
package dsldesign.prpro.scala.adt

import Types._
import Generators._

class TypesSpec
  extends org.scalatest.freespec.AnyFreeSpec
  with org.scalatest.matchers.must.Matchers
  with org.scalatest.prop.Configuration
  with org.scalatest.Inside
  with org.scalatestplus.scalacheck.ScalaCheckPropertyChecks
{

  "Case-based regressions" -  {

    "isSubTypeOf (vector length is contravariant)" in {

      val result = VectorTy (1, FloatTy)
        .isSubTypeOf (VectorTy (2, FloatTy))
      result must
        be (false)
    }

    "lub (ProbTy, NonNegFloatTy)" in {

      lub (ProbTy, NonNegFloatTy) must
        be (NonNegFloatTy)
    }

    "Is it least? (a case-based test, several non-trivial cases)" in {

      lub (NatTy, PosProbTy) must
        be (PosFloatTy)
      lub (PosProbTy, IntTy) must
        be (FloatTy)
      lub (ProbTy, IntTy) must
        be (FloatTy)
    }

    "DistribTy(PosProbTy) isSubTypeOf NonNegFloatTy" in {
      (DistribTy (PosProbTy) isSubTypeOf NonNegFloatTy) must
        be (true)
    }

    "FloatTy is not super type of VectorTy(1, IntTy)" in {
      (FloatTy isSuperTypeOf VectorTy (1, IntTy)) must
        be (false)
    }

  }

  "The partial order of SimpleTy" - {

    "sub-typing is reflexive" in
      forAll ("t") {
        t: SimpleTy =>
          (t isSubTypeOf t) must
            be (true)
      }

    "sub-typing is anti-symmetric for simple types" in
      forAll ("t1", "t2", maxDiscardedFactor (100)) {
        (t1: SimpleTy, t2: SimpleTy) =>
          whenever (t1.isSubTypeOf (t2) && t2.isSubTypeOf (t1)) {
            t1 must
              be (t2)
          }
      }

    "sub-typing is transitive for simple types" in
      forAll ("t1", "t2", "t3", maxDiscardedFactor (100)) {
        (t1: SimpleTy, t2: SimpleTy, t3: SimpleTy) =>
          whenever (t1.isSubTypeOf (t2) && t2.isSubTypeOf (t3)) {
            t1.isSubTypeOf (t3) must
              be (true)
          }
      }

    "super-typing is reflexive" in
      forAll ("t") {
        t: SimpleTy =>
          (t isSuperTypeOf t) must
            be (true)
      }

    "lub is reflexive (idempotent) on simple types" in
      forAll ("ty") {
        ty: SimpleTy =>
          lub (ty, ty) must
            be (ty)
      }

    "lub is one of its arguments if one subtype of the other (left)" in
      forAll ("t1", "t2") {
        (t1: SimpleTy, t2: SimpleTy) =>
          whenever (t1 isSubTypeOf t2) {
            lub (t1, t2) must
              be (t2)
          }
      }

    "lub is one of its arguments if one subtype of the other (right)" in
      forAll ("t1", "t2") {
        (t1: SimpleTy, t2: SimpleTy) =>
          whenever (t2 isSubTypeOf t1) {
            lub (t1, t2) must
              be (t1)
          }
      }

    "Axiom: lub is a supertype of both of its arguments" in
      forAll ("t1", "t2") {
        (t1: SimpleTy, t2: SimpleTy) =>
          val t = lub (t1, t2)
          t.isSuperTypeOf (t2) must
            be (true)
          t.isSuperTypeOf (t1) must
            be (true)
    }

    "Axiom: lub is least such" in {

      // It is fairly difficult to randomly generate
      // the triple satisfying the condition, so
      // we tell the generator to try harder

      implicit val generatorDrivenConfig =
        PropertyCheckConfiguration (maxDiscardedFactor = 100.0)

      forAll ("t", "t1", "t2") {
        (t: SimpleTy, t1: SimpleTy, t2: SimpleTy) =>
          whenever (t.isSuperTypeOf (t1) && t.isSuperTypeOf (t2)) {
            val ty = lub (t1, t2)
            withClue (s"{lub (t1, t2) = $ty}") {
              (t isSuperTypeOf ty) must
                be (true)
            }
          }
      }
    }

  }

  "The partial order on Ty" - {

    "sub-typing is reflexive" in
      forAll ("t") { t: Ty =>
        (t isSubTypeOf t) must
          be (true)
      }

    "sub-typing is anti-symmetric" in
      forAll ("t1", "t2", maxDiscardedFactor (100)) {
        (t1: Ty, t2: Ty) =>
          whenever (t1.isSubTypeOf (t2) && t2.isSubTypeOf (t1)) {
            t1 must
              be (t2)
          }
      }

    "sub-typing is transitive" in
      forAll ("t1", "t2", "t3", maxDiscardedFactor (100)) {
        (t1: Ty, t2: Ty, t3: Ty) =>
          whenever (t1.isSubTypeOf (t2) && t2.isSubTypeOf (t3)) {
            t1.isSubTypeOf (t3) must
              be (true)
          }
      }


    "super-typing is reflexive" in
      forAll ("t") { t: Ty =>
          (t isSuperTypeOf t) must
            be (true)
      }

    "lub is able to unify vectors of simple types" in
      forAll ("t1","t2", "l1", "l2") {
        (t1: SimpleTy, t2: SimpleTy, n: Int, m: Int) =>
          val l1 = Math.abs (n)
          val l2 = Math.abs (m)
          lub (VectorTy (l1, t1), VectorTy (l2, t2)) must
            be  (Right (VectorTy (Math.min (l1,l2), lub (t1,t2))))
      }

    "lub should not unify VectorTy with simple types" in
      forAll ("t1", "t2") {
        (t1: VectorTy, t2: SimpleTy) =>
          whenever (t1.elemTy.isInstanceOf[SimpleTy]) {
            lub (t1,t2) must
              be (Symbol ("Left"))
            lub (t2,t1) must
              be (Symbol ("Left"))
          }
      }

    "lub succeds for all non-vector arguments" in
      forAll ("t1", "t2") {
        (t1: Ty, t2: Ty) =>
          whenever (!t1.isInstanceOf[VectorTy] && !t2.isInstanceOf[VectorTy]) {
            lub (t1,t2) must
              be (Symbol ("Right"))
         }
      }

    "lub (DistribTy (t1), DistribTy (t2)) = D (lub (t1, t2))" in
      forAll ("t1", "t2") {
        (t1: SimpleTy, t2: SimpleTy) =>
            lub (DistribTy (t1), DistribTy (t2)) must
              be (Right (DistribTy (lub (t1, t2))))
      }

    "lub (DistribTy (t1), t2) = lub (t1, t2)" in
      forAll ("t1", "t2") {
        (t1: SimpleTy, t2: SimpleTy) =>
          lub (DistribTy (t1), t2) must
           be (Right (lub (t1,t2)))
          lub (DistribTy (t2), t1) must
            be (Right (lub (t1, t2)))
      }

    "lub on Ty delegates to lub on SimpleTy" in
      forAll ("t1","t2") {
        (t1: SimpleTy, t2: SimpleTy) =>
          lub (t1: Ty, t2: Ty) must
            be (Right (lub (t1, t2)))
      }

    "Commutative" in
      forAll ("t1","t2") {
        (t1: Ty, t2: Ty) =>
          for {
            rhs <- lub (t1, t2)
            lhs <- lub (t2, t1)
          } yield lhs must be (rhs)
      }

    "'fuzzing'" in
      forAll ("t1", "t2") {
        (t1: Ty, t2: Ty) =>
          noException must be thrownBy lub (t1, t2)
      }

    "Axiom: lub is a supertype of both of its arguments if it exists" in
      forAll ("t1", "t2") {
        (t1: Ty, t2: Ty) =>
          val t = lub (t1, t2)
          whenever (t.isRight) {
            t match {

              case Right (ty) =>
                withClue (s"{$ty isSuperTypeOf $t1}") {
                  (ty isSuperTypeOf t1) must
                    be (true)
                }
                withClue (s"{$ty isSuperTypeOf $t2}") {
                  (ty isSuperTypeOf t2) must
                    be (true)
                }

              case Left (_) =>

                val incomparable =
                  (t1.isVector && !t2.isVector) || (!t1.isVector && t2.isVector)
                incomparable must
                  be (true)
            }
          }

      }

    // The same test as above, slightly less precise, but also more
    // concise for the book presentation

    "Axiom: lub is a supertype of its arguments if it exists" in
      forAll { (t1: Ty, t2: Ty) =>
        inside (lub (t1,t2)) {
          case Right (ty) =>
            t1.isSubTypeOf (ty) must be (true)
            t2.isSubTypeOf (ty) must be (true)
          case Left (msg) =>
        }
      }

    "Axiom: lub is least such" in {

      // It is fairly difficult to randomly generate
      // the triple satisfying the condition, so
      // we tell the generator to try harder

      forAll ("t", "t1", "t2", maxDiscardedFactor (800)) {
        (t: Ty, t1: Ty, t2: Ty) =>
          whenever (t.isSuperTypeOf (t1) && t.isSuperTypeOf (t2)) {

            lub (t1, t2) match {
              case Left (_) =>
                fail (s"lub must exist if common super-type exists ($t1, $t2)")

              case Right (ty) =>
                t.isSuperTypeOf (ty)  must
                  be (true)
            }
          }
      }

    }

    // The same test as above, slightly less precise, but also more
    // concise for the book presentation

    "join is least such" in {
      forAll (maxDiscardedFactor (100)) { (t: Ty, t1: Ty, t2: Ty) =>
        whenever (t1.isSubTypeOf (t) && t2.isSubTypeOf (t)) {
          inside (lub (t1,t2)) {
            case Right (ty) => ty.isSubTypeOf (t) must be (true)
            case Left (_) => fail (s"must exist if a super-type does!")
          }
        }
      }
    }

    "lub Regression" in {

      val t1 = PosProbTy
      val t2 = VectorTy (2147483647, PosProbTy)
      lub (t1, t2) match {
        case Right (ty) =>
          fail (s"lub should not exist if no common super-type exists ($t1, $t2)")

        case Left (_) =>
      }
    }

  }

}
