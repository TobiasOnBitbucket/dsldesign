/*
   Copyright 2020-2021 Andrzej Wasowski and Thorsten Berger

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package mdsebook.robot.scala

import dsldesign.scala.emf._
import scala.jdk.CollectionConverters._
import mdsebook.robot._

object Constraints {

  // All reactions in the same mode should have distinct trigger events

  val nonOverlappingTriggers = inv[Mode] { self =>
      val triggers = self.getReactions.asScala map { _.getTrigger }
      triggers.toSet.size == triggers.size }

  // Every mode has no submodes, or has an initial submode

  val allModesInitialized = inv[Mode] { self =>
    (!self.getModes.isEmpty) implies (self.getModes.asScala.exists {_.isInitial}) }

  val invariants: List[Constraint] = List (nonOverlappingTriggers, allModesInitialized)

}
