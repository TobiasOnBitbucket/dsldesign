// Copyright 2020 Andrzej Wasowski and Thorsten Berger
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dsldesign.fsm.xtend

import dsldesign.fsm.FiniteStateMachine
import dsldesign.fsm.Model

class Main {

  val static (FiniteStateMachine) => Boolean C2 = [ 
    states.forall [ s1 | 
    states.forall [ s2 | s1==s2 || s1.name!=s2.name ] ]
  ]

  // Load a test case
  def static Model loadInstance (String fname) {
    println ('''Loading «fname»''')
    // From the helper file XtendUtil.xtend
    val resource = XtendUtil.loadFromXMI (fname) 
    resource.contents.get (0) as Model
  }

  // Check constraint C2 on all machines in the file designated by fname
  def static checkAll (String fname) {
    val results = 
      loadInstance ('''../dsldesign.fsm/test-files/«fname».xmi''')
      .machines
      .map [ it -> C2.apply (it) ]

      if (results.forall [ it.value ])
        println ('''C2 is satisfied by all machines in «fname».xmi''')
      else results
        .filter [!value]
        .forEach [println ('''C2 VIOLATED by machine '«key.name»' in «fname».xmi''')]
  }

  def static main (String[] args) {
      dsldesign.fsm.FsmPackage.eINSTANCE.eClass

      // File names we want to test (same as test cases for Scala)
      val testModels = #[ "test-00", "test-01", "test-02", "test-07",
          "test-09", "test-11", "test-04", "test-14", 
          "test-08" ]

      testModels.forEach [ checkAll (it) ]
  }
}
