// // (c) dsldesign, wasowski, tberger
// A code generator written in Xtend for creating Java and Graphviz code
// from a finite state machine model in-place transformation implemented in Xtend.
// Run using ToJavaCodeMain.xtend

// As common for M2T, we generate code independent of the Java or Graphvize meta-model.
// Specifically, we use a minimal subset of Java and little structure.  Almost the same
// code should be generated for C, to be used on a micro-controller for example.
package dsldesign.fsm.xtend

import dsldesign.fsm.FiniteStateMachine

class ToJavaCode{

	def static compileToJava(FiniteStateMachine it) {
		var int i = -1
		''' 
			import java.util.Scanner;
			
			class FSM�it.name.toFirstUpper� {
			
				�FOR state : it.states�
					static final int �state.name.toUpperCase� = �i = i + 1�;
				�ENDFOR�
				static int current;
				
				static final String[] stateNames = { 
					�FOR state : states�"�state.name�",�ENDFOR�
				};
					
				static final String[] availableInputs = {
					�FOR state : states�
						"�FOR t : state.leavingTransitions�<�t.input�>�ENDFOR�",
					�ENDFOR�
				};
					
				public static void main (String[] args) {
			
				@SuppressWarnings(value = { "resource" })
				Scanner scanner = new Scanner(System.in);
				current = �initial.name.toUpperCase�;
				
				while (true) {
					System.out.print ("[" + stateNames[current] + "] ");
					System.out.print ("What is the next event? available: " + availableInputs[current]);
					System.out.print ("?");
					String input = scanner.nextLine();
					
					switch (current) {
				
					�FOR state : states�
						case �state.name.toUpperCase�:
							switch (input) {
							�FOR t : state.leavingTransitions�
								case "�t.input�":
									System.out.println ("machine says: �t.output�");
									current = �t.target.name.toUpperCase�;
									break;
							�ENDFOR�
							}
							break;
					�ENDFOR�
					}
				}
				}
				
			}
		'''
	}

	def static compileToDot(FiniteStateMachine it) {
		'''
			digraph "�it.name�" {
				_init -> �it.initial.name�;
				�FOR state : states�
					�FOR t : state.leavingTransitions�
						"�state.name�" -> "�t.target.name�" [label="�t.input� / �t.output� "];
					�ENDFOR�
				�ENDFOR�
				�it.initial.name� [shape=doublecircle];
				_init [shape=point];
			}
		'''
	}
	
}
