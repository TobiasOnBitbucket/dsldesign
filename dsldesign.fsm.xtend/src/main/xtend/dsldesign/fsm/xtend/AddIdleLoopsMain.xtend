// (c) dsldesign, wasowski, tberger
package dsldesign.fsm.xtend

import dsldesign.fsm.FsmPackage
import dsldesign.fsm.Model

class AddIdleLoopsMain {
	
	// change input file name of your state machine model here; 
	// path is relative to project root
	// in this case we load the coffee machine example
	val static instanceFileName = "../dsldesign.fsm/test-files/coffeemachine.xmi"
	
	/**
	 * This method reads the model in abstract syntax (XMI file),
	 * executes the AddIdleLoop transformation, and saves the output
	 * in a temporary file.
	 */
	def static void main (String[] args) {
		
		// register our meta-model package for abstract syntax
		FsmPackage.eINSTANCE.eClass
		
		// we are loading our file here
		val resource = XtendUtil.loadFromXMI( instanceFileName )
		
		/* The call to get(0) below gives you the first model root. 
		// If you open a directory instead of a file, 
		// you can iterate over all models in it, 
		// by changing 0 to a suitable index */
		val model = resource.contents.get(0) as Model
		
		// we run the transformation, which changes m
		model.machines.forEach[ m | AddIdleLoops.run(m) ]
		
		// save the resulting model
		XtendUtil.saveAsXMI( "xtend-gen/test-output.xmi", model )
	}
	
}
