// (c) mdsebook, wasowski, tberger
// Run using 'sbt run' and select  MainBinary as the main class

package mdsebook.numbers.scala

object Binary {

  case class Number (n: Int) {
    def I = Number (n*2 + 1)
    def O = Number (n*2 + 0)
    def I (i: Number) = Number (n*4 + 2 + i.n)
    def O (i: Number) = Number (n*4 + 0 + i.n)
    override def toString = n.toString
  }

  object I extends Number (1)
  object O extends Number (0)
}

object MainBinary extends App {

  import scala.language.postfixOps

  { import Binary._

    val l = List (I, I O, I O O, I O I O, O I O I I, 0)
    l foreach {println _}
  }

}

