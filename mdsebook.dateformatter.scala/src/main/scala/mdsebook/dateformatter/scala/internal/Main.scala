package mdsebook.dateformatter.scala.internal

import mdsebook.dateformatter.scala.internal.DSL._
import scala.language.postfixOps

object Main extends App {

  import Formatter._

  val myDate = Date (28, 2, 2017)

  println (  { yyyy - mm - dd }                 format myDate )
  println (  { mm / dd / yyyy }                 format myDate )
  println (  { yyyy mm dd }                     format myDate )
  println (  { mmm (" ") yy }                   format myDate )
  println (  { mmm (" ") ddth (", ") yyyy }     format myDate )
  println (  { / mmm (" ") ddth (", ") yyyy / } format myDate )

}
