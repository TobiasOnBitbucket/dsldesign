// (c) dsldesign, wasowski, tberger

/*
   See  dsldesign.prpro.scala project  for a  more comprehensive  test
   suite implemented  in Scalatest  for the Scala  ADT version  of the
   language.

   We did  not reimplement  all tests  in Java/JUnit  (And we  did not
   re-use the scala test-suite to  test the java implementation, which
   could  have been  done  'in principle'). Instead,  we include  some
   rudimentary tests,  to show  how JUnit can  be integrated  into the
   infrastructure (as opposed to Scalatest).

   We try keep the order and names  of tests between Scala and Java to
   be  the same,  to  help  you comparing  them. Compare  to tests  in
   TypeCheckerSpec.scala and TypesSpec.scala

*/
package dsldesign.prpro.java;

import java.util.Map;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;

import dsldesign.prpro.*;
import dsldesign.prpro.java.Types;
import dsldesign.prproTypes.*;
import dsldesign.scala.emf.package$;
import org.eclipse.emf.ecore.util.EcoreUtil;

import static dsldesign.prpro.java.Types.*;

@DisplayName ("TypeCheckerTest: Test cases")
class TypeCheckerTest {

  PrproFactory factory = PrproFactory.eINSTANCE;

  @BeforeAll
  static void before () { PrproPackage.eINSTANCE.eClass (); }

  Model loadPrPro (String name) {
     return package$.MODULE$.loadFromXMI
      ("../dsldesign.prpro/test-files/" + name + ".xmi");
  }

  // Create an empty environment on demand
  Map<String, Ty> tenv0 () {
    return new 
      HashMap<String, Ty> (TypeChecker.emptyTypingEnvironment);
  } 

  @Test
  @DisplayName ("01 Example1.xmi shall typeCheck")
  void test01() 
  { 
    Model m = loadPrPro ("example1");
    Map<String, Ty> result = 
      assertDoesNotThrow (() -> TypeChecker.tyCheck (tenv0 (), m));
    assertNotNull (result);
    assertEquals (6, result.size ());
    assertFalse (result.values ().contains (null));
  }

  @Test
  @DisplayName ("02 Typing (Norm) positive")
  void test02 ()
  {
    Normal e = factory.createNormal ();
    CstF mu = factory.createCstF ();
    mu.setValue (0.42);
    CstF sigma = factory.createCstF ();
    sigma.setValue (0.1);
    e.setMu (mu);
    e.setSigma (sigma);

    assertTrue (
        EcoreUtil.equals (
          distribTy (floatTy),
          TypeChecker.tyCheck (tenv0 (), e)
        ));
  }

  @Test
  @DisplayName ("03 Typing (Norm) negative")
  void test03 ()
  {
    Normal e = factory.createNormal ();
    CstF mu = factory.createCstF ();
    mu.setValue (0.42);
    CstF sigma = factory.createCstF ();
    sigma.setValue (-0.42);
    e.setMu (mu);
    e.setSigma (sigma);

    assertThrows (
        TypeError.class, 
        () -> TypeChecker.tyCheck (tenv0 (), e)
    );
  }

  @Test
  @DisplayName ("04 Typing (Norm-O) positive")
  void test04 ()
  {
    Normal e = factory.createNormal ();
    CstF mu = factory.createCstF ();
    mu.setValue (0.42);
    CstF sigma = factory.createCstF ();
    sigma.setValue (0.1);

    Data d = factory.createData ();
    d.setName ("z");
    VarRef ref = factory.createVarRef ();
    ref.setReferencedVar (d);
    Map<String, Ty> tenv = tenv0 ();
    tenv.put ("z", vectorTy (42, probTy));
    
    e.setMu (mu);
    e.setSigma (sigma);
    e.setObserved (ref);

    assertTrue (
        EcoreUtil.equals (
          distribTy (floatTy),
          TypeChecker.tyCheck (tenv, e)
        ));
  }

  @Test
  @DisplayName ("05 Typing (Norm-O) negative")
  void test05 ()
  {
    Normal e = factory.createNormal ();
    CstF mu = factory.createCstF ();
    mu.setValue (0.42);
    CstF sigma = factory.createCstF ();
    sigma.setValue (-0.42);

    Data d = factory.createData ();
    d.setName ("z");
    VarRef ref = factory.createVarRef ();
    ref.setReferencedVar (d);
    Map<String, Ty> tenv = tenv0 ();
    tenv.put ("z", probTy);
    
    e.setMu (mu);
    e.setSigma (sigma);
    e.setObserved (ref);

    assertThrows (
        TypeError.class, 
        () -> TypeChecker.tyCheck (tenv, e)
    );
  }

  @Test
  @DisplayName ("06 Typing (CstF) regression")
  void test06 ()
  {
    CstF e0 = factory.createCstF ();
    e0.setValue (0.42);
    CstF e1 = factory.createCstF ();
    e1.setValue (0.1);

    assertEquals (posProbTy, TypeChecker.tyCheck (tenv0 (), e0));
    assertEquals (posProbTy, TypeChecker.tyCheck (tenv0 (), e1));
  }

   
  @Test
  @DisplayName ("07 Typing (Unif) positive")
  void test07 ()
  {
    Uniform u = factory.createUniform ();
    CstF e0 = factory.createCstF ();
    e0.setValue (0.42);
    CstF e1 = factory.createCstF ();
    e1.setValue (0.1);
    u.setLo (e1);
    u.setHi (e0);

    assertTrue (
        EcoreUtil.equals (
          distribTy (posProbTy),
          TypeChecker.tyCheck (tenv0 (), u)
        ));
  }

  @Test
  @DisplayName ("08 Typing (Unif) negative")
  void test08 ()
  {
    Uniform u = factory.createUniform ();
    CstF e0 = factory.createCstF ();
    e0.setValue (0.42);

    Data d = factory.createData ();
    d.setName ("z");
    VarRef ref = factory.createVarRef ();
    ref.setReferencedVar (d);
    Map<String, Ty> tenv = tenv0 ();
    tenv.put ("z", vectorTy (42, probTy));

    u.setLo (ref);
    u.setHi (e0);

    assertThrows (
        TypeError.class, 
        () -> TypeChecker.tyCheck (tenv0 (), u)
    );
  }

  @Test
  @DisplayName ("09 Typing (Unif-O) positive")
  void test09 ()
  {
    Uniform u = factory.createUniform ();
    CstF e0 = factory.createCstF ();
    e0.setValue (0.42);
    CstF e1 = factory.createCstF ();
    e1.setValue (0.1);
    u.setLo (e1);
    u.setHi (e0);

    Data d = factory.createData ();
    d.setName ("z");
    VarRef ref = factory.createVarRef ();
    ref.setReferencedVar (d);
    Map<String, Ty> tenv = tenv0 ();
    tenv.put ("z", vectorTy (42, posProbTy));
    
    u.setObserved (ref);

    assertTrue (
        EcoreUtil.equals (
          distribTy (posProbTy),
          TypeChecker.tyCheck (tenv, u)
        ));
  }

  @Test
  @DisplayName ("10 Typing (Unif-O) negative")
  void test10 ()
  {
    Uniform u = factory.createUniform ();
    CstF e0 = factory.createCstF ();
    e0.setValue (0.42);
    CstF e1 = factory.createCstF ();
    e1.setValue (0.1);
    u.setLo (e1);
    u.setHi (e0);

    Data d = factory.createData ();
    d.setName ("z");
    VarRef ref = factory.createVarRef ();
    ref.setReferencedVar (d);
    Map<String, Ty> tenv = tenv0 ();
    tenv.put ("z", probTy);
    
    u.setObserved (ref);

    assertThrows (
        TypeError.class, 
        () -> TypeChecker.tyCheck (tenv, u)
    );
  }

  @Test
  @DisplayName("#11 regression for Uniform (distribTy, distribTy)")
  void test11 () 
  { 
    CstF e0 = factory.createCstF ();
    e0.setValue (0.0);
    CstF e1 = factory.createCstF ();
    e1.setValue (0.1);
    CstF e2 = factory.createCstF ();
    e2.setValue (42.0);

    Normal lo = factory.createNormal ();
    Normal hi = factory.createNormal ();
    lo.setMu (e0);
    lo.setSigma (e1);
    hi.setMu (e2);
    hi.setSigma (EcoreUtil.copy (e1));

    Uniform u = factory.createUniform ();
    u.setLo (lo);
    u.setHi (hi);
    Ty t = TypeChecker.tyCheck (tenv0 (), u);

    assertTrue (
      EcoreUtil.equals (distribTy (floatTy), t), 
      "The type of 'u' is '" + t + "'");
  }
   
   
}
