// (c) dsldesign, wasowski, tberger

/*
   See  dsldesign.prpro.scala project  for a  more comprehensive  test
   suite implemented  in Scalatest  for the Scala  ADT version  of the
   language.

   We did  not reimplement  all tests  in Java/JUnit  (And we  did not
   re-use the scala test-suite to  test the java implementation, which
   could  have been  done  'in principle'). Instead,  we include  some
   rudimentary tests,  to show  how JUnit can  be integrated  into the
   infrastructure (as opposed to Scalatest).

   We try keep the order and names  of tests between Scala and Java to
   be  the same,  to  help  you comparing  them. Compare  to tests  in
   TypeCheckerSpec.scala and TypesSpec.scala

*/
package dsldesign.prpro.java;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import dsldesign.prpro.*;
import dsldesign.prproTypes.*;

import static dsldesign.prpro.java.Types.*;
import org.eclipse.emf.ecore.util.EcoreUtil;

@DisplayName("TypesTest: Case-based regressions")
class TypesTest
{

  @Test
  @DisplayName("#01 isSubTypeOf (vector length is contravariant)")
  void test01 () 
  { 
    VectorTy vf1 = vectorTy (1, floatTy);
    VectorTy vf2 = vectorTy (2, floatTy);
    assertNotNull (vf1.getElemTy (), "vf1.getElemTy () returns " + vf1.getElemTy ());
    assertNotNull (vf2.getElemTy (), "vf1.getElemTy () returns " + vf2.getElemTy ());
    Boolean result = isSubTypeOf  (vf1, vf2);
    assertNotNull (result);
    assertFalse (result);
  }

  @Test
  @DisplayName("#02 lub (ProbTy, NonNegFloatTy)")
  void test02 () 
  { assertEquals (nonNegFloatTy, lub (probTy, nonNegFloatTy)); }

  @Test
  @DisplayName("#03 Is it least? (a case-based test, several non-trivial cases)")
  void test03 () 
  {
    assertAll (
      () -> assertEquals (posFloatTy, lub (natTy, posProbTy)),
      () -> assertEquals (floatTy, lub (posProbTy, intTy)),
      () -> assertEquals (floatTy, lub (probTy, intTy))
    );
  }

  @Test
  @DisplayName("#04 DistributionTy (posProbTy) isSuperTypeOf nonNegFloatTy")
  void test04 () 
  { assertTrue (isSuperTypeOf (nonNegFloatTy, distribTy (posProbTy))); }

  @Test
  @DisplayName("#05 PosProbTy isSubTypeOf NonNegFloatTy")
  void test05 () 
  { assertTrue (isSubTypeOf (posProbTy, nonNegFloatTy)); }

  @Test
  @DisplayName("#06 NonNegFloatTy is in supertypes of PosProbTy")
  void test06 () 
  { assertTrue (superTyTags (posProbTy).contains (nonNegFloatTy.getTag ())); }

  @Test
  @DisplayName("#07 NonNegFloatTy's supertypes do not include PosProbTy")
  void test07 () 
  { assertFalse (superTyTags (nonNegFloatTy).contains (posProbTy)); }

  @Test
  @DisplayName("#08 IntTy isSubTypeOf FloatTy plus some equality debugging")
  void test08 () 
  { 
    assertAll (
      () -> assertFalse (EcoreUtil.copy (intTy) == intTy, "8.1"),
      () -> assertFalse (EcoreUtil.copy (intTy).equals (intTy), "8.2"),
      () -> assertTrue (isSubTypeOf (intTy, floatTy), "8.3"), 
      () -> assertTrue (isSubTypeOf (EcoreUtil.copy (intTy), EcoreUtil.copy (floatTy)), "8.4")
    );
  }

  @Test
  @DisplayName("#09 floatTy is not a super type of vectorTy(1, distribTy (intTy))")
  void test09 () 
  { assertFalse (isSuperTypeOf (floatTy, vectorTy (1, intTy))); }

  @Test
  @DisplayName("#10 floatTy isSuperTypeOf distribTy (intTy)")
  void test10 () 
  { assertTrue (isSuperTypeOf (floatTy, distribTy (intTy))); }

  @Test
  @DisplayName ("#11 Vector type cannot be a super type of a Distribution type (regression)")
  void test11 ()
  { assertFalse ( isSuperTypeOf (vectorTy (1, intTy), distribTy (intTy))); }

  @Test
  @DisplayName("#12 lub (vectorTy, NonNegFloatTy)")
  void test12 () 
  { 
    assertThrows (
      TypeError.class, 
      () -> { lub (vectorTy(10, intTy),nonNegFloatTy); }); 
  }

  @Test
  @DisplayName("#13 lub (vectorTy, vectorTy)")
  void test13 () 
  { 
      Ty t = lub (vectorTy (10, intTy), vectorTy (42, probTy));
      assertTrue (EcoreUtil.equals(vectorTy (10, floatTy), t),
          "lub result: " + t);
  }

  @Test
  @DisplayName("#14 regression lub (intTy, probTy)")
  void test14 () 
  { assertEquals ( floatTy, lub (intTy, probTy));  }

  @Test
  @DisplayName("#15 lub (distribTy, distribTy)")
  void test15 () 
  { 
      assertTrue (EcoreUtil.equals (
          distribTy (nonNegFloatTy),
          lub (distribTy (probTy), distribTy (natTy))
      )); 
  }

  @Test
  @DisplayName("#16 lub (distribTy, nonNegIntTy)")
  void test16 () 
  { 
      assertEquals (
          nonNegFloatTy,
          lub (distribTy (probTy), nonNegIntTy)
      ); 
  }

  @Test
  @DisplayName("#17 lub (nonNegIntTy, distribTy)")
  void test17 () 
  { 
      assertEquals (
          nonNegFloatTy,
          lub (nonNegIntTy, distribTy (probTy))
      ); 
  }

}
