package dsldesign.prpro.java;

class TypeError extends RuntimeException {
  public TypeError (String msg) { this.msg = msg; }
  public final String msg;
  public String toString () { return msg; }
}
