// (c) dsldesign, wasowski, tberger
package dsldesign.prpro.java;

import java.util.Map;
import java.util.HashMap;

import dsldesign.prpro.*;
import dsldesign.prproTypes.*;

import dsldesign.prpro.util.PrproSwitch;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

class TypeChecker {

  static public final Map<String, Ty> emptyTypingEnvironment = 
    new HashMap<String, Ty> ();

  static public Map<String, Ty> 
    tyCheck (Map<String, Ty> tenv, Model model) 
    throws TypeError
  {
    for (Declaration decl: model.getDecls ())
      tyCheck (tenv, decl);
    return tenv;
  }

  static public Map<String, Ty> 
    tyCheck (Map<String, Ty> tenv, Declaration decl) 
    throws TypeError
  { return new TyCheckDeclSwitch (tenv, decl).get (); }

  static public Ty tyCheck (Map<String, Ty> tenv, Expression expr) 
    throws TypeError
  { return new TyCheckExprSwitch (tenv, expr).get (); }


  static class TyCheckSwitch<T> extends PrproSwitch<T> {
    protected EObject ast;
    protected Map<String,Ty> tenv;

    public TyCheckSwitch (Map<String,Ty> tenv, EObject ast)
    {
      this.tenv = tenv;
      this.ast = ast;
    }

    public T get () { return this.doSwitch (ast); }

    public T defaultCase (EObject ignore) throws TypeError
    { 
      throw new TypeError ("Internal Error (Should not happen). " +
          "Applied to object " + ignore); 
    }
  }


  static class TyCheckDeclSwitch extends TyCheckSwitch<Map<String,Ty>> 
  {
    public TyCheckDeclSwitch (Map<String,Ty> tenv, EObject ast) 
    { super (tenv, ast); }

    @Override public Map<String,Ty> caseData (Data decl) throws TypeError
    {
      String name = decl.getName ();
      if (tenv.containsKey (name))
        throw new TypeError 
          ("Identifier '" + name + "' has already been defined!");
      tenv.put (name, decl.getTy ());
      return tenv;
    }

    @Override public Map<String,Ty> caseLet (Let let) throws TypeError
    {
      String name = let.getName ();
      Ty t1 = tyCheck (tenv, let.getValue ());

      if (tenv.containsKey (name)) 
        throw new TypeError 
          ("Identifier '" + name + "' has already been defined!");
      
      tenv.put (name, t1);
      return tenv;
    }
  }


  static class TyCheckExprSwitch extends TyCheckSwitch<Ty> 
  {
    public TyCheckExprSwitch (Map<String,Ty> tenv, EObject ast) 
    { super (tenv, ast); }


    @Override public Ty caseCstI (CstI expr)
    {
      Ty result = Types.intTy;
      if (expr.getValue () > 0) 
        result = Types.natTy;
      else if (expr.getValue () == 0) 
        result = Types.nonNegIntTy;
      expr.setTy (result);
      return result;
    }

    @Override public Ty caseCstF (CstF expr)
    {
      Ty result = Types.floatTy;
      Double value = expr.getValue ();
      if (value == 0.0) 
        result = Types.probTy;
      else if (value > 0.0 && value <= 1.0) 
        result = Types.posProbTy;
      else if (value > 1.0)
        result = Types.posFloatTy;
      expr.setTy (result);
      return result;
    }

    @Override public Ty caseVarRef (VarRef expr) throws TypeError
    {
      String name = expr.getReferencedVar ().getName ();
      // The reference existence is ensured by Ecore at this point.
      // But not that it is declared before in the file, which we require.
      if (!tenv.containsKey (name))
        throw new TypeError ("Variable '" + name + "' not declared before use!");
      Ty result = tenv.get (name);
      expr.setTy (result);
      return result;
    }
    
    // This rule is not sound for all our simple type -- operator
    // combinations.  We have an exercise to refine it.
    @Override public Ty caseBExpr (BExpr expr) throws TypeError
    {
      Ty t1 = tyCheck (tenv, expr.getLeft ());
      Ty t2 = tyCheck (tenv, expr.getLeft ());
      Ty t = Types.lub (t1, t2);
      expr.setTy (t);
      return t;
    }


    @Override public Ty caseNormal (Normal expr) throws TypeError
    {
      Ty t1 = tyCheck (tenv, expr.getMu ());
      if (!Types.isSubTypeOf (t1, Types.floatTy))
        throw new TypeError (
         "Need a subtype of FloatTy  for 'mu' but got '" + t1 +"'" );

      Ty t2 = tyCheck (tenv, expr.getSigma ());
      if (!Types.isSubTypeOf (t2, Types.nonNegFloatTy))
        throw new TypeError (
         "Need a subtype of NonNegFloatTy for argument 'sigma' but seen '"
         + t2 +"'" );

      if (expr.getObserved () != null) {
        Ty tob = tyCheck (tenv, expr.getObserved ());
        Ty stob = Types.vectorTy (1, Types.floatTy);
        if (!Types.isSubTypeOf (tob, stob))
          throw new TypeError (
           "Need a vector of Floats for observed data but got '" + tob +"'" );
      }

      Ty t = Types.distribTy (Types.floatTy);
      expr.setTy (t);
      return t;
    }

    @Override public Ty caseUniform (Uniform expr)
    {
      Ty t0 = tyCheck (tenv, expr.getLo ());
      Ty t1 = tyCheck (tenv, expr.getHi ());
      Ty ty = Types.lub (t0, t1);
      SimpleTy t; 
      if (ty instanceof SimpleTy)
        t = (SimpleTy) ty;
      else if (ty instanceof DistribTy)
        t = ((DistribTy) ty).getOutcomeTy ();
      else throw new TypeError (
           "The supertype of extremes must be numeric but found '" + ty + "'!");

      if (expr.getObserved () != null) {
        Ty tob = tyCheck (tenv, expr.getObserved ());
        Ty stob = Types.vectorTy (1, t);
        if (!Types.isSubTypeOf (tob, stob))
          throw new TypeError (
           "Need a vector of '" + t + "' for observed data but got '" + tob +"'" );
      }

      expr.setTy (Types.distribTy (t));
      return expr.getTy ();
    }

  }


}
