This directory contains the Eclipse MDT implemenetation of OCL. I downloaded it
from https://www.eclipse.org/modeling/mdt/downloads/?project=ocl and unzipped.

Presently the directory should contain OCL 6.9.0. To upgrade, try downloading a
new zip with the OCL distribution and replace.
