// Copyright 2020 Andrzej Wasowski and Thorsten Berger
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mdsebook.fsm.groovy

import dsldesign.fsm.FsmPackage
import dsldesign.fsm.FiniteStateMachine

import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl

class Main {

  // C2: all states within the same machine must have distinct names
  static def C2 = { 
    it.states.every { s1 -> 
    it.states.every { s2 -> s1==s2 || s1.name!=s2.name } }
  }
  

  def static resourceSet

  // Load a test case
  static def loadInstance (String fname) 
  {
    println ("Loading '${fname}'...")
    resourceSet.getResource (URI.createURI (fname), true).contents[0]
  }

  // Check constraint C2 on all machines in the file designated by fname
  static def checkAll (String fname) 
  {
    def results = 
      loadInstance ("../dsldesign.fsm/test-files/${fname}.xmi")
        .machines
        .collect { new Tuple2 (it, Main.C2 (it)) }

    if (results.every { it.second })
      println ("C2 is satisfied by all machines in ${fname}.xmi")
    else results.each { it ->
        if (!it.second)
          println ("C2 VIOLATED by machine '${it.first.name}' in ${fname}.xmi")
    }
  }


  // Initialize EMF and validate 8 test cases
  static void main(String[] args) {

    Resource.Factory.Registry.INSTANCE
      .extensionToFactoryMap
      .put ("xmi", new XMIResourceFactoryImpl())
    FsmPackage.eINSTANCE.eClass()
    resourceSet = new ResourceSetImpl()

    // File names we want to test (same as test cases for Scala)
    def testModels = ["test-00", "test-01", "test-02", "test-07", 
                      "test-09", "test-11", "test-04", "test-14", 
                      "test-08"]

    testModels.each { checkAll (it) }
  }

}
