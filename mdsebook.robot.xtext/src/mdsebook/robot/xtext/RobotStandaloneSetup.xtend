/*
 * generated by Xtext 2.12.0
 */
package mdsebook.robot.xtext


/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
class RobotStandaloneSetup extends RobotStandaloneSetupGenerated {

	def static void doSetup() {
		new RobotStandaloneSetup().createInjectorAndDoEMFRegistration()
	}
}
