package mdsebook.robot.xtext.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import mdsebook.robot.xtext.services.RobotGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalRobotParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'->'", "'{'", "'}'", "'on'", "'for'", "'s'", "'at'", "'speed'", "'return'", "'to'", "'base'", "'turn'", "'right'", "'left'", "'by'", "'move'", "'forward'", "'backward'", "'+'", "'-'", "'*'", "'/'", "'('", "')'", "'random'", "','", "'obstacle'", "'clap'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__37=37;
    public static final int T__16=16;
    public static final int T__38=38;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=5;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalRobotParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalRobotParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalRobotParser.tokenNames; }
    public String getGrammarFileName() { return "InternalRobot.g"; }



     	private RobotGrammarAccess grammarAccess;

        public InternalRobotParser(TokenStream input, RobotGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Mode";
       	}

       	@Override
       	protected RobotGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleMode"
    // InternalRobot.g:65:1: entryRuleMode returns [EObject current=null] : iv_ruleMode= ruleMode EOF ;
    public final EObject entryRuleMode() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMode = null;


        try {
            // InternalRobot.g:65:45: (iv_ruleMode= ruleMode EOF )
            // InternalRobot.g:66:2: iv_ruleMode= ruleMode EOF
            {
             newCompositeNode(grammarAccess.getModeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMode=ruleMode();

            state._fsp--;

             current =iv_ruleMode; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMode"


    // $ANTLR start "ruleMode"
    // InternalRobot.g:72:1: ruleMode returns [EObject current=null] : ( ( (lv_initial_0_0= '->' ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( ( (lv_actions_3_0= ruleAction ) ) | ( (lv_reactions_4_0= ruleReaction ) ) | ( (lv_modes_5_0= ruleMode ) ) )* otherlv_6= '}' (otherlv_7= '->' ( (otherlv_8= RULE_ID ) ) )? ) ;
    public final EObject ruleMode() throws RecognitionException {
        EObject current = null;

        Token lv_initial_0_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        EObject lv_actions_3_0 = null;

        EObject lv_reactions_4_0 = null;

        EObject lv_modes_5_0 = null;



        	enterRule();

        try {
            // InternalRobot.g:78:2: ( ( ( (lv_initial_0_0= '->' ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( ( (lv_actions_3_0= ruleAction ) ) | ( (lv_reactions_4_0= ruleReaction ) ) | ( (lv_modes_5_0= ruleMode ) ) )* otherlv_6= '}' (otherlv_7= '->' ( (otherlv_8= RULE_ID ) ) )? ) )
            // InternalRobot.g:79:2: ( ( (lv_initial_0_0= '->' ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( ( (lv_actions_3_0= ruleAction ) ) | ( (lv_reactions_4_0= ruleReaction ) ) | ( (lv_modes_5_0= ruleMode ) ) )* otherlv_6= '}' (otherlv_7= '->' ( (otherlv_8= RULE_ID ) ) )? )
            {
            // InternalRobot.g:79:2: ( ( (lv_initial_0_0= '->' ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( ( (lv_actions_3_0= ruleAction ) ) | ( (lv_reactions_4_0= ruleReaction ) ) | ( (lv_modes_5_0= ruleMode ) ) )* otherlv_6= '}' (otherlv_7= '->' ( (otherlv_8= RULE_ID ) ) )? )
            // InternalRobot.g:80:3: ( (lv_initial_0_0= '->' ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( ( (lv_actions_3_0= ruleAction ) ) | ( (lv_reactions_4_0= ruleReaction ) ) | ( (lv_modes_5_0= ruleMode ) ) )* otherlv_6= '}' (otherlv_7= '->' ( (otherlv_8= RULE_ID ) ) )?
            {
            // InternalRobot.g:80:3: ( (lv_initial_0_0= '->' ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==11) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalRobot.g:81:4: (lv_initial_0_0= '->' )
                    {
                    // InternalRobot.g:81:4: (lv_initial_0_0= '->' )
                    // InternalRobot.g:82:5: lv_initial_0_0= '->'
                    {
                    lv_initial_0_0=(Token)match(input,11,FOLLOW_3); 

                    					newLeafNode(lv_initial_0_0, grammarAccess.getModeAccess().getInitialHyphenMinusGreaterThanSignKeyword_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getModeRule());
                    					}
                    					setWithLastConsumed(current, "initial", true, "->");
                    				

                    }


                    }
                    break;

            }

            // InternalRobot.g:94:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalRobot.g:95:4: (lv_name_1_0= RULE_ID )
            {
            // InternalRobot.g:95:4: (lv_name_1_0= RULE_ID )
            // InternalRobot.g:96:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            					newLeafNode(lv_name_1_0, grammarAccess.getModeAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getModeRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_2, grammarAccess.getModeAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalRobot.g:116:3: ( ( (lv_actions_3_0= ruleAction ) ) | ( (lv_reactions_4_0= ruleReaction ) ) | ( (lv_modes_5_0= ruleMode ) ) )*
            loop2:
            do {
                int alt2=4;
                switch ( input.LA(1) ) {
                case 19:
                case 22:
                case 26:
                    {
                    alt2=1;
                    }
                    break;
                case 14:
                    {
                    alt2=2;
                    }
                    break;
                case RULE_ID:
                case 11:
                    {
                    alt2=3;
                    }
                    break;

                }

                switch (alt2) {
            	case 1 :
            	    // InternalRobot.g:117:4: ( (lv_actions_3_0= ruleAction ) )
            	    {
            	    // InternalRobot.g:117:4: ( (lv_actions_3_0= ruleAction ) )
            	    // InternalRobot.g:118:5: (lv_actions_3_0= ruleAction )
            	    {
            	    // InternalRobot.g:118:5: (lv_actions_3_0= ruleAction )
            	    // InternalRobot.g:119:6: lv_actions_3_0= ruleAction
            	    {

            	    						newCompositeNode(grammarAccess.getModeAccess().getActionsActionParserRuleCall_3_0_0());
            	    					
            	    pushFollow(FOLLOW_5);
            	    lv_actions_3_0=ruleAction();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getModeRule());
            	    						}
            	    						add(
            	    							current,
            	    							"actions",
            	    							lv_actions_3_0,
            	    							"mdsebook.robot.xtext.Robot.Action");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalRobot.g:137:4: ( (lv_reactions_4_0= ruleReaction ) )
            	    {
            	    // InternalRobot.g:137:4: ( (lv_reactions_4_0= ruleReaction ) )
            	    // InternalRobot.g:138:5: (lv_reactions_4_0= ruleReaction )
            	    {
            	    // InternalRobot.g:138:5: (lv_reactions_4_0= ruleReaction )
            	    // InternalRobot.g:139:6: lv_reactions_4_0= ruleReaction
            	    {

            	    						newCompositeNode(grammarAccess.getModeAccess().getReactionsReactionParserRuleCall_3_1_0());
            	    					
            	    pushFollow(FOLLOW_5);
            	    lv_reactions_4_0=ruleReaction();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getModeRule());
            	    						}
            	    						add(
            	    							current,
            	    							"reactions",
            	    							lv_reactions_4_0,
            	    							"mdsebook.robot.xtext.Robot.Reaction");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalRobot.g:157:4: ( (lv_modes_5_0= ruleMode ) )
            	    {
            	    // InternalRobot.g:157:4: ( (lv_modes_5_0= ruleMode ) )
            	    // InternalRobot.g:158:5: (lv_modes_5_0= ruleMode )
            	    {
            	    // InternalRobot.g:158:5: (lv_modes_5_0= ruleMode )
            	    // InternalRobot.g:159:6: lv_modes_5_0= ruleMode
            	    {

            	    						newCompositeNode(grammarAccess.getModeAccess().getModesModeParserRuleCall_3_2_0());
            	    					
            	    pushFollow(FOLLOW_5);
            	    lv_modes_5_0=ruleMode();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getModeRule());
            	    						}
            	    						add(
            	    							current,
            	    							"modes",
            	    							lv_modes_5_0,
            	    							"mdsebook.robot.xtext.Robot.Mode");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            otherlv_6=(Token)match(input,13,FOLLOW_6); 

            			newLeafNode(otherlv_6, grammarAccess.getModeAccess().getRightCurlyBracketKeyword_4());
            		
            // InternalRobot.g:181:3: (otherlv_7= '->' ( (otherlv_8= RULE_ID ) ) )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==11) ) {
                int LA3_1 = input.LA(2);

                if ( (LA3_1==RULE_ID) ) {
                    int LA3_3 = input.LA(3);

                    if ( (LA3_3==EOF||LA3_3==RULE_ID||LA3_3==11||(LA3_3>=13 && LA3_3<=14)||LA3_3==19||LA3_3==22||LA3_3==26) ) {
                        alt3=1;
                    }
                }
            }
            switch (alt3) {
                case 1 :
                    // InternalRobot.g:182:4: otherlv_7= '->' ( (otherlv_8= RULE_ID ) )
                    {
                    otherlv_7=(Token)match(input,11,FOLLOW_3); 

                    				newLeafNode(otherlv_7, grammarAccess.getModeAccess().getHyphenMinusGreaterThanSignKeyword_5_0());
                    			
                    // InternalRobot.g:186:4: ( (otherlv_8= RULE_ID ) )
                    // InternalRobot.g:187:5: (otherlv_8= RULE_ID )
                    {
                    // InternalRobot.g:187:5: (otherlv_8= RULE_ID )
                    // InternalRobot.g:188:6: otherlv_8= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getModeRule());
                    						}
                    					
                    otherlv_8=(Token)match(input,RULE_ID,FOLLOW_2); 

                    						newLeafNode(otherlv_8, grammarAccess.getModeAccess().getContinuationModeCrossReference_5_1_0());
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMode"


    // $ANTLR start "entryRuleReaction"
    // InternalRobot.g:204:1: entryRuleReaction returns [EObject current=null] : iv_ruleReaction= ruleReaction EOF ;
    public final EObject entryRuleReaction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReaction = null;


        try {
            // InternalRobot.g:204:49: (iv_ruleReaction= ruleReaction EOF )
            // InternalRobot.g:205:2: iv_ruleReaction= ruleReaction EOF
            {
             newCompositeNode(grammarAccess.getReactionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleReaction=ruleReaction();

            state._fsp--;

             current =iv_ruleReaction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReaction"


    // $ANTLR start "ruleReaction"
    // InternalRobot.g:211:1: ruleReaction returns [EObject current=null] : (otherlv_0= 'on' ( (lv_trigger_1_0= ruleEvent ) ) otherlv_2= '->' ( (otherlv_3= RULE_ID ) ) ) ;
    public final EObject ruleReaction() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Enumerator lv_trigger_1_0 = null;



        	enterRule();

        try {
            // InternalRobot.g:217:2: ( (otherlv_0= 'on' ( (lv_trigger_1_0= ruleEvent ) ) otherlv_2= '->' ( (otherlv_3= RULE_ID ) ) ) )
            // InternalRobot.g:218:2: (otherlv_0= 'on' ( (lv_trigger_1_0= ruleEvent ) ) otherlv_2= '->' ( (otherlv_3= RULE_ID ) ) )
            {
            // InternalRobot.g:218:2: (otherlv_0= 'on' ( (lv_trigger_1_0= ruleEvent ) ) otherlv_2= '->' ( (otherlv_3= RULE_ID ) ) )
            // InternalRobot.g:219:3: otherlv_0= 'on' ( (lv_trigger_1_0= ruleEvent ) ) otherlv_2= '->' ( (otherlv_3= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,14,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getReactionAccess().getOnKeyword_0());
            		
            // InternalRobot.g:223:3: ( (lv_trigger_1_0= ruleEvent ) )
            // InternalRobot.g:224:4: (lv_trigger_1_0= ruleEvent )
            {
            // InternalRobot.g:224:4: (lv_trigger_1_0= ruleEvent )
            // InternalRobot.g:225:5: lv_trigger_1_0= ruleEvent
            {

            					newCompositeNode(grammarAccess.getReactionAccess().getTriggerEventEnumRuleCall_1_0());
            				
            pushFollow(FOLLOW_8);
            lv_trigger_1_0=ruleEvent();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getReactionRule());
            					}
            					set(
            						current,
            						"trigger",
            						lv_trigger_1_0,
            						"mdsebook.robot.xtext.Robot.Event");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getReactionAccess().getHyphenMinusGreaterThanSignKeyword_2());
            		
            // InternalRobot.g:246:3: ( (otherlv_3= RULE_ID ) )
            // InternalRobot.g:247:4: (otherlv_3= RULE_ID )
            {
            // InternalRobot.g:247:4: (otherlv_3= RULE_ID )
            // InternalRobot.g:248:5: otherlv_3= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getReactionRule());
            					}
            				
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_3, grammarAccess.getReactionAccess().getTargetModeCrossReference_3_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReaction"


    // $ANTLR start "entryRuleAction"
    // InternalRobot.g:263:1: entryRuleAction returns [EObject current=null] : iv_ruleAction= ruleAction EOF ;
    public final EObject entryRuleAction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAction = null;


        try {
            // InternalRobot.g:263:47: (iv_ruleAction= ruleAction EOF )
            // InternalRobot.g:264:2: iv_ruleAction= ruleAction EOF
            {
             newCompositeNode(grammarAccess.getActionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAction=ruleAction();

            state._fsp--;

             current =iv_ruleAction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAction"


    // $ANTLR start "ruleAction"
    // InternalRobot.g:270:1: ruleAction returns [EObject current=null] : ( (this_AcDock_0= ruleAcDock | this_AcMove_1= ruleAcMove | this_AcTurn_2= ruleAcTurn ) ( (otherlv_3= 'for' ( (lv_duration_4_0= ruleAExpr ) ) otherlv_5= 's' ) | (otherlv_6= 'at' otherlv_7= 'speed' ( (lv_speed_8_0= ruleAExpr ) ) ) )? ) ;
    public final EObject ruleAction() throws RecognitionException {
        EObject current = null;

        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        EObject this_AcDock_0 = null;

        EObject this_AcMove_1 = null;

        EObject this_AcTurn_2 = null;

        EObject lv_duration_4_0 = null;

        EObject lv_speed_8_0 = null;



        	enterRule();

        try {
            // InternalRobot.g:276:2: ( ( (this_AcDock_0= ruleAcDock | this_AcMove_1= ruleAcMove | this_AcTurn_2= ruleAcTurn ) ( (otherlv_3= 'for' ( (lv_duration_4_0= ruleAExpr ) ) otherlv_5= 's' ) | (otherlv_6= 'at' otherlv_7= 'speed' ( (lv_speed_8_0= ruleAExpr ) ) ) )? ) )
            // InternalRobot.g:277:2: ( (this_AcDock_0= ruleAcDock | this_AcMove_1= ruleAcMove | this_AcTurn_2= ruleAcTurn ) ( (otherlv_3= 'for' ( (lv_duration_4_0= ruleAExpr ) ) otherlv_5= 's' ) | (otherlv_6= 'at' otherlv_7= 'speed' ( (lv_speed_8_0= ruleAExpr ) ) ) )? )
            {
            // InternalRobot.g:277:2: ( (this_AcDock_0= ruleAcDock | this_AcMove_1= ruleAcMove | this_AcTurn_2= ruleAcTurn ) ( (otherlv_3= 'for' ( (lv_duration_4_0= ruleAExpr ) ) otherlv_5= 's' ) | (otherlv_6= 'at' otherlv_7= 'speed' ( (lv_speed_8_0= ruleAExpr ) ) ) )? )
            // InternalRobot.g:278:3: (this_AcDock_0= ruleAcDock | this_AcMove_1= ruleAcMove | this_AcTurn_2= ruleAcTurn ) ( (otherlv_3= 'for' ( (lv_duration_4_0= ruleAExpr ) ) otherlv_5= 's' ) | (otherlv_6= 'at' otherlv_7= 'speed' ( (lv_speed_8_0= ruleAExpr ) ) ) )?
            {
            // InternalRobot.g:278:3: (this_AcDock_0= ruleAcDock | this_AcMove_1= ruleAcMove | this_AcTurn_2= ruleAcTurn )
            int alt4=3;
            switch ( input.LA(1) ) {
            case 19:
                {
                alt4=1;
                }
                break;
            case 26:
                {
                alt4=2;
                }
                break;
            case 22:
                {
                alt4=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalRobot.g:279:4: this_AcDock_0= ruleAcDock
                    {

                    				newCompositeNode(grammarAccess.getActionAccess().getAcDockParserRuleCall_0_0());
                    			
                    pushFollow(FOLLOW_9);
                    this_AcDock_0=ruleAcDock();

                    state._fsp--;


                    				current = this_AcDock_0;
                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;
                case 2 :
                    // InternalRobot.g:288:4: this_AcMove_1= ruleAcMove
                    {

                    				newCompositeNode(grammarAccess.getActionAccess().getAcMoveParserRuleCall_0_1());
                    			
                    pushFollow(FOLLOW_9);
                    this_AcMove_1=ruleAcMove();

                    state._fsp--;


                    				current = this_AcMove_1;
                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;
                case 3 :
                    // InternalRobot.g:297:4: this_AcTurn_2= ruleAcTurn
                    {

                    				newCompositeNode(grammarAccess.getActionAccess().getAcTurnParserRuleCall_0_2());
                    			
                    pushFollow(FOLLOW_9);
                    this_AcTurn_2=ruleAcTurn();

                    state._fsp--;


                    				current = this_AcTurn_2;
                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;

            }

            // InternalRobot.g:306:3: ( (otherlv_3= 'for' ( (lv_duration_4_0= ruleAExpr ) ) otherlv_5= 's' ) | (otherlv_6= 'at' otherlv_7= 'speed' ( (lv_speed_8_0= ruleAExpr ) ) ) )?
            int alt5=3;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==15) ) {
                alt5=1;
            }
            else if ( (LA5_0==17) ) {
                alt5=2;
            }
            switch (alt5) {
                case 1 :
                    // InternalRobot.g:307:4: (otherlv_3= 'for' ( (lv_duration_4_0= ruleAExpr ) ) otherlv_5= 's' )
                    {
                    // InternalRobot.g:307:4: (otherlv_3= 'for' ( (lv_duration_4_0= ruleAExpr ) ) otherlv_5= 's' )
                    // InternalRobot.g:308:5: otherlv_3= 'for' ( (lv_duration_4_0= ruleAExpr ) ) otherlv_5= 's'
                    {
                    otherlv_3=(Token)match(input,15,FOLLOW_10); 

                    					newLeafNode(otherlv_3, grammarAccess.getActionAccess().getForKeyword_1_0_0());
                    				
                    // InternalRobot.g:312:5: ( (lv_duration_4_0= ruleAExpr ) )
                    // InternalRobot.g:313:6: (lv_duration_4_0= ruleAExpr )
                    {
                    // InternalRobot.g:313:6: (lv_duration_4_0= ruleAExpr )
                    // InternalRobot.g:314:7: lv_duration_4_0= ruleAExpr
                    {

                    							newCompositeNode(grammarAccess.getActionAccess().getDurationAExprParserRuleCall_1_0_1_0());
                    						
                    pushFollow(FOLLOW_11);
                    lv_duration_4_0=ruleAExpr();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getActionRule());
                    							}
                    							set(
                    								current,
                    								"duration",
                    								lv_duration_4_0,
                    								"mdsebook.robot.xtext.Robot.AExpr");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }

                    otherlv_5=(Token)match(input,16,FOLLOW_2); 

                    					newLeafNode(otherlv_5, grammarAccess.getActionAccess().getSKeyword_1_0_2());
                    				

                    }


                    }
                    break;
                case 2 :
                    // InternalRobot.g:337:4: (otherlv_6= 'at' otherlv_7= 'speed' ( (lv_speed_8_0= ruleAExpr ) ) )
                    {
                    // InternalRobot.g:337:4: (otherlv_6= 'at' otherlv_7= 'speed' ( (lv_speed_8_0= ruleAExpr ) ) )
                    // InternalRobot.g:338:5: otherlv_6= 'at' otherlv_7= 'speed' ( (lv_speed_8_0= ruleAExpr ) )
                    {
                    otherlv_6=(Token)match(input,17,FOLLOW_12); 

                    					newLeafNode(otherlv_6, grammarAccess.getActionAccess().getAtKeyword_1_1_0());
                    				
                    otherlv_7=(Token)match(input,18,FOLLOW_10); 

                    					newLeafNode(otherlv_7, grammarAccess.getActionAccess().getSpeedKeyword_1_1_1());
                    				
                    // InternalRobot.g:346:5: ( (lv_speed_8_0= ruleAExpr ) )
                    // InternalRobot.g:347:6: (lv_speed_8_0= ruleAExpr )
                    {
                    // InternalRobot.g:347:6: (lv_speed_8_0= ruleAExpr )
                    // InternalRobot.g:348:7: lv_speed_8_0= ruleAExpr
                    {

                    							newCompositeNode(grammarAccess.getActionAccess().getSpeedAExprParserRuleCall_1_1_2_0());
                    						
                    pushFollow(FOLLOW_2);
                    lv_speed_8_0=ruleAExpr();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getActionRule());
                    							}
                    							set(
                    								current,
                    								"speed",
                    								lv_speed_8_0,
                    								"mdsebook.robot.xtext.Robot.AExpr");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAction"


    // $ANTLR start "entryRuleAcDock"
    // InternalRobot.g:371:1: entryRuleAcDock returns [EObject current=null] : iv_ruleAcDock= ruleAcDock EOF ;
    public final EObject entryRuleAcDock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAcDock = null;


        try {
            // InternalRobot.g:371:47: (iv_ruleAcDock= ruleAcDock EOF )
            // InternalRobot.g:372:2: iv_ruleAcDock= ruleAcDock EOF
            {
             newCompositeNode(grammarAccess.getAcDockRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAcDock=ruleAcDock();

            state._fsp--;

             current =iv_ruleAcDock; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAcDock"


    // $ANTLR start "ruleAcDock"
    // InternalRobot.g:378:1: ruleAcDock returns [EObject current=null] : ( () otherlv_1= 'return' otherlv_2= 'to' otherlv_3= 'base' ) ;
    public final EObject ruleAcDock() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;


        	enterRule();

        try {
            // InternalRobot.g:384:2: ( ( () otherlv_1= 'return' otherlv_2= 'to' otherlv_3= 'base' ) )
            // InternalRobot.g:385:2: ( () otherlv_1= 'return' otherlv_2= 'to' otherlv_3= 'base' )
            {
            // InternalRobot.g:385:2: ( () otherlv_1= 'return' otherlv_2= 'to' otherlv_3= 'base' )
            // InternalRobot.g:386:3: () otherlv_1= 'return' otherlv_2= 'to' otherlv_3= 'base'
            {
            // InternalRobot.g:386:3: ()
            // InternalRobot.g:387:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAcDockAccess().getAcDockAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,19,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getAcDockAccess().getReturnKeyword_1());
            		
            otherlv_2=(Token)match(input,20,FOLLOW_14); 

            			newLeafNode(otherlv_2, grammarAccess.getAcDockAccess().getToKeyword_2());
            		
            otherlv_3=(Token)match(input,21,FOLLOW_2); 

            			newLeafNode(otherlv_3, grammarAccess.getAcDockAccess().getBaseKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAcDock"


    // $ANTLR start "entryRuleAcTurn"
    // InternalRobot.g:409:1: entryRuleAcTurn returns [EObject current=null] : iv_ruleAcTurn= ruleAcTurn EOF ;
    public final EObject entryRuleAcTurn() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAcTurn = null;


        try {
            // InternalRobot.g:409:47: (iv_ruleAcTurn= ruleAcTurn EOF )
            // InternalRobot.g:410:2: iv_ruleAcTurn= ruleAcTurn EOF
            {
             newCompositeNode(grammarAccess.getAcTurnRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAcTurn=ruleAcTurn();

            state._fsp--;

             current =iv_ruleAcTurn; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAcTurn"


    // $ANTLR start "ruleAcTurn"
    // InternalRobot.g:416:1: ruleAcTurn returns [EObject current=null] : ( () otherlv_1= 'turn' ( ( (lv_right_2_0= 'right' ) ) | otherlv_3= 'left' )? (otherlv_4= 'by' ( (lv_angle_5_0= ruleAExpr ) ) )? ) ;
    public final EObject ruleAcTurn() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_right_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        EObject lv_angle_5_0 = null;



        	enterRule();

        try {
            // InternalRobot.g:422:2: ( ( () otherlv_1= 'turn' ( ( (lv_right_2_0= 'right' ) ) | otherlv_3= 'left' )? (otherlv_4= 'by' ( (lv_angle_5_0= ruleAExpr ) ) )? ) )
            // InternalRobot.g:423:2: ( () otherlv_1= 'turn' ( ( (lv_right_2_0= 'right' ) ) | otherlv_3= 'left' )? (otherlv_4= 'by' ( (lv_angle_5_0= ruleAExpr ) ) )? )
            {
            // InternalRobot.g:423:2: ( () otherlv_1= 'turn' ( ( (lv_right_2_0= 'right' ) ) | otherlv_3= 'left' )? (otherlv_4= 'by' ( (lv_angle_5_0= ruleAExpr ) ) )? )
            // InternalRobot.g:424:3: () otherlv_1= 'turn' ( ( (lv_right_2_0= 'right' ) ) | otherlv_3= 'left' )? (otherlv_4= 'by' ( (lv_angle_5_0= ruleAExpr ) ) )?
            {
            // InternalRobot.g:424:3: ()
            // InternalRobot.g:425:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAcTurnAccess().getAcTurnAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,22,FOLLOW_15); 

            			newLeafNode(otherlv_1, grammarAccess.getAcTurnAccess().getTurnKeyword_1());
            		
            // InternalRobot.g:435:3: ( ( (lv_right_2_0= 'right' ) ) | otherlv_3= 'left' )?
            int alt6=3;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==23) ) {
                alt6=1;
            }
            else if ( (LA6_0==24) ) {
                alt6=2;
            }
            switch (alt6) {
                case 1 :
                    // InternalRobot.g:436:4: ( (lv_right_2_0= 'right' ) )
                    {
                    // InternalRobot.g:436:4: ( (lv_right_2_0= 'right' ) )
                    // InternalRobot.g:437:5: (lv_right_2_0= 'right' )
                    {
                    // InternalRobot.g:437:5: (lv_right_2_0= 'right' )
                    // InternalRobot.g:438:6: lv_right_2_0= 'right'
                    {
                    lv_right_2_0=(Token)match(input,23,FOLLOW_16); 

                    						newLeafNode(lv_right_2_0, grammarAccess.getAcTurnAccess().getRightRightKeyword_2_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAcTurnRule());
                    						}
                    						setWithLastConsumed(current, "right", true, "right");
                    					

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalRobot.g:451:4: otherlv_3= 'left'
                    {
                    otherlv_3=(Token)match(input,24,FOLLOW_16); 

                    				newLeafNode(otherlv_3, grammarAccess.getAcTurnAccess().getLeftKeyword_2_1());
                    			

                    }
                    break;

            }

            // InternalRobot.g:456:3: (otherlv_4= 'by' ( (lv_angle_5_0= ruleAExpr ) ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==25) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalRobot.g:457:4: otherlv_4= 'by' ( (lv_angle_5_0= ruleAExpr ) )
                    {
                    otherlv_4=(Token)match(input,25,FOLLOW_10); 

                    				newLeafNode(otherlv_4, grammarAccess.getAcTurnAccess().getByKeyword_3_0());
                    			
                    // InternalRobot.g:461:4: ( (lv_angle_5_0= ruleAExpr ) )
                    // InternalRobot.g:462:5: (lv_angle_5_0= ruleAExpr )
                    {
                    // InternalRobot.g:462:5: (lv_angle_5_0= ruleAExpr )
                    // InternalRobot.g:463:6: lv_angle_5_0= ruleAExpr
                    {

                    						newCompositeNode(grammarAccess.getAcTurnAccess().getAngleAExprParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_angle_5_0=ruleAExpr();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAcTurnRule());
                    						}
                    						set(
                    							current,
                    							"angle",
                    							lv_angle_5_0,
                    							"mdsebook.robot.xtext.Robot.AExpr");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAcTurn"


    // $ANTLR start "entryRuleAcMove"
    // InternalRobot.g:485:1: entryRuleAcMove returns [EObject current=null] : iv_ruleAcMove= ruleAcMove EOF ;
    public final EObject entryRuleAcMove() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAcMove = null;


        try {
            // InternalRobot.g:485:47: (iv_ruleAcMove= ruleAcMove EOF )
            // InternalRobot.g:486:2: iv_ruleAcMove= ruleAcMove EOF
            {
             newCompositeNode(grammarAccess.getAcMoveRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAcMove=ruleAcMove();

            state._fsp--;

             current =iv_ruleAcMove; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAcMove"


    // $ANTLR start "ruleAcMove"
    // InternalRobot.g:492:1: ruleAcMove returns [EObject current=null] : ( () otherlv_1= 'move' ( ( (lv_forward_2_0= 'forward' ) ) | otherlv_3= 'backward' ) ) ;
    public final EObject ruleAcMove() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_forward_2_0=null;
        Token otherlv_3=null;


        	enterRule();

        try {
            // InternalRobot.g:498:2: ( ( () otherlv_1= 'move' ( ( (lv_forward_2_0= 'forward' ) ) | otherlv_3= 'backward' ) ) )
            // InternalRobot.g:499:2: ( () otherlv_1= 'move' ( ( (lv_forward_2_0= 'forward' ) ) | otherlv_3= 'backward' ) )
            {
            // InternalRobot.g:499:2: ( () otherlv_1= 'move' ( ( (lv_forward_2_0= 'forward' ) ) | otherlv_3= 'backward' ) )
            // InternalRobot.g:500:3: () otherlv_1= 'move' ( ( (lv_forward_2_0= 'forward' ) ) | otherlv_3= 'backward' )
            {
            // InternalRobot.g:500:3: ()
            // InternalRobot.g:501:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAcMoveAccess().getAcMoveAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,26,FOLLOW_17); 

            			newLeafNode(otherlv_1, grammarAccess.getAcMoveAccess().getMoveKeyword_1());
            		
            // InternalRobot.g:511:3: ( ( (lv_forward_2_0= 'forward' ) ) | otherlv_3= 'backward' )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==27) ) {
                alt8=1;
            }
            else if ( (LA8_0==28) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalRobot.g:512:4: ( (lv_forward_2_0= 'forward' ) )
                    {
                    // InternalRobot.g:512:4: ( (lv_forward_2_0= 'forward' ) )
                    // InternalRobot.g:513:5: (lv_forward_2_0= 'forward' )
                    {
                    // InternalRobot.g:513:5: (lv_forward_2_0= 'forward' )
                    // InternalRobot.g:514:6: lv_forward_2_0= 'forward'
                    {
                    lv_forward_2_0=(Token)match(input,27,FOLLOW_2); 

                    						newLeafNode(lv_forward_2_0, grammarAccess.getAcMoveAccess().getForwardForwardKeyword_2_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAcMoveRule());
                    						}
                    						setWithLastConsumed(current, "forward", true, "forward");
                    					

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalRobot.g:527:4: otherlv_3= 'backward'
                    {
                    otherlv_3=(Token)match(input,28,FOLLOW_2); 

                    				newLeafNode(otherlv_3, grammarAccess.getAcMoveAccess().getBackwardKeyword_2_1());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAcMove"


    // $ANTLR start "entryRuleAExpr"
    // InternalRobot.g:536:1: entryRuleAExpr returns [EObject current=null] : iv_ruleAExpr= ruleAExpr EOF ;
    public final EObject entryRuleAExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAExpr = null;


        try {
            // InternalRobot.g:536:46: (iv_ruleAExpr= ruleAExpr EOF )
            // InternalRobot.g:537:2: iv_ruleAExpr= ruleAExpr EOF
            {
             newCompositeNode(grammarAccess.getAExprRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAExpr=ruleAExpr();

            state._fsp--;

             current =iv_ruleAExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAExpr"


    // $ANTLR start "ruleAExpr"
    // InternalRobot.g:543:1: ruleAExpr returns [EObject current=null] : (this_MinusMultExpr_0= ruleMinusMultExpr | this_PlusMultExpr_1= rulePlusMultExpr ) ;
    public final EObject ruleAExpr() throws RecognitionException {
        EObject current = null;

        EObject this_MinusMultExpr_0 = null;

        EObject this_PlusMultExpr_1 = null;



        	enterRule();

        try {
            // InternalRobot.g:549:2: ( (this_MinusMultExpr_0= ruleMinusMultExpr | this_PlusMultExpr_1= rulePlusMultExpr ) )
            // InternalRobot.g:550:2: (this_MinusMultExpr_0= ruleMinusMultExpr | this_PlusMultExpr_1= rulePlusMultExpr )
            {
            // InternalRobot.g:550:2: (this_MinusMultExpr_0= ruleMinusMultExpr | this_PlusMultExpr_1= rulePlusMultExpr )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==30) ) {
                alt9=1;
            }
            else if ( (LA9_0==RULE_INT||LA9_0==29||LA9_0==33||LA9_0==35) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalRobot.g:551:3: this_MinusMultExpr_0= ruleMinusMultExpr
                    {

                    			newCompositeNode(grammarAccess.getAExprAccess().getMinusMultExprParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_MinusMultExpr_0=ruleMinusMultExpr();

                    state._fsp--;


                    			current = this_MinusMultExpr_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalRobot.g:560:3: this_PlusMultExpr_1= rulePlusMultExpr
                    {

                    			newCompositeNode(grammarAccess.getAExprAccess().getPlusMultExprParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_PlusMultExpr_1=rulePlusMultExpr();

                    state._fsp--;


                    			current = this_PlusMultExpr_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAExpr"


    // $ANTLR start "entryRulePlusMultExpr"
    // InternalRobot.g:572:1: entryRulePlusMultExpr returns [EObject current=null] : iv_rulePlusMultExpr= rulePlusMultExpr EOF ;
    public final EObject entryRulePlusMultExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePlusMultExpr = null;


        try {
            // InternalRobot.g:572:53: (iv_rulePlusMultExpr= rulePlusMultExpr EOF )
            // InternalRobot.g:573:2: iv_rulePlusMultExpr= rulePlusMultExpr EOF
            {
             newCompositeNode(grammarAccess.getPlusMultExprRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePlusMultExpr=rulePlusMultExpr();

            state._fsp--;

             current =iv_rulePlusMultExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePlusMultExpr"


    // $ANTLR start "rulePlusMultExpr"
    // InternalRobot.g:579:1: rulePlusMultExpr returns [EObject current=null] : ( (otherlv_0= '+' )? this_MultExpr_1= ruleMultExpr ( () ( ( (lv_ope_3_1= '+' | lv_ope_3_2= '-' ) ) ) ( (lv_right_4_0= ruleMultExpr ) ) )* ) ;
    public final EObject rulePlusMultExpr() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_ope_3_1=null;
        Token lv_ope_3_2=null;
        EObject this_MultExpr_1 = null;

        EObject lv_right_4_0 = null;



        	enterRule();

        try {
            // InternalRobot.g:585:2: ( ( (otherlv_0= '+' )? this_MultExpr_1= ruleMultExpr ( () ( ( (lv_ope_3_1= '+' | lv_ope_3_2= '-' ) ) ) ( (lv_right_4_0= ruleMultExpr ) ) )* ) )
            // InternalRobot.g:586:2: ( (otherlv_0= '+' )? this_MultExpr_1= ruleMultExpr ( () ( ( (lv_ope_3_1= '+' | lv_ope_3_2= '-' ) ) ) ( (lv_right_4_0= ruleMultExpr ) ) )* )
            {
            // InternalRobot.g:586:2: ( (otherlv_0= '+' )? this_MultExpr_1= ruleMultExpr ( () ( ( (lv_ope_3_1= '+' | lv_ope_3_2= '-' ) ) ) ( (lv_right_4_0= ruleMultExpr ) ) )* )
            // InternalRobot.g:587:3: (otherlv_0= '+' )? this_MultExpr_1= ruleMultExpr ( () ( ( (lv_ope_3_1= '+' | lv_ope_3_2= '-' ) ) ) ( (lv_right_4_0= ruleMultExpr ) ) )*
            {
            // InternalRobot.g:587:3: (otherlv_0= '+' )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==29) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalRobot.g:588:4: otherlv_0= '+'
                    {
                    otherlv_0=(Token)match(input,29,FOLLOW_10); 

                    				newLeafNode(otherlv_0, grammarAccess.getPlusMultExprAccess().getPlusSignKeyword_0());
                    			

                    }
                    break;

            }


            			newCompositeNode(grammarAccess.getPlusMultExprAccess().getMultExprParserRuleCall_1());
            		
            pushFollow(FOLLOW_18);
            this_MultExpr_1=ruleMultExpr();

            state._fsp--;


            			current = this_MultExpr_1;
            			afterParserOrEnumRuleCall();
            		
            // InternalRobot.g:601:3: ( () ( ( (lv_ope_3_1= '+' | lv_ope_3_2= '-' ) ) ) ( (lv_right_4_0= ruleMultExpr ) ) )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( ((LA12_0>=29 && LA12_0<=30)) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalRobot.g:602:4: () ( ( (lv_ope_3_1= '+' | lv_ope_3_2= '-' ) ) ) ( (lv_right_4_0= ruleMultExpr ) )
            	    {
            	    // InternalRobot.g:602:4: ()
            	    // InternalRobot.g:603:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getPlusMultExprAccess().getBinExprLeftAction_2_0(),
            	    						current);
            	    				

            	    }

            	    // InternalRobot.g:609:4: ( ( (lv_ope_3_1= '+' | lv_ope_3_2= '-' ) ) )
            	    // InternalRobot.g:610:5: ( (lv_ope_3_1= '+' | lv_ope_3_2= '-' ) )
            	    {
            	    // InternalRobot.g:610:5: ( (lv_ope_3_1= '+' | lv_ope_3_2= '-' ) )
            	    // InternalRobot.g:611:6: (lv_ope_3_1= '+' | lv_ope_3_2= '-' )
            	    {
            	    // InternalRobot.g:611:6: (lv_ope_3_1= '+' | lv_ope_3_2= '-' )
            	    int alt11=2;
            	    int LA11_0 = input.LA(1);

            	    if ( (LA11_0==29) ) {
            	        alt11=1;
            	    }
            	    else if ( (LA11_0==30) ) {
            	        alt11=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 11, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt11) {
            	        case 1 :
            	            // InternalRobot.g:612:7: lv_ope_3_1= '+'
            	            {
            	            lv_ope_3_1=(Token)match(input,29,FOLLOW_10); 

            	            							newLeafNode(lv_ope_3_1, grammarAccess.getPlusMultExprAccess().getOpePlusSignKeyword_2_1_0_0());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getPlusMultExprRule());
            	            							}
            	            							setWithLastConsumed(current, "ope", lv_ope_3_1, null);
            	            						

            	            }
            	            break;
            	        case 2 :
            	            // InternalRobot.g:623:7: lv_ope_3_2= '-'
            	            {
            	            lv_ope_3_2=(Token)match(input,30,FOLLOW_10); 

            	            							newLeafNode(lv_ope_3_2, grammarAccess.getPlusMultExprAccess().getOpeHyphenMinusKeyword_2_1_0_1());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getPlusMultExprRule());
            	            							}
            	            							setWithLastConsumed(current, "ope", lv_ope_3_2, null);
            	            						

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // InternalRobot.g:636:4: ( (lv_right_4_0= ruleMultExpr ) )
            	    // InternalRobot.g:637:5: (lv_right_4_0= ruleMultExpr )
            	    {
            	    // InternalRobot.g:637:5: (lv_right_4_0= ruleMultExpr )
            	    // InternalRobot.g:638:6: lv_right_4_0= ruleMultExpr
            	    {

            	    						newCompositeNode(grammarAccess.getPlusMultExprAccess().getRightMultExprParserRuleCall_2_2_0());
            	    					
            	    pushFollow(FOLLOW_18);
            	    lv_right_4_0=ruleMultExpr();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getPlusMultExprRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_4_0,
            	    							"mdsebook.robot.xtext.Robot.MultExpr");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePlusMultExpr"


    // $ANTLR start "entryRuleMinusMultExpr"
    // InternalRobot.g:660:1: entryRuleMinusMultExpr returns [EObject current=null] : iv_ruleMinusMultExpr= ruleMinusMultExpr EOF ;
    public final EObject entryRuleMinusMultExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMinusMultExpr = null;


        try {
            // InternalRobot.g:660:54: (iv_ruleMinusMultExpr= ruleMinusMultExpr EOF )
            // InternalRobot.g:661:2: iv_ruleMinusMultExpr= ruleMinusMultExpr EOF
            {
             newCompositeNode(grammarAccess.getMinusMultExprRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMinusMultExpr=ruleMinusMultExpr();

            state._fsp--;

             current =iv_ruleMinusMultExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMinusMultExpr"


    // $ANTLR start "ruleMinusMultExpr"
    // InternalRobot.g:667:1: ruleMinusMultExpr returns [EObject current=null] : ( () otherlv_1= '-' ( (lv_aexpr_2_0= ruleMultExpr ) ) ( () ( ( (lv_ope_4_1= '+' | lv_ope_4_2= '-' ) ) ) ( (lv_right_5_0= ruleMultExpr ) ) )* ) ;
    public final EObject ruleMinusMultExpr() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_ope_4_1=null;
        Token lv_ope_4_2=null;
        EObject lv_aexpr_2_0 = null;

        EObject lv_right_5_0 = null;



        	enterRule();

        try {
            // InternalRobot.g:673:2: ( ( () otherlv_1= '-' ( (lv_aexpr_2_0= ruleMultExpr ) ) ( () ( ( (lv_ope_4_1= '+' | lv_ope_4_2= '-' ) ) ) ( (lv_right_5_0= ruleMultExpr ) ) )* ) )
            // InternalRobot.g:674:2: ( () otherlv_1= '-' ( (lv_aexpr_2_0= ruleMultExpr ) ) ( () ( ( (lv_ope_4_1= '+' | lv_ope_4_2= '-' ) ) ) ( (lv_right_5_0= ruleMultExpr ) ) )* )
            {
            // InternalRobot.g:674:2: ( () otherlv_1= '-' ( (lv_aexpr_2_0= ruleMultExpr ) ) ( () ( ( (lv_ope_4_1= '+' | lv_ope_4_2= '-' ) ) ) ( (lv_right_5_0= ruleMultExpr ) ) )* )
            // InternalRobot.g:675:3: () otherlv_1= '-' ( (lv_aexpr_2_0= ruleMultExpr ) ) ( () ( ( (lv_ope_4_1= '+' | lv_ope_4_2= '-' ) ) ) ( (lv_right_5_0= ruleMultExpr ) ) )*
            {
            // InternalRobot.g:675:3: ()
            // InternalRobot.g:676:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getMinusMultExprAccess().getMinusAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,30,FOLLOW_10); 

            			newLeafNode(otherlv_1, grammarAccess.getMinusMultExprAccess().getHyphenMinusKeyword_1());
            		
            // InternalRobot.g:686:3: ( (lv_aexpr_2_0= ruleMultExpr ) )
            // InternalRobot.g:687:4: (lv_aexpr_2_0= ruleMultExpr )
            {
            // InternalRobot.g:687:4: (lv_aexpr_2_0= ruleMultExpr )
            // InternalRobot.g:688:5: lv_aexpr_2_0= ruleMultExpr
            {

            					newCompositeNode(grammarAccess.getMinusMultExprAccess().getAexprMultExprParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_18);
            lv_aexpr_2_0=ruleMultExpr();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getMinusMultExprRule());
            					}
            					set(
            						current,
            						"aexpr",
            						lv_aexpr_2_0,
            						"mdsebook.robot.xtext.Robot.MultExpr");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalRobot.g:705:3: ( () ( ( (lv_ope_4_1= '+' | lv_ope_4_2= '-' ) ) ) ( (lv_right_5_0= ruleMultExpr ) ) )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( ((LA14_0>=29 && LA14_0<=30)) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalRobot.g:706:4: () ( ( (lv_ope_4_1= '+' | lv_ope_4_2= '-' ) ) ) ( (lv_right_5_0= ruleMultExpr ) )
            	    {
            	    // InternalRobot.g:706:4: ()
            	    // InternalRobot.g:707:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getMinusMultExprAccess().getBinExprLeftAction_3_0(),
            	    						current);
            	    				

            	    }

            	    // InternalRobot.g:713:4: ( ( (lv_ope_4_1= '+' | lv_ope_4_2= '-' ) ) )
            	    // InternalRobot.g:714:5: ( (lv_ope_4_1= '+' | lv_ope_4_2= '-' ) )
            	    {
            	    // InternalRobot.g:714:5: ( (lv_ope_4_1= '+' | lv_ope_4_2= '-' ) )
            	    // InternalRobot.g:715:6: (lv_ope_4_1= '+' | lv_ope_4_2= '-' )
            	    {
            	    // InternalRobot.g:715:6: (lv_ope_4_1= '+' | lv_ope_4_2= '-' )
            	    int alt13=2;
            	    int LA13_0 = input.LA(1);

            	    if ( (LA13_0==29) ) {
            	        alt13=1;
            	    }
            	    else if ( (LA13_0==30) ) {
            	        alt13=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 13, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt13) {
            	        case 1 :
            	            // InternalRobot.g:716:7: lv_ope_4_1= '+'
            	            {
            	            lv_ope_4_1=(Token)match(input,29,FOLLOW_10); 

            	            							newLeafNode(lv_ope_4_1, grammarAccess.getMinusMultExprAccess().getOpePlusSignKeyword_3_1_0_0());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getMinusMultExprRule());
            	            							}
            	            							setWithLastConsumed(current, "ope", lv_ope_4_1, null);
            	            						

            	            }
            	            break;
            	        case 2 :
            	            // InternalRobot.g:727:7: lv_ope_4_2= '-'
            	            {
            	            lv_ope_4_2=(Token)match(input,30,FOLLOW_10); 

            	            							newLeafNode(lv_ope_4_2, grammarAccess.getMinusMultExprAccess().getOpeHyphenMinusKeyword_3_1_0_1());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getMinusMultExprRule());
            	            							}
            	            							setWithLastConsumed(current, "ope", lv_ope_4_2, null);
            	            						

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // InternalRobot.g:740:4: ( (lv_right_5_0= ruleMultExpr ) )
            	    // InternalRobot.g:741:5: (lv_right_5_0= ruleMultExpr )
            	    {
            	    // InternalRobot.g:741:5: (lv_right_5_0= ruleMultExpr )
            	    // InternalRobot.g:742:6: lv_right_5_0= ruleMultExpr
            	    {

            	    						newCompositeNode(grammarAccess.getMinusMultExprAccess().getRightMultExprParserRuleCall_3_2_0());
            	    					
            	    pushFollow(FOLLOW_18);
            	    lv_right_5_0=ruleMultExpr();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getMinusMultExprRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_5_0,
            	    							"mdsebook.robot.xtext.Robot.MultExpr");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMinusMultExpr"


    // $ANTLR start "entryRuleMultExpr"
    // InternalRobot.g:764:1: entryRuleMultExpr returns [EObject current=null] : iv_ruleMultExpr= ruleMultExpr EOF ;
    public final EObject entryRuleMultExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMultExpr = null;


        try {
            // InternalRobot.g:764:49: (iv_ruleMultExpr= ruleMultExpr EOF )
            // InternalRobot.g:765:2: iv_ruleMultExpr= ruleMultExpr EOF
            {
             newCompositeNode(grammarAccess.getMultExprRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMultExpr=ruleMultExpr();

            state._fsp--;

             current =iv_ruleMultExpr; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMultExpr"


    // $ANTLR start "ruleMultExpr"
    // InternalRobot.g:771:1: ruleMultExpr returns [EObject current=null] : (this_Atomic_0= ruleAtomic ( () ( ( (lv_ope_2_1= '*' | lv_ope_2_2= '/' ) ) ) ( (lv_right_3_0= ruleAtomic ) ) )* ) ;
    public final EObject ruleMultExpr() throws RecognitionException {
        EObject current = null;

        Token lv_ope_2_1=null;
        Token lv_ope_2_2=null;
        EObject this_Atomic_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalRobot.g:777:2: ( (this_Atomic_0= ruleAtomic ( () ( ( (lv_ope_2_1= '*' | lv_ope_2_2= '/' ) ) ) ( (lv_right_3_0= ruleAtomic ) ) )* ) )
            // InternalRobot.g:778:2: (this_Atomic_0= ruleAtomic ( () ( ( (lv_ope_2_1= '*' | lv_ope_2_2= '/' ) ) ) ( (lv_right_3_0= ruleAtomic ) ) )* )
            {
            // InternalRobot.g:778:2: (this_Atomic_0= ruleAtomic ( () ( ( (lv_ope_2_1= '*' | lv_ope_2_2= '/' ) ) ) ( (lv_right_3_0= ruleAtomic ) ) )* )
            // InternalRobot.g:779:3: this_Atomic_0= ruleAtomic ( () ( ( (lv_ope_2_1= '*' | lv_ope_2_2= '/' ) ) ) ( (lv_right_3_0= ruleAtomic ) ) )*
            {

            			newCompositeNode(grammarAccess.getMultExprAccess().getAtomicParserRuleCall_0());
            		
            pushFollow(FOLLOW_19);
            this_Atomic_0=ruleAtomic();

            state._fsp--;


            			current = this_Atomic_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalRobot.g:787:3: ( () ( ( (lv_ope_2_1= '*' | lv_ope_2_2= '/' ) ) ) ( (lv_right_3_0= ruleAtomic ) ) )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( ((LA16_0>=31 && LA16_0<=32)) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalRobot.g:788:4: () ( ( (lv_ope_2_1= '*' | lv_ope_2_2= '/' ) ) ) ( (lv_right_3_0= ruleAtomic ) )
            	    {
            	    // InternalRobot.g:788:4: ()
            	    // InternalRobot.g:789:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getMultExprAccess().getBinExprLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalRobot.g:795:4: ( ( (lv_ope_2_1= '*' | lv_ope_2_2= '/' ) ) )
            	    // InternalRobot.g:796:5: ( (lv_ope_2_1= '*' | lv_ope_2_2= '/' ) )
            	    {
            	    // InternalRobot.g:796:5: ( (lv_ope_2_1= '*' | lv_ope_2_2= '/' ) )
            	    // InternalRobot.g:797:6: (lv_ope_2_1= '*' | lv_ope_2_2= '/' )
            	    {
            	    // InternalRobot.g:797:6: (lv_ope_2_1= '*' | lv_ope_2_2= '/' )
            	    int alt15=2;
            	    int LA15_0 = input.LA(1);

            	    if ( (LA15_0==31) ) {
            	        alt15=1;
            	    }
            	    else if ( (LA15_0==32) ) {
            	        alt15=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 15, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt15) {
            	        case 1 :
            	            // InternalRobot.g:798:7: lv_ope_2_1= '*'
            	            {
            	            lv_ope_2_1=(Token)match(input,31,FOLLOW_10); 

            	            							newLeafNode(lv_ope_2_1, grammarAccess.getMultExprAccess().getOpeAsteriskKeyword_1_1_0_0());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getMultExprRule());
            	            							}
            	            							setWithLastConsumed(current, "ope", lv_ope_2_1, null);
            	            						

            	            }
            	            break;
            	        case 2 :
            	            // InternalRobot.g:809:7: lv_ope_2_2= '/'
            	            {
            	            lv_ope_2_2=(Token)match(input,32,FOLLOW_10); 

            	            							newLeafNode(lv_ope_2_2, grammarAccess.getMultExprAccess().getOpeSolidusKeyword_1_1_0_1());
            	            						

            	            							if (current==null) {
            	            								current = createModelElement(grammarAccess.getMultExprRule());
            	            							}
            	            							setWithLastConsumed(current, "ope", lv_ope_2_2, null);
            	            						

            	            }
            	            break;

            	    }


            	    }


            	    }

            	    // InternalRobot.g:822:4: ( (lv_right_3_0= ruleAtomic ) )
            	    // InternalRobot.g:823:5: (lv_right_3_0= ruleAtomic )
            	    {
            	    // InternalRobot.g:823:5: (lv_right_3_0= ruleAtomic )
            	    // InternalRobot.g:824:6: lv_right_3_0= ruleAtomic
            	    {

            	    						newCompositeNode(grammarAccess.getMultExprAccess().getRightAtomicParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_19);
            	    lv_right_3_0=ruleAtomic();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getMultExprRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"mdsebook.robot.xtext.Robot.Atomic");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultExpr"


    // $ANTLR start "entryRuleAtomic"
    // InternalRobot.g:846:1: entryRuleAtomic returns [EObject current=null] : iv_ruleAtomic= ruleAtomic EOF ;
    public final EObject entryRuleAtomic() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAtomic = null;


        try {
            // InternalRobot.g:846:47: (iv_ruleAtomic= ruleAtomic EOF )
            // InternalRobot.g:847:2: iv_ruleAtomic= ruleAtomic EOF
            {
             newCompositeNode(grammarAccess.getAtomicRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAtomic=ruleAtomic();

            state._fsp--;

             current =iv_ruleAtomic; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAtomic"


    // $ANTLR start "ruleAtomic"
    // InternalRobot.g:853:1: ruleAtomic returns [EObject current=null] : (this_RndI_0= ruleRndI | ( () ( (lv_value_2_0= RULE_INT ) ) ) | (otherlv_3= '(' this_AExpr_4= ruleAExpr otherlv_5= ')' ) ) ;
    public final EObject ruleAtomic() throws RecognitionException {
        EObject current = null;

        Token lv_value_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject this_RndI_0 = null;

        EObject this_AExpr_4 = null;



        	enterRule();

        try {
            // InternalRobot.g:859:2: ( (this_RndI_0= ruleRndI | ( () ( (lv_value_2_0= RULE_INT ) ) ) | (otherlv_3= '(' this_AExpr_4= ruleAExpr otherlv_5= ')' ) ) )
            // InternalRobot.g:860:2: (this_RndI_0= ruleRndI | ( () ( (lv_value_2_0= RULE_INT ) ) ) | (otherlv_3= '(' this_AExpr_4= ruleAExpr otherlv_5= ')' ) )
            {
            // InternalRobot.g:860:2: (this_RndI_0= ruleRndI | ( () ( (lv_value_2_0= RULE_INT ) ) ) | (otherlv_3= '(' this_AExpr_4= ruleAExpr otherlv_5= ')' ) )
            int alt17=3;
            switch ( input.LA(1) ) {
            case 35:
                {
                alt17=1;
                }
                break;
            case RULE_INT:
                {
                alt17=2;
                }
                break;
            case 33:
                {
                alt17=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }

            switch (alt17) {
                case 1 :
                    // InternalRobot.g:861:3: this_RndI_0= ruleRndI
                    {

                    			newCompositeNode(grammarAccess.getAtomicAccess().getRndIParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_RndI_0=ruleRndI();

                    state._fsp--;


                    			current = this_RndI_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalRobot.g:870:3: ( () ( (lv_value_2_0= RULE_INT ) ) )
                    {
                    // InternalRobot.g:870:3: ( () ( (lv_value_2_0= RULE_INT ) ) )
                    // InternalRobot.g:871:4: () ( (lv_value_2_0= RULE_INT ) )
                    {
                    // InternalRobot.g:871:4: ()
                    // InternalRobot.g:872:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAtomicAccess().getCstIAction_1_0(),
                    						current);
                    				

                    }

                    // InternalRobot.g:878:4: ( (lv_value_2_0= RULE_INT ) )
                    // InternalRobot.g:879:5: (lv_value_2_0= RULE_INT )
                    {
                    // InternalRobot.g:879:5: (lv_value_2_0= RULE_INT )
                    // InternalRobot.g:880:6: lv_value_2_0= RULE_INT
                    {
                    lv_value_2_0=(Token)match(input,RULE_INT,FOLLOW_2); 

                    						newLeafNode(lv_value_2_0, grammarAccess.getAtomicAccess().getValueINTTerminalRuleCall_1_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAtomicRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_2_0,
                    							"org.eclipse.xtext.common.Terminals.INT");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalRobot.g:898:3: (otherlv_3= '(' this_AExpr_4= ruleAExpr otherlv_5= ')' )
                    {
                    // InternalRobot.g:898:3: (otherlv_3= '(' this_AExpr_4= ruleAExpr otherlv_5= ')' )
                    // InternalRobot.g:899:4: otherlv_3= '(' this_AExpr_4= ruleAExpr otherlv_5= ')'
                    {
                    otherlv_3=(Token)match(input,33,FOLLOW_10); 

                    				newLeafNode(otherlv_3, grammarAccess.getAtomicAccess().getLeftParenthesisKeyword_2_0());
                    			

                    				newCompositeNode(grammarAccess.getAtomicAccess().getAExprParserRuleCall_2_1());
                    			
                    pushFollow(FOLLOW_20);
                    this_AExpr_4=ruleAExpr();

                    state._fsp--;


                    				current = this_AExpr_4;
                    				afterParserOrEnumRuleCall();
                    			
                    otherlv_5=(Token)match(input,34,FOLLOW_2); 

                    				newLeafNode(otherlv_5, grammarAccess.getAtomicAccess().getRightParenthesisKeyword_2_2());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAtomic"


    // $ANTLR start "entryRuleRndI"
    // InternalRobot.g:920:1: entryRuleRndI returns [EObject current=null] : iv_ruleRndI= ruleRndI EOF ;
    public final EObject entryRuleRndI() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRndI = null;


        try {
            // InternalRobot.g:920:45: (iv_ruleRndI= ruleRndI EOF )
            // InternalRobot.g:921:2: iv_ruleRndI= ruleRndI EOF
            {
             newCompositeNode(grammarAccess.getRndIRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRndI=ruleRndI();

            state._fsp--;

             current =iv_ruleRndI; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRndI"


    // $ANTLR start "ruleRndI"
    // InternalRobot.g:927:1: ruleRndI returns [EObject current=null] : ( () otherlv_1= 'random' (otherlv_2= '(' ( (lv_min_3_0= ruleAExpr ) ) otherlv_4= ',' ( (lv_max_5_0= ruleAExpr ) ) otherlv_6= ')' )? ) ;
    public final EObject ruleRndI() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_min_3_0 = null;

        EObject lv_max_5_0 = null;



        	enterRule();

        try {
            // InternalRobot.g:933:2: ( ( () otherlv_1= 'random' (otherlv_2= '(' ( (lv_min_3_0= ruleAExpr ) ) otherlv_4= ',' ( (lv_max_5_0= ruleAExpr ) ) otherlv_6= ')' )? ) )
            // InternalRobot.g:934:2: ( () otherlv_1= 'random' (otherlv_2= '(' ( (lv_min_3_0= ruleAExpr ) ) otherlv_4= ',' ( (lv_max_5_0= ruleAExpr ) ) otherlv_6= ')' )? )
            {
            // InternalRobot.g:934:2: ( () otherlv_1= 'random' (otherlv_2= '(' ( (lv_min_3_0= ruleAExpr ) ) otherlv_4= ',' ( (lv_max_5_0= ruleAExpr ) ) otherlv_6= ')' )? )
            // InternalRobot.g:935:3: () otherlv_1= 'random' (otherlv_2= '(' ( (lv_min_3_0= ruleAExpr ) ) otherlv_4= ',' ( (lv_max_5_0= ruleAExpr ) ) otherlv_6= ')' )?
            {
            // InternalRobot.g:935:3: ()
            // InternalRobot.g:936:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getRndIAccess().getRndIAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,35,FOLLOW_21); 

            			newLeafNode(otherlv_1, grammarAccess.getRndIAccess().getRandomKeyword_1());
            		
            // InternalRobot.g:946:3: (otherlv_2= '(' ( (lv_min_3_0= ruleAExpr ) ) otherlv_4= ',' ( (lv_max_5_0= ruleAExpr ) ) otherlv_6= ')' )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==33) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalRobot.g:947:4: otherlv_2= '(' ( (lv_min_3_0= ruleAExpr ) ) otherlv_4= ',' ( (lv_max_5_0= ruleAExpr ) ) otherlv_6= ')'
                    {
                    otherlv_2=(Token)match(input,33,FOLLOW_10); 

                    				newLeafNode(otherlv_2, grammarAccess.getRndIAccess().getLeftParenthesisKeyword_2_0());
                    			
                    // InternalRobot.g:951:4: ( (lv_min_3_0= ruleAExpr ) )
                    // InternalRobot.g:952:5: (lv_min_3_0= ruleAExpr )
                    {
                    // InternalRobot.g:952:5: (lv_min_3_0= ruleAExpr )
                    // InternalRobot.g:953:6: lv_min_3_0= ruleAExpr
                    {

                    						newCompositeNode(grammarAccess.getRndIAccess().getMinAExprParserRuleCall_2_1_0());
                    					
                    pushFollow(FOLLOW_22);
                    lv_min_3_0=ruleAExpr();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getRndIRule());
                    						}
                    						set(
                    							current,
                    							"min",
                    							lv_min_3_0,
                    							"mdsebook.robot.xtext.Robot.AExpr");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_4=(Token)match(input,36,FOLLOW_10); 

                    				newLeafNode(otherlv_4, grammarAccess.getRndIAccess().getCommaKeyword_2_2());
                    			
                    // InternalRobot.g:974:4: ( (lv_max_5_0= ruleAExpr ) )
                    // InternalRobot.g:975:5: (lv_max_5_0= ruleAExpr )
                    {
                    // InternalRobot.g:975:5: (lv_max_5_0= ruleAExpr )
                    // InternalRobot.g:976:6: lv_max_5_0= ruleAExpr
                    {

                    						newCompositeNode(grammarAccess.getRndIAccess().getMaxAExprParserRuleCall_2_3_0());
                    					
                    pushFollow(FOLLOW_20);
                    lv_max_5_0=ruleAExpr();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getRndIRule());
                    						}
                    						set(
                    							current,
                    							"max",
                    							lv_max_5_0,
                    							"mdsebook.robot.xtext.Robot.AExpr");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_6=(Token)match(input,34,FOLLOW_2); 

                    				newLeafNode(otherlv_6, grammarAccess.getRndIAccess().getRightParenthesisKeyword_2_4());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRndI"


    // $ANTLR start "ruleEvent"
    // InternalRobot.g:1002:1: ruleEvent returns [Enumerator current=null] : ( (enumLiteral_0= 'obstacle' ) | (enumLiteral_1= 'clap' ) ) ;
    public final Enumerator ruleEvent() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalRobot.g:1008:2: ( ( (enumLiteral_0= 'obstacle' ) | (enumLiteral_1= 'clap' ) ) )
            // InternalRobot.g:1009:2: ( (enumLiteral_0= 'obstacle' ) | (enumLiteral_1= 'clap' ) )
            {
            // InternalRobot.g:1009:2: ( (enumLiteral_0= 'obstacle' ) | (enumLiteral_1= 'clap' ) )
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==37) ) {
                alt19=1;
            }
            else if ( (LA19_0==38) ) {
                alt19=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }
            switch (alt19) {
                case 1 :
                    // InternalRobot.g:1010:3: (enumLiteral_0= 'obstacle' )
                    {
                    // InternalRobot.g:1010:3: (enumLiteral_0= 'obstacle' )
                    // InternalRobot.g:1011:4: enumLiteral_0= 'obstacle'
                    {
                    enumLiteral_0=(Token)match(input,37,FOLLOW_2); 

                    				current = grammarAccess.getEventAccess().getEV_OBSTACLEEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getEventAccess().getEV_OBSTACLEEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalRobot.g:1018:3: (enumLiteral_1= 'clap' )
                    {
                    // InternalRobot.g:1018:3: (enumLiteral_1= 'clap' )
                    // InternalRobot.g:1019:4: enumLiteral_1= 'clap'
                    {
                    enumLiteral_1=(Token)match(input,38,FOLLOW_2); 

                    				current = grammarAccess.getEventAccess().getEV_CLAPEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getEventAccess().getEV_CLAPEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEvent"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000004486810L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000006000000000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000028002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000A60000020L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000003800002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000018000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000060000002L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000180000002L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000200000002L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000001000000000L});

}