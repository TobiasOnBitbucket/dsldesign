/* Copyright 2020-2021 Andrzej Wasowski and Thorsten Berger

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
// A gradle task implemented in Scala to generate code from xtext models

import scala.jdk.CollectionConverters._
import org.gradle.api.tasks.{JavaExec, InputFile, OutputDirectory, SourceSetContainer}
import javax.inject.Inject

class GenerateXtextLanguageTask (
  xtextFile: String,
  mwe2File: String,
  foo: Unit
) extends JavaExec {

  val root = this.getProject.getRootProject.getProjectDir().toString + "/"

  setMain ("org.eclipse.emf.mwe2.launch.runtime.Mwe2Launcher")
  setClasspath (this.getProject.getConfigurations.getByName ("compile"))
  setArgs (List(mwe2File, "-p", s"rootPath=${root}").asJava)

  val outputPath =  "src/main/xtext-gen"
  val log = new ScalaLoggerBridge (getProject.getLogger).info _

  log (s"GenerateXtextLanguage for '${xtextFile}' with '${mwe2File}' into '${outputPath}'")

  @InputFile
  def getXtextFile () = xtextFile

  @InputFile
  def getMwe2File () = mwe2File

  @OutputDirectory
  def getOutputPath () = outputPath

  getProject
    .getTasksByName ("generateXtext", false)
    .iterator
    .next
    .dependsOn (this)

  val sourceSets: SourceSetContainer =
    getProject
      .getProperties
      .get ("sourceSets")
      .asInstanceOf[SourceSetContainer]

  sourceSets
    .getByName ("main")
    .getJava
    .srcDir ("src/main/java")
    .srcDir ("src/main/xtext-gen")
    .srcDir ("src/main/xtend-gen")

  sourceSets
    .getByName ("test")
    .getJava
    .srcDir ("src/test/java")
    .srcDir ("src/test/xtext-gen")
    .srcDir ("src/test/xtend-gen")

  // Gradle requires that the constructor is annotated with Inject, so we make
  // the constructor here, and add a fake parameter to the default class
  // constructor, to distinguish the injected and the regular one.
  @Inject
  def this(xtextFile: String, mwe2File: String) =
    this(xtextFile, mwe2File, ())

}
