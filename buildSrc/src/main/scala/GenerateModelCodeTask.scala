/* Copyright 2020-2021 Andrzej Wasowski and Thorsten Berger

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
// A gradle task in Scala. Groovy had trouble finding GenerateModelCode.scala

import scala.jdk.CollectionConverters._

import org.gradle.api._
import org.gradle.api.tasks._
import javax.inject.Inject


// the genmodel should be a path relative to the root of the platform (the book
// project)
class GenerateModelCodeTask (genmodel: String, foo: Unit) extends DefaultTask {

  // Gradle requires that the constructor is annotated with Inject, so we make
  // the constructor here, and add a fake parameter to the default class
  // constructor, to distinguish the injected and the regular one.
  @Inject
  def this(genmodel: String) = this (genmodel, ())

  val logger = new ScalaLoggerBridge (getProject.getLogger).info _
  val root = getProject.getRootProject.getProjectDir.toString + "/"
  val generator = dsldesign.cmd.GenerateModelCode (genmodel, root, logger)
  val genmodelPath = this.getProject.file (root + generator.genmodelPath)
  val ecorePath =
    for (m <- generator.genmodel.getForeignModel.asScala)
    yield {
      getProject
        .file (root+generator.genmodelPath)
        .getParentFile
        .toString + "/" + m
    }

  val codePath =
    for (path <- generator.codePath)
    yield this.getProject.file (root + path)

  logger (s"GenerateModelCodeTask " +
          s"for '${genmodelPath}' " +
          s"into '${codePath.mkString (",")}'" +
          s"from ecore models '${ecorePath.mkString (",")}'")

  // setup outputs so that automatic clean tasks work
  for (path <- codePath)
    getOutputs.dir (path)

  // setup inputs so that automatic dependencies work
  getInputs.file (genmodelPath)
  for (ep <- ecorePath)
    getInputs.file (ep)

  this.setDescription (s"Generate model code for ${genmodel}")

  @TaskAction
  def taskAction () = generator.clean.generate

}
