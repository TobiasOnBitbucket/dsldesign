// (c) mdsebook, wasowski, tberger
package mdsebook.person.scala

import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

class PersonSpec extends AnyFreeSpec with Matchers {

  "Example showing a parent-children-inversion" - {

    lazy val Mom: Person = Person (name="Mom", child=LazyList (Son))
    lazy val Son: Person = Person (name="Son", parent=LazyList (Mom))

    info ("Children of Mom: " + Mom.getChild(0).getName)
    info ("Parents of Son: " + Son.getParent(0).getName)

    "confirm inversion" in {
      Mom.getChild(0) shouldBe Son
      Son.getParent(0) shouldBe Mom
    }
  }

  // All tests below are inverted (negated).  
  // Since the instances are broken,
  // the tests actually pass when they are broken.

  "Example violating parent children-inversion" - {

    lazy val B = Person (name="B", parent=LazyList (A))
    lazy val A = Person (name="A", child=LazyList (C))
    lazy val C = Person (name="C", parent=LazyList (D))
    lazy val D = Person (name="D")

    "Confirm violation of parent child-inversion" in {
      A.getChild should not contain (B)
      C.getParent should not contain (A)
      D.getParent should not contain (C)
    }
  }

  "Circular instances with two objects" - {

    lazy val Alice: Person = Person (name="Alice", parent=LazyList (Bob))
    lazy val Bob = Person (name="Bob", parent=LazyList (Alice))

    info ("Grand-parent of Alice: " + 
      Alice.getParent(0).getParent(0).getName)

    "Confirm circularity" in {
      Alice.getParent should contain (Bob)
      Bob.getParent should contain (Alice)
    }
  }

  "A circular instance with a single object" - {
    lazy val E: Person = 
      Person (name="E", parent=LazyList (E), child=LazyList (E))

    info ("Parent of E: " + E.getParent(0).getName)

    "Confirm circularity" in {
      E.getParent should contain (E)
      E.getChild should contain (E)
    }
  }
}
