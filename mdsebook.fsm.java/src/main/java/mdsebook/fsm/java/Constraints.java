// Copyright 2020 Andrzej Wasowski and Thorsten Berger
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mdsebook.fsm.java;

// Instead of reusing various extensions and infrastructure code implemented in
// Scala here (the APIs are, in principle accessible), we opted for a pure Java
// 8 implementation, to make the example reusable outside the context of this
// project.

import java.util.function.Function;
import java.util.stream.Stream;
import java.util.Objects;

import dsldesign.fsm.FiniteStateMachine;
import dsldesign.fsm.FsmPackage;
import dsldesign.fsm.Model;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;


class Main {
  
  // C2: all states within the same machine must have distinct names
  static Function<FiniteStateMachine, Boolean> C2a = 
    m -> m.getStates().stream().allMatch ( s1 -> 
           m.getStates().stream().allMatch ( s2 ->
              s1 == s2 
              || s1.getName() == null 
                  ? s2.getName() != null 
                  : ! s1.getName().equals (s2.getName())
             ) 
        );

  // A more concise formulation that handles the null problems
  // using Objects.equals (thanks @ekuiter)
  static Function<FiniteStateMachine, Boolean> C2 = 
    m -> m.getStates().stream().allMatch ( s1 -> 
           m.getStates().stream().allMatch ( s2 ->
              s1 == s2 || !Objects.equals (s1.getName(),s2.getName())
             ) 
        );

  private static ResourceSet resourceSet;

  // Load a test case
  public static Model loadInstance (String fname) 
  {
    System.out.println (String.format ("Loading '%s'...", fname));
    URI uri = URI.createURI (fname);
    Resource res = resourceSet.getResource (uri, true);
    return (Model) res.getContents().get(0);
  }



  // Check constraint C2 on all machines in the file designated by fname
  public static Boolean checkAll (String fname) 
  {
    Model testCase = loadInstance  (
        String.format ("../dsldesign.fsm/test-files/%s.xmi",fname));
    FiniteStateMachine[] machines = testCase
      .getMachines()
      .toArray(new FiniteStateMachine[0]);
    Boolean result = true;

    for (int i=0; i < machines.length; ++i)  {
      if (! C2.apply (machines[i])) 
      {
        String msg =  String.format (
            "C2 VIOLATED by machine '%s' in %s.xmi",
            machines[i].getName(), fname);
        System.out.println (msg);
        result = false;
      }
    }

    if (result) {
      String msg =  String.format(
          "C2 is satisfied by all machines in %s.xmi", fname);
      System.out.println (msg);
    } 

    return result;
  } 


  // Initialize EMF and validate 8 test cases
  public static void main(String[] args) {

    Resource.Factory.Registry.INSTANCE
      .getExtensionToFactoryMap()
      .put("xmi", new XMIResourceFactoryImpl());
    FsmPackage.eINSTANCE.eClass();
    resourceSet = new ResourceSetImpl();

    // File names we want to test (same as test cases for Scala)
    Stream<String> testModels = 
      Stream.of ("test-00", "test-01", "test-02", "test-07", 
                 "test-09", "test-11", "test-04", "test-14", 
                 "test-08");

    testModels.forEach (t -> checkAll (t));
  }

}
