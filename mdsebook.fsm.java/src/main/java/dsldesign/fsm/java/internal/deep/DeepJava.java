/*
   Copyright 2020-2021 Andrzej Wasowski and Thorsten Berger

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package dsldesign.fsm.java.internal.deep;
   
/** 
 * This interface allows to inject new values into a Java class through
 * inheritance, because in Java importing values does not seem to be a thing.
 * Or at least not in the version we use.
 */
interface DeepFsmJava {

   /** 'state$' refers to the object type in the Scala implementation. */
   static dsldesign.fsm.scala.internal.deep.state$ state = 
     dsldesign.fsm.scala.internal.deep.state$.MODULE$;
}
