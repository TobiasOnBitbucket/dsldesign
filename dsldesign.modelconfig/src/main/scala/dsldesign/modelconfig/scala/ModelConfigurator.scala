// (c) dsldesign, berger, wasowski
package dsldesign.modelconfig.scala

import dsldesign.expr._
import dsldesign.expr.xtext.scala.ExprParserUtils
import dsldesign.expr.xtext.{ExprParseHelper, ExprStandaloneSetupGenerated}
import dsldesign.scala.emf
import dsldesign.scala.emf._
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.{EModelElement, EObject, EPackage}

import java.io.{File, FileInputStream}
import java.util.Properties
import scala.collection.convert.ImplicitConversions.{`collection AsScalaIterable`, `dictionary AsScalaMap`}

object ModelConfigurator {

  def deriveModel( epackage:EPackage, configurationPropertiesFile: File ) {
    val properties = new Properties
    properties.load( new FileInputStream( configurationPropertiesFile ) )
    deriveModel( epackage, properties )
  }

  def deriveModel( epackage:EPackage, configuration: Properties ){
    // very simple configuration value parsing
    val c:List[Tuple2[String,Boolean]] = configuration.map{ case (k, v) =>
      ( k.toString, v.toString equalsIgnoreCase "true" )
    }.toList
    deriveModel( epackage, ExprParserUtils.createConfiguration( "", c ) )
  }

  def deriveModel( epackage:EPackage, configuration: Map[String,Boolean] ): Unit ={
    deriveModel( epackage,
      ExprParserUtils.createConfiguration( "", configuration.toList )
    )
  }

  def deriveModel( epackage:EPackage, configuration: Configuration ){
    val injector = new ExprStandaloneSetupGenerated().
      createInjectorAndDoEMFRegistration
    val ph = injector.getInstance(classOf[ExprParseHelper[Expression]])
    val c = EcoreUtil.getAllProperContents[EObject] (epackage, false)
    c foreach ( _ match{
      case e:EModelElement => {
        e.getEAnnotations.find( _.getDetails.containsKey("condition") ) match {
          case Some( annotation ) => {
            val cond = annotation.getDetails get "condition"
            if( evaluateCondition( ph, cond, configuration ) ) {
              // include element, i.e., just remove annotation
              EcoreUtil delete annotation
            } else {
              // do not include element, i.e., remove the element
              EcoreUtil delete e
            }
          }
          case _ => ;
        }
      }
      case _ => ;
    })

  }

  def evaluateCondition( ph: ExprParseHelper[Expression],
                         condition:String,
                         configuration: Configuration ): Boolean ={
    val e = ph.parse( condition )
    ExprParserUtils.eval( e, configuration )
  }
}
