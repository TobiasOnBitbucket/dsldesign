// (c) dsldesign, berger, wasowski
// Run using 'gradlew dsldesign.modelconfig:run'
package dsldesign.modelconfig.scala

import dsldesign.fsmpl.FsmplPackage
import dsldesign.scala.emf
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl
import org.eclipse.emf.ecore.{EObject, EPackage}

import java.io.File


object ModelConfiguratorExampleMain extends App {

  // register a resource factory for XMI files
  Resource.Factory.Registry.INSTANCE.
    getExtensionToFactoryMap.put("ecore", new EcoreResourceFactoryImpl)

  // register the package magic (impure)
  FsmplPackage.eINSTANCE.eClass

  // TODO fix paths (doesn't work from gradle, but intellij for now)
  val epackage:EPackage = emf.loadFromXMI( "dsldesign.fsmpl/model/fsmpl.ecore" )
  // http://download.eclipse.org/modeling/emf/emf/javadoc/2.11/org/eclipse/emf/ecore/util/EcoreUtil.html#getAllProperContents%28org.eclipse.emf.ecore.resource.Resource,%20boolean%29
  val content = EcoreUtil.getAllProperContents[EObject] (epackage, false)

  ModelConfigurator.deriveModel( epackage, new File("dsldesign.fsmpl/test-files/configuration1.properties") )

  emf.saveAsXMI ( "dsldesign.fsmpl/test-files/configured/fsmpl1.ecore") ( epackage )



}
