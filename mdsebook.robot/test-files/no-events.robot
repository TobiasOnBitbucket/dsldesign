-> NoEvents {

  -> MovingBackward  {
    start moving backward;
  } -> Rotating;

  Rotating {
    turn; 
  } -> MovingBackward;

};
