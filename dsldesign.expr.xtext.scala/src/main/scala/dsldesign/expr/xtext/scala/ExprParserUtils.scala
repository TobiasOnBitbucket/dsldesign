// (c) mdsebook, wasowski, tberger
package dsldesign.expr.xtext.scala

import dsldesign.expr._

import scala.collection.convert.ImplicitConversions.`iterable AsScalaIterable`


object ExprParserUtils {

  ExprPackage.eINSTANCE.eClass

  def prettyPrint( e: Expression ): String =
    e match {
      case a:AND => "(" + prettyPrint( a.getLeft ) +
        " && " + prettyPrint( a.getRight ) + ")"
      case o:OR => "(" + prettyPrint( o.getLeft )+
        " || " + prettyPrint( o.getRight) + ")"
      case i:Identifier => i.getName
      case n:NOT => prettyPrint( n.getExpr )
      case _ => "[unknown expression node]"
    }

  def printConfiguration( c: Configuration ): String =
    c.getName + ": " + c.getLiteral.map( l =>
      l.getName + "=" + l.isValue ).mkString(", ")

  def createConfiguration( name: String,
                           c: List[(String, Boolean)]
                         ): Configuration = {
    val eFactory = ExprFactory.eINSTANCE
    val ret = eFactory.createConfiguration
    ret setName name
    c foreach { case (n,v) => {
      val i = eFactory.createIdentifier
      i setName n
      i setValue v
      ret.getLiteral add i
    }}
    ret
  }

  def eval( e: Expression, c: Configuration): Boolean = {
    e match {
      case a:AND => eval( a.getLeft, c ) && eval( a.getRight, c )
      case o:OR => eval ( o.getLeft, c ) || eval( o.getRight, c )
      case i:Identifier => c.getLiteral.find( _.getName == i.getName ) match{
        case Some( cl ) => cl.isValue
        case None => false
      }
      case n:NOT => eval( n.getExpr, c )
      case _ => false
    }
  }


}
