// (c) mdsebook, wasowski, tberger
package dsldesign.expr
package xtext
package scala

import mdsebook.xtext.scala.XtextScala._
import ExprParserUtils._

object ExprXtextParserExampleMain extends App {

  ExprPackage.eINSTANCE.eClass
  implicit val setup = new ExprStandaloneSetup

  val expr = "(a && b) || c".parse[Expression]
  expr match{ case Some(a) => println( prettyPrint ( a ) ) }

  val eFactory = ExprFactory.eINSTANCE
  val c1 = createConfiguration( "configuration 1", ("a",true)::("b",false)::("c",true)::Nil )
  println( printConfiguration( c1 ) )
  println( eval ( expr.get, c1 ))



}

