// (c) mdsebook, wasowski, berger

// To execute this example run class mdsebook.fsm.xtext.scala.Main
// You can package as a standalone JVM application and use jvm directly. Still,
// the quickest, if you do not need to package and deploy, just run the
// following in the top directory of the mdsebook tree:
//
// ./gradlew :mdsebook.fsm.xtext.scala:run
//
// The results of the generation should be placed in
//
//    /mdsebook/mdsebook.fsm.xtext.scala/src-gen/
//
// In Eclipse you should be able to just right click this file and Run as >
// Scala Application.
//
// The input file name is hard coded below, but you can easily
// change this to use command line arguments

package mdsebook.fsm.xtext.scala

object Main extends App {

  val inputModel = "../dsldesign.fsm/test-files/CoffeeMachine.fsm"
  val g = new Generators (inputModel)

}
