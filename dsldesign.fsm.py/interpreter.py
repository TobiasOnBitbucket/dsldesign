from pyecore.resources import ResourceSet, URI

# Load the fsm meta-model
resource_set = ResourceSet()
resource = resource_set.get_resource(URI('../dsldesign.fsm/model/fsm.ecore'))
fsm_model = resource.contents[0]

# Register fsm ast the meta-model (needed)
resource_set.metamodel_registry[fsm_model.nsURI] = fsm_model

# A helper function to load fsm instances
def load_instance (fname):
    print (f'Loading "{fname}"...')
    resource = resource_set \
        .get_resource (URI (fname))
    return resource.contents[0]

def eval(state, input: str):
    active = filter(lambda t: t.input==input,
        state.leavingTransitions)
    tran = next(active, None)
    if tran:
        return (tran.target, tran.output)
    else:
        return None

def repl(state):
    while True:
        name = state.name
        msg1 = f'Machine in state "{name}".'
        inputs = (s.input
            for s in state.leavingTransitions)
        msg2 = ', '.join(inputs)
        try:
            print(msg1, end = '')
            i = input(f' Input [{msg2}]? ')
            result = eval(state, i)
            if result:
                state = result[0]
                if result[1]:
                    print('Machine outputs:',
                            end = '')
                    print(f' {result[1]}')
            else:
                print("Invalid input!")
        except EOFError:
            print("\nInvalid input!")

if __name__ == '__main__':
   test_case = load_instance("../dsldesign.fsm/test-files/CoffeeMachine.xmi")
   initial_state = test_case.machines[0].initial
   repl(initial_state)
